from math import sqrt, log, ceil, floor
import numpy as np
import glob
import otbApplication as otb
from scipy.ndimage import gaussian_filter
import os
import xml.etree.ElementTree as ET
import math
import s2signal as ss
from scipy.ndimage.morphology import distance_transform_cdt
import ogr
import osr


def readImageStats(stat_file):
    vals = []
    root = ET.parse(stat_file).getroot()
    for item in root.findall('Statistic'):
        for child in item:
            vals.append(float(child.attrib['value'].replace(',', '.')))
    mns = np.array(vals[:int(len(vals) / 2)])
    sds = np.array(vals[int(len(vals) / 2):])

    return mns, sds





def normalize_stack(lst, stat_file, mask_file=None):
    concat = otb.Registry.CreateApplication('ConcatenateImages')
    concat.SetParameterStringList('il', lst)
    concat.Execute()

    stat = otb.Registry.CreateApplication('ComputeImagesStatistics')

    if mask_file is not None:

        gen_msk = otb.Registry.CreateApplication('BandMath')
        gen_msk.SetParameterStringList('il', [mask_file])
        gen_msk.SetParameterString('exp', 'im1b1==0')
        gen_msk.Execute()
        ms = gen_msk.GetVectorImageAsNumpyArray('out')

        msk = otb.Registry.CreateApplication('BandMathX')
        msk.AddImageToParameterInputImageList('il', concat.GetParameterOutputImage('out'))
        msk.AddImageToParameterInputImageList('il', gen_msk.GetParameterOutputImage('out'))
        expr = '{' + ';'.join([f"im2b1==0?im1b{i + 1}:-10000" for i in range(len(lst))]) + '}'
        msk.SetParameterString('exp', expr)
        msk.Execute()
        stat.AddImageToParameterInputImageList('il', msk.GetParameterOutputImage('out'))
    else:
        stat.AddImageToParameterInputImageList('il', concat.GetParameterOutputImage('out'))

    stat.SetParameterString('bv', '-10000')
    stat.SetParameterString('out', stat_file)
    stat.ExecuteAndWriteOutput()

    mns, sds = readImageStats(stat_file)

    expr = '{' + ';'.join(
        ['(im1b%s-%s)/%s' % (str(i), str(m), str(s)) for i, m, s in
         zip(range(1, mns.shape[0] + 1), mns, sds)]) + '}'

    bm = otb.Registry.CreateApplication('BandMathX')
    bm.AddImageToParameterInputImageList('il', concat.GetParameterOutputImage('out'))
    bm.SetParameterString('exp', expr)
    # bm.SetParameterString('out', stack_file)
    # bm.SetParameterOutputImagePixelType('out', otb.ImagePixelType_float)
    # bm.ExecuteAndWriteOutput()
    bm.Execute()
    arr = bm.GetVectorImageAsNumpyArray('out')

    '''
    if mask_file is not None:
        return np.copy(arr), np.copy(ms)
    else:
        return np.copy(arr)
    '''

def create_normalized_numpy_stack(image_folder, out_dir=None, create_mask=None):
    if out_dir is None:
        out_dir = image_folder + os.sep + 'tmp'
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    lst = sorted(glob.glob(image_folder + os.sep + '*FRE_B*'))
    lst[-1], lst[-2] = lst[-2], lst[-1]
    lst = lst[2:] + lst[:2]

    lst10 = lst[:3] + [lst[6]]
    lst20 = lst[3:6] + lst[7:]

    s10_stats_path = out_dir + os.sep + 'S2_10_stats.xml'
    s20_stats_path = out_dir + os.sep + 'S2_20_stats.xml'
    if create_mask is not None:
        msk_10 = glob.glob(image_folder + os.sep + 'MASKS' + os.sep + '*CLM_R1*')[0]
        msk_20 = glob.glob(image_folder + os.sep + 'MASKS' + os.sep + '*CLM_R2*')[0]
    else:
        msk_10 = None
        msk_20 = None

    if create_mask is not None:
        stack_10, mask_10 = normalize_stack(lst10, s10_stats_path, msk_10)
        stack_20, mask_20 = normalize_stack(lst20, s20_stats_path, msk_20)
        mask = [mask_10, mask_20]
        return stack_10, stack_20, mask
    else:
        stack_10 = normalize_stack(lst10, s10_stats_path, msk_10)
        stack_20 = normalize_stack(lst20, s20_stats_path, msk_20)
        return stack_10, stack_20


def build_S2_Gaussian_filters(filter_size):
    GNyq10 = [0.275, 0.28, 0.25, 0.24]
    GNyq20 = [0.365, 0.33, 0.34, 0.32, 0.205, 0.235]

    N = filter_size;
    fcut = 1 / 2;

    gauss10 = []
    gauss20 = []

    for ii in range(len(GNyq10)):
        sigma = sqrt(((N - 1) * (fcut / 2)) ** 2 / (-2 * log(GNyq10[ii])))
        x = np.zeros((N, N))
        x[floor(N / 2), floor(N / 2)] = 1
        gauss10.append(gaussian_filter(x, sigma))

    for ii in range(len(GNyq20)):
        sigma = sqrt(((N - 1) * (fcut / 2)) ** 2 / (-2 * log(GNyq20[ii])))
        x = np.zeros((N, N))
        x[floor(N / 2), floor(N / 2)] = 1
        gauss20.append(gaussian_filter(x, sigma))

    gauss10str = []
    gauss20str = []

    for g in gauss10:
        gauss10str.append(
            '{ ' + np.array2string(g, precision=15, suppress_small=True, max_line_width=1000, separator=' , ').replace(
                '\n', ';').replace(',;', ';').replace('[', '').replace(']', '') + ' }')
    for g in gauss20:
        gauss20str.append(
            '{ ' + np.array2string(g, precision=15, suppress_small=True, max_line_width=1000, separator=' , ').replace(
                '\n', ';').replace(',;', ';').replace('[', '').replace(']', '') + ' }')

    gauss10 = np.dstack(gauss10)
    gauss20 = np.dstack(gauss20)

    return gauss10, gauss20, gauss10str, gauss20str


def downgrade_s2(stack_10, stack_20, create_mask=None, filter_size=7):
    s10 = stack_10
    s20 = stack_20
    # s10_d20 = stack_folder + os.sep + 'S2_10_norm_d20.tif'
    # s20_d40 = stack_folder + os.sep + 'S2_20_norm_d40.tif'
    # s20_tgt = tmp_dir + os.sep + 'S2_20_target.tif'

    # OTB solution
    _, _, gauss10exp, gauss20exp = build_S2_Gaussian_filters(filter_size)

    flt10 = ['dotpr( ' + gauss10exp[i] + ', im1b%dN%dx%d )' % (i + 1, filter_size, filter_size) for i in range(4)]
    flt20 = ['dotpr( ' + gauss20exp[i] + ', im1b%dN%dx%d )' % (i + 1, filter_size, filter_size) for i in range(6)]

    ref20 = s20
    ref40app = otb.Registry.CreateApplication('RigidTransformResample')
    ref40app.SetVectorImageFromNumpyArray('in', s20)
    ref40app.SetParameterFloat('transform.type.id.scalex', 0.5)
    ref40app.SetParameterFloat('transform.type.id.scaley', 0.5)
    ref40app.Execute()
    ref40 = ref40app.GetParameterOutputImage('out')

    bmx = []

    # Cannot perform a filter bank in a single BandMathX run
    # (delivers all zeros). Working if done band by band.
    concat10 = otb.Registry.CreateApplication('ConcatenateImages')
    for i in range(len(flt10)):
        bmx.append(otb.Registry.CreateApplication('BandMathX'))
        bmx[-1].SetVectorImageFromNumpyArray('il', s10)
        bmx[-1].SetParameterString('exp', flt10[i])
        bmx[-1].Execute()
        concat10.AddImageToParameterInputImageList('il', bmx[-1].GetParameterOutputImage('out'))
    concat10.Execute()

    concat20 = otb.Registry.CreateApplication('ConcatenateImages')
    for i in range(len(flt20)):
        bmx.append(otb.Registry.CreateApplication('BandMathX'))
        bmx[-1].SetVectorImageFromNumpyArray('il', s20)
        bmx[-1].SetParameterString('exp', flt20[i])
        bmx[-1].Execute()
        concat20.AddImageToParameterInputImageList('il', bmx[-1].GetParameterOutputImage('out'))
    concat20.Execute()

    sup10 = otb.Registry.CreateApplication('Superimpose')
    sup10.SetParameterInputImage('inm', concat10.GetParameterOutputImage('out'))
    sup10.SetVectorImageFromNumpyArray('inr', ref20)
    sup10.SetParameterString('interpolator', 'nn')

    # sup10.SetParameterString('out', s10_d20)
    # sup10.SetParameterOutputImagePixelType('out', otb.ImagePixelType_float)
    # sup10.ExecuteAndWriteOutput()
    sup10.Execute()

    s10_d20 = sup10.GetVectorImageAsNumpyArray('out')
    s10_d20 = np.copy(s10_d20)

    sup20 = otb.Registry.CreateApplication('Superimpose')
    sup20.SetParameterInputImage('inm', concat20.GetParameterOutputImage('out'))
    sup20.SetParameterInputImage('inr', ref40)
    sup20.SetParameterString('interpolator', 'nn')

    #    sup20.SetParameterString('out', s20_d40)
    #    sup20.SetParameterOutputImagePixelType('out', otb.ImagePixelType_float)
    #    sup20.ExecuteAndWriteOutput()
    sup20.Execute()

    s20_d40 = sup20.GetVectorImageAsNumpyArray('out')
    s20_d40 = np.copy(s20_d40)
    if create_mask is not None:
        msk20 = mask[1]
        supm = otb.Registry.CreateApplication('Superimpose')
        supm.SetImageFromNumpyArray('inm', msk20)
        supm.SetParameterInputImage('inr', ref40)
        supm.SetParameterString('interpolator', 'nn')
        # supm.SetParameterString('out', msk40)
        # supm.SetParameterOutputImagePixelType('out', otb.ImagePixelType_uint8)
        # supm.ExecuteAndWriteOutput()
        supm.Execute()
        msk40 = supm.GetImageAsNumpyArray('out')
        masks = [mask[0], mask[1], msk40]
        return s10_d20, s20_d40, masks
    else:
        return s10_d20, s20_d40



def createPointsWithDistanceConstraint(n, shape, min_dist):
    # thanks Samir
    # https://stackoverflow.com/users/5231231/samir

    # specify params
    shape = np.roll(np.array(shape), 1)
    d = floor(min_dist / 2)

    # compute grid shape based on number of points
    width_ratio = shape[1] / shape[0]
    num_y = np.int32(np.sqrt(n / width_ratio)) + 1
    num_x = np.int32(n / num_y) + 1

    # create regularly spaced neurons
    x = np.linspace(d, shape[1] - d, num_x, dtype=np.float32)
    y = np.linspace(d, shape[0] - d, num_y, dtype=np.float32)
    coords = np.stack(np.meshgrid(x, y), -1).reshape(-1, 2)

    # compute spacing
    init_dist = np.min((x[1] - x[0], y[1] - y[0]))

    if init_dist <= min_dist:
        print('Grid too small for the requested number of patches, returning regular grid without distance constraint.')
        return np.round(coords).astype(np.int)

    # perturb points
    max_movement = floor((init_dist - min_dist) / 2)
    noise = np.random.uniform(
        low=-max_movement,
        high=max_movement,
        size=(len(coords), 2))
    coords += noise

    # Push points close to border (< d) to distance d for patching purposes
    coords[coords < d] = d
    coords[coords[:, 0] > shape[1] - d, 0] = shape[1] - d
    coords[coords[:, 1] > shape[0] - d, 1] = shape[0] - d

    return np.round(coords).astype(np.int)



def getPatchCenters(img_file, patch_size, n_patches, mask_file=None, out_vec=None):
    shape_img = (img_file.shape[0], img_file.shape[1])
    npx_img = img_file.shape[0] * img_file.shape[1]

    if mask_file is not None:

        # Solution with binary erosion more precise but less efficient
        # Taxicab distance gives very close results, excludes very few close-to-mask positions
        # sel = np.ones((patch_size, patch_size))
        # in_arr = binary_erosion(arr, selem=sel)
        in_arr = (distance_transform_cdt(mask_file) > patch_size / 2)
        npx_msk = np.sum(in_arr == 1)
    else:
        arr = np.ones(shape_img)
        in_arr = arr
        npx_msk = npx_img

    # approximate compensation for "filtered" centers --> OLD
    # n_patches = floor(1.1 * n_patches)

    n_req_px = n_patches * (npx_img / npx_msk)

    coords = createPointsWithDistanceConstraint(ceil(n_req_px), shape_img, 66)
    out_coords = []

    q = floor(patch_size / 2)
    for c in coords:
        if q <= c[0] <= shape_img[0] - q and q <= c[1] <= shape_img[1] - q and in_arr[c[0], c[1]] == 1:
            #arr[c[0] - q:c[0] - q + patch_size, c[1] - q:c[1] - q + patch_size] = 2
            #arr[c[0], c[1]] = 3
            out_coords.append(c)

    #return np.asarray(out_coords), arr
    return np.asarray(out_coords)



def PatchExtraction (File10, File20, FileRef, PatSize=None, NSamples=None, cloud_mask=None):
    Patch10 = []
    Patch20 = []
    PatchRef = []

    if PatSize is None:
        PatchSize = [32, 32]
    else:
        PatchSize = PatSize

    if NSamples is None:
        Samples = 500
    else:
        Samples = NSamples

    Centers = getPatchCenters(File20, min(PatchSize), Samples, cloud_mask)
    Centers = np.asarray(Centers)
    Cnt_x_20 = Centers[:, 0]
    Cnt_y_20 = Centers[:, 1]

#    File10 = np.squeeze(File10, axis=0)
#    File20 = np.squeeze(File20, axis=0)
#    FileRef = np.squeeze(FileRef, axis=0)

    File10 = np.moveaxis(File10, -1, 0)
    File20 = np.moveaxis(File20, -1, 0)
    FileRef = np.moveaxis(FileRef, -1, 0)

    for i in range(len(Cnt_x_20)):
        MinPatch20x = Cnt_x_20[i] - math.ceil(PatchSize[0] / 2)
        MinPatch20y = Cnt_y_20[i] - math.ceil(PatchSize[1] / 2)
        MaxPatch20x = Cnt_x_20[i] + math.floor(PatchSize[0] / 2)
        MaxPatch20y = Cnt_y_20[i] + math.floor(PatchSize[1] / 2)

        TempPatch20 = File20[:, MinPatch20x:MaxPatch20x, MinPatch20y:MaxPatch20y]
        Patch20.append(TempPatch20)

        MinPatch10x = MinPatch20x * 2
        MinPatch10y = MinPatch20y * 2
        MaxPatch10x = MaxPatch20x * 2
        MaxPatch10y = MaxPatch20y * 2

        TempPatch10 = File10[:, MinPatch10x:MaxPatch10x, MinPatch10y:MaxPatch10y]
        Patch10.append(TempPatch10)

        TempPatchRef = FileRef[:, MinPatch10x:MaxPatch10x, MinPatch10y:MaxPatch10y]
        PatchRef.append(TempPatchRef)

    Patch10 = np.asarray(Patch10)
    Patch20 = np.asarray(Patch20)
    PatchRef = np.asarray(PatchRef)

    ##Reshape for last channeling

    Patch10 = np.moveaxis(Patch10, 1, -1)
    Patch20 = np.moveaxis(Patch20, 1, -1)
    PatchRef = np.moveaxis(PatchRef, 1, -1)

    return Patch10, Patch20, PatchRef


if __name__ == '__main__':
    print('Launching test...')
    c_m = True
    stack_10, stack_20, mask = create_normalized_numpy_stack(
        '/DATA/Senegal/S2/SENTINEL2A_20190607-114707-360_L2A_T28PDC_D/SENTINEL2A_20190607-114707-360_L2A_T28PDC_C_V2-2',
        create_mask=c_m)
    stack_10d_20, stack_20_d40, stack_masks = downgrade_s2(stack_10, stack_20, c_m)
    patch10, patch20, patchRef = PatchExtraction(stack_10d_20, stack_20_d40, stack_20,[32,32], 1000, stack_masks[2])
    print('Done! Check "tmp" subdirectory.')
