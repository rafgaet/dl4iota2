from tensorflow.keras import layers, optimizers, models, callbacks, backend
from tensorflow import math
import tensorflow as tf
import datetime
import os
from math import sqrt, log, floor
import numpy as np
from scipy.ndimage import gaussian_filter


def NyquistGainFilter(Gain10=None, Gain20=None, FilterSize=None, FrqCut=None):
    if Gain10 is None:
        GNyq10 = [0.275, 0.28, 0.25, 0.24]
    else:
        GNyq10 = Gain10

    if Gain20 is None:
        GNyq20 = [0.365, 0.33, 0.34, 0.32, 0.205, 0.235]
    else:
        GNyq20 = Gain20

    if FilterSize is None:
        N = 7
    else:
        N = FilterSize

    if FrqCut is None:
        fcut = 1 / 2
    else:
        fcut = FrqCut

    gauss10 = []
    gauss20 = []

    for ii in range(len(GNyq10)):
        sigma = sqrt(((N - 1) * (fcut / 2)) ** 2 / (-2 * log(GNyq10[ii])))
        x = np.zeros((N, N))
        x[floor(N / 2), floor(N / 2)] = 1
        gauss10.append(gaussian_filter(x, sigma))

    for ii in range(len(GNyq20)):
        sigma = sqrt(((N - 1) * (fcut / 2)) ** 2 / (-2 * log(GNyq20[ii])))
        x = np.zeros((N, N))
        x[floor(N / 2), floor(N / 2)] = 1
        gauss20.append(gaussian_filter(x, sigma))

    gauss10 = np.dstack(gauss10)
    gauss20 = np.dstack(gauss20)

    gauss10 = np.expand_dims(gauss10, 3)
    gauss20 = np.expand_dims(gauss20, 3)

    return gauss10, gauss20


def scheduler(epoch):
    if epoch < 5:
        return 1e-4
    if 5 <= epoch < 100:
        return 1e-5
    if 10 <= epoch < 150:
        return 1e-6
    else:
        return 1e-5 * math.exp(0.1 * (20 - epoch))


def CustomCallbacks():
    TensorBoardDir = os.path.curdir + os.sep + 'logs' + os.sep + 'MaxNet_' + datetime.datetime.now().strftime(
        '%Y-%m-%d_%H-%M-%S')
    CheckPointDir = os.path.curdir + os.sep + 'checkpoints' + os.sep + 'MaxNet_' + datetime.datetime.now().strftime(
        '%Y-%m-%d_%H-%M-%S')
    if os.path.exists(CheckPointDir) is False:
        os.mkdir(CheckPointDir)
    CheckPointPrefix = os.path.join(CheckPointDir, "ckpt_{epoch:03d}.h5")

    callback = [
        callbacks.TensorBoard(log_dir=TensorBoardDir),
        callbacks.ModelCheckpoint(filepath=CheckPointPrefix, save_weights_only=True),
        callbacks.LearningRateScheduler(scheduler)
    ]

    return callback


def FindLastWeights():
    CheckPointRoot = os.path.curdir + os.sep + 'checkpoints'
    CheckDir = next(os.walk(CheckPointRoot))[1]
    LastDir = list(filter(lambda f: f.startswith("MaxNet_"), CheckDir))
    LastDir = sorted(LastDir)
    LastDirPath = os.path.join(CheckPointRoot, LastDir[-1])
    Checkpoints = next(os.walk(LastDirPath))[2]
    LastCheck = list(filter(lambda f: f.startswith("ckpt_"), Checkpoints))
    LastCheck = sorted(LastCheck)
    last = os.path.join(LastDirPath, LastCheck[-1])

    return last


def NetMax(S2_20, S2_10, load_default_weights=False, NyGain=None):
    if NyGain is None:
        gauss10, gauss20 = NyquistGainFilter()
    else:
        gauss10 = NyGain[0]
        gauss20 = NyGain[1]

    x = layers.Input(shape=(None, None, S2_20.shape[-1]), name="InputLayer_x")

    x_up = layers.UpSampling2D(size=(2, 2), interpolation='bilinear')(x)

    lf = layers.DepthwiseConv2D([7, 7], activation=None, padding='same', name='GaussianFilter_x',
                                data_format='channels_last')
    lf(x)
    lf.set_weights([gauss20, np.array([0] * S2_20.shape[-1])])
    lf.trainable = False
    filtered = lf(x)
    x_tilde = x - filtered
    x_tilde_hp = layers.UpSampling2D(size=(2, 2), interpolation='bilinear')(x_tilde)

    z = layers.Input(shape=(None, None, S2_10.shape[-1]), name="InputLayer_z")

    lf_1 = layers.DepthwiseConv2D([7, 7], activation=None, padding='same', name='GaussianFilter_z',
                                  data_format='channels_last')
    lf_1(z)
    lf_1.set_weights([gauss10, np.array([0] * S2_10.shape[-1])])
    lf_1.trainable = False
    filtered_1 = lf_1(z)
    z_hp = z - filtered_1

    """
    if S2_10.shape[1] % S2_20.shape[1] is not 0:
        x_tilde_hp = tf.pad(x_tilde_hp, [[0, 0], [0, 1], [0, 0], [0, 0]])
        x_up = tf.pad(x_up, [[0, 0], [0, 1], [0, 0], [0, 0]])

    if S2_10.shape[2] % S2_20.shape[2] is not 0:
        x_tilde_hp = tf.pad(x_tilde_hp, [[0, 0], [0, 0], [0, 1], [0, 0]])
        x_up = tf.pad(x_up, [[0, 0], [0, 0], [0, 1], [0, 0]])
    """
    concatenated = backend.concatenate([z_hp, x_tilde_hp], axis=-1)

    l1_1 = layers.BatchNormalization(name="bn_1")(concatenated)
    l2_1 = layers.Conv2D(48, [3, 3], name="cnn_1__1", activation="relu", data_format='channels_last', padding='same')(
        l1_1)
    l3_1 = layers.Conv2D(32, [3, 3], name="cnn_1__2", activation="relu", data_format='channels_last', padding='same')(
        l2_1)
    l4_1 = layers.Conv2D(32, [3, 3], name="cnn_1__3", activation="relu", data_format='channels_last', padding='same')(
        l3_1)
    l5_1 = layers.Conv2D(1, [3, 3], name="cnn_1__4", data_format='channels_last', padding='same')(
        l4_1)

    l1_2 = layers.BatchNormalization(name="bn_2")(concatenated)
    l2_2 = layers.Conv2D(48, [3, 3], name="cnn_2__1", activation="relu", data_format='channels_last', padding='same')(
        l1_2)
    l3_2 = layers.Conv2D(32, [3, 3], name="cnn_2__2", activation="relu", data_format='channels_last', padding='same')(
        l2_2)
    l4_2 = layers.Conv2D(32, [3, 3], name="cnn_2__3", activation="relu", data_format='channels_last', padding='same')(
        l3_2)
    l5_2 = layers.Conv2D(1, [3, 3], name="cnn_2__4", data_format='channels_last', padding='same')(
        l4_2)

    l1_3 = layers.BatchNormalization(name="bn_3")(concatenated)
    l2_3 = layers.Conv2D(48, [3, 3], name="cnn_3__1", activation="relu", data_format='channels_last', padding='same')(
        l1_3)
    l3_3 = layers.Conv2D(32, [3, 3], name="cnn_3__2", activation="relu", data_format='channels_last', padding='same')(
        l2_3)
    l4_3 = layers.Conv2D(32, [3, 3], name="cnn_3__3", activation="relu", data_format='channels_last', padding='same')(
        l3_3)
    l5_3 = layers.Conv2D(1, [3, 3], name="cnn_3__4", data_format='channels_last', padding='same')(
        l4_3)

    l1_4 = layers.BatchNormalization(name="bn_4")(concatenated)
    l2_4 = layers.Conv2D(48, [3, 3], name="cnn_4__1", activation="relu", data_format='channels_last', padding='same')(
        l1_4)
    l3_4 = layers.Conv2D(32, [3, 3], name="cnn_4__2", activation="relu", data_format='channels_last', padding='same')(
        l2_4)
    l4_4 = layers.Conv2D(32, [3, 3], name="cnn_4__3", activation="relu", data_format='channels_last', padding='same')(
        l3_4)
    l5_4 = layers.Conv2D(1, [3, 3], name="cnn_4__4", data_format='channels_last', padding='same')(
        l4_4)

    l1_5 = layers.BatchNormalization(name="bn_5")(concatenated)
    l2_5 = layers.Conv2D(48, [3, 3], name="cnn_5__1", activation="relu", data_format='channels_last', padding='same')(
        l1_5)
    l3_5 = layers.Conv2D(32, [3, 3], name="cnn_5__2", activation="relu", data_format='channels_last', padding='same')(
        l2_5)
    l4_5 = layers.Conv2D(32, [3, 3], name="cnn_5__3", activation="relu", data_format='channels_last', padding='same')(
        l3_5)
    l5_5 = layers.Conv2D(1, [3, 3], name="cnn_5__4", data_format='channels_last', padding='same')(
        l4_5)

    l1_6 = layers.BatchNormalization(name="bn_6")(concatenated)
    l2_6 = layers.Conv2D(48, [3, 3], name="cnn_6__1", activation="relu", data_format='channels_last', padding='same')(
        l1_6)
    l3_6 = layers.Conv2D(32, [3, 3], name="cnn_6__2", activation="relu", data_format='channels_last', padding='same')(
        l2_6)
    l4_6 = layers.Conv2D(32, [3, 3], name="cnn_6__3", activation="relu", data_format='channels_last', padding='same')(
        l3_6)
#    l5_6 = layers.Conv2D(1, [3, 3], name="cnn_6__4", activation="tanh", data_format='channels_last', padding='same')(
#        l4_6)
    l5_6 = layers.Conv2D(1, [3, 3], name="cnn_6__4", data_format='channels_last', padding='same')(
        l4_6)

    Out_1 = l5_1 + backend.expand_dims(x_up[:, :, :, 0])
    Out_2 = l5_2 + backend.expand_dims(x_up[:, :, :, 1])
    Out_3 = l5_3 + backend.expand_dims(x_up[:, :, :, 2])
    Out_4 = l5_4 + backend.expand_dims(x_up[:, :, :, 3])
    Out_5 = l5_5 + backend.expand_dims(x_up[:, :, :, 4])
    Out_6 = l5_6 + backend.expand_dims(x_up[:, :, :, 5])

    Out = layers.concatenate([Out_1, Out_2, Out_3, Out_4, Out_5, Out_6], axis=3)

    maxnet = models.Model(inputs=[x, z], outputs=Out)

    opt = optimizers.Adam(learning_rate=1e-4, beta_1=0.9, beta_2=0.999)

    if load_default_weights is True:
        maxnet.load_weights('default_weights.h5')
        print("Default weights loaded!")

    maxnet.compile(optimizer=opt, loss=LightLoss)

    return maxnet


def PaperLoss(y_true, y_pred):

    if y_true.shape[1] is not None:
        MeanFactor = y_true.shape[1] * y_true.shape[2]
    else:
        MeanFactor = 66*66


    Diff = y_pred - y_true

    tfDiff_0 = Diff[:, :, :, 0]
    tfDiff_1 = Diff[:, :, :, 1]
    tfDiff_2 = Diff[:, :, :, 2]
    tfDiff_3 = Diff[:, :, :, 3]
    tfDiff_4 = Diff[:, :, :, 4]
    tfDiff_5 = Diff[:, :, :, 5]

    tfDiff_0 = tf.expand_dims(tfDiff_0, axis=-1)
    tfDiff_1 = tf.expand_dims(tfDiff_1, axis=-1)
    tfDiff_2 = tf.expand_dims(tfDiff_2, axis=-1)
    tfDiff_3 = tf.expand_dims(tfDiff_3, axis=-1)
    tfDiff_4 = tf.expand_dims(tfDiff_4, axis=-1)
    tfDiff_5 = tf.expand_dims(tfDiff_5, axis=-1)

    y_pred_0 = tf.expand_dims(y_pred[:, :, :, 0], axis=-1)
    y_pred_1 = tf.expand_dims(y_pred[:, :, :, 1], axis=-1)
    y_pred_2 = tf.expand_dims(y_pred[:, :, :, 2], axis=-1)
    y_pred_3 = tf.expand_dims(y_pred[:, :, :, 3], axis=-1)
    y_pred_4 = tf.expand_dims(y_pred[:, :, :, 4], axis=-1)
    y_pred_5 = tf.expand_dims(y_pred[:, :, :, 5], axis=-1)

    filter1 = [[0, 1, 0], [0, 0, 0], [0, -1, 0]]
    filter1 = np.asarray(filter1)
    filter1 = np.expand_dims(filter1, axis=-1)
    filter1 = np.expand_dims(filter1, axis=-1)
    filter1 = filter1.astype('float32')

    filter2 = [[0, 0, 0], [1, 0, -1], [0, 0, 0]]
    filter2 = np.asarray(filter2)
    filter2 = np.expand_dims(filter2, axis=-1)
    filter2 = np.expand_dims(filter2, axis=-1)
    filter2 = filter2.astype('float32')

    filter3 = [[1, 0, 0], [0, 0, 0], [0, 0, -1]]
    filter3 = np.asarray(filter3)
    filter3 = np.expand_dims(filter3, axis=-1)
    filter3 = np.expand_dims(filter3, axis=-1)
    filter3 = filter3.astype('float32')

    filter4 = [[0, 0, 1], [0, 0, 0], [-1, 0, 0]]
    filter4 = np.asarray(filter4)
    filter4 = np.expand_dims(filter4, axis=-1)
    filter4 = np.expand_dims(filter4, axis=-1)
    filter4 = filter4.astype('float32')

    tfFilter1 = filter1
    tfFilter2 = filter2
    tfFilter3 = filter3
    tfFilter4 = filter4

    L0_Gx = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_1, tfFilter1, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L0_Gy = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_1, tfFilter2, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L0_GD = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_1, tfFilter3, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L0_Gd = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_1, tfFilter4, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor

    L1_Gx = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_1, tfFilter1, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L1_Gy = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_1, tfFilter2, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L1_GD = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_1, tfFilter3, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L1_Gd = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_1, tfFilter4, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor

    L2_Gx = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_2, tfFilter1, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L2_Gy = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_2, tfFilter2, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L2_GD = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_2, tfFilter3, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L2_Gd = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_2, tfFilter4, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor

    L3_Gx = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_3, tfFilter1, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L3_Gy = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_3, tfFilter2, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L3_GD = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_3, tfFilter3, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L3_Gd = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_3, tfFilter4, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor

    L4_Gx = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_4, tfFilter1, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L4_Gy = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_4, tfFilter2, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L4_GD = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_4, tfFilter3, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L4_Gd = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_4, tfFilter4, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor

    L5_Gx = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_5, tfFilter1, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L5_Gy = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_5, tfFilter2, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L5_GD = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_5, tfFilter3, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
    L5_Gd = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_5, tfFilter4, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor

    L0_G = []
    L0_G = tf.reduce_sum([L0_Gx, L0_Gy, L0_GD, L0_Gd])



    L1_G = []
    L1_G = tf.reduce_sum([L1_Gx, L1_Gy, L1_GD, L1_Gd])

    L2_G = []
    L2_G = tf.reduce_sum([L2_Gx, L2_Gy, L2_GD, L2_Gd])

    L3_G = []
    L3_G = tf.reduce_sum([L3_Gx, L3_Gy, L3_GD, L3_Gd])

    L4_G = []
    L4_G = tf.reduce_sum([L4_Gx, L4_Gy, L4_GD, L4_Gd])

    L5_G = []
    L5_G = tf.reduce_sum([L5_Gx, L5_Gy, L5_GD, L5_Gd])

    L0SpecArg = tf.norm(abs(tfDiff_0), ord=1)
    L0Spec = L0SpecArg / MeanFactor

    L1SpecArg = tf.norm(abs(tfDiff_1), ord=1)
    L1Spec = L1SpecArg / MeanFactor

    L2SpecArg = tf.norm(abs(tfDiff_2), ord=1)
    L2Spec = L2SpecArg / MeanFactor

    L3SpecArg = tf.norm(abs(tfDiff_3), ord=1)
    L3Spec = L3SpecArg / MeanFactor

    L4SpecArg = tf.norm(abs(tfDiff_4), ord=1)
    L4Spec = L4SpecArg / MeanFactor

    L5SpecArg = tf.norm(abs(tfDiff_5), ord=1)
    L5Spec = L5SpecArg / MeanFactor

    L0Struct = L0_G / 4

    L1Struct = L1_G / 4

    L2Struct = L2_G / 4

    L3Struct = L3_G / 4

    L4Struct = L4_G / 4

    L5Struct = L5_G / 4

    L0Reg = tf.cast((tf.norm(tf.image.image_gradients(y_pred_0), ord=1)) / MeanFactor, dtype='float32')
    L1Reg = tf.cast((tf.norm(tf.image.image_gradients(y_pred_1), ord=1)) / MeanFactor, dtype='float32')
    L2Reg = tf.cast((tf.norm(tf.image.image_gradients(y_pred_2), ord=1)) / MeanFactor, dtype='float32')
    L3Reg = tf.cast((tf.norm(tf.image.image_gradients(y_pred_3), ord=1)) / MeanFactor, dtype='float32')
    L4Reg = tf.cast((tf.norm(tf.image.image_gradients(y_pred_4), ord=1)) / MeanFactor, dtype='float32')
    L5Reg = tf.cast((tf.norm(tf.image.image_gradients(y_pred_5), ord=1)) / MeanFactor, dtype='float32')

    LambdaSpec = 1
    LambdaStruct = 0.1
    LambdaReg = 0.01

    L0 = LambdaSpec * L0Spec + LambdaStruct * L0Struct + LambdaReg * L0Reg
    L1 = LambdaSpec * L1Spec + LambdaStruct * L1Struct + LambdaReg * L1Reg
    L2 = LambdaSpec * L2Spec + LambdaStruct * L2Struct + LambdaReg * L2Reg
    L3 = LambdaSpec * L3Spec + LambdaStruct * L3Struct + LambdaReg * L3Reg
    L4 = LambdaSpec * L4Spec + LambdaStruct * L4Struct + LambdaReg * L4Reg
    L5 = LambdaSpec * L5Spec + LambdaStruct * L5Struct + LambdaReg * L5Reg

    L = L0 + L1 + L2 + L3 + L4 + L5

    return L

def LightLoss (y_true, y_pred):

    if y_true.shape[1] is not None:
        MeanFactor = y_true.shape[1] * y_true.shape[2]
    else:
        MeanFactor = 66*66

    Diff = y_pred - y_true

    baseflt = np.asarray([[0, 1, 0], [0, 0, 0], [0, -1, 0]])
    filter1 = np.zeros((3, 3, 6, 6))
    for i in range(6):
        filter1[:, :, i, i] = baseflt
    filter1 = filter1.astype('float32')

    baseflt = np.asarray([[0, 0, 0], [1, 0, -1], [0, 0, 0]])
    filter2 = np.zeros((3, 3, 6, 6))
    for i in range(6):
        filter2[:, :, i, i] = baseflt
    filter2 = filter2.astype('float32')

    baseflt = np.asarray([[1, 0, 0], [0, 0, 0], [0, 0, -1]])
    filter3 = np.zeros((3, 3, 6, 6))
    for i in range(6):
        filter3[:, :, i, i] = baseflt
    filter3 = filter3.astype('float32')

    baseflt = np.asarray([[0, 0, 1], [0, 0, 0], [-1, 0, 0]])
    filter4 = np.zeros((3, 3, 6, 6))
    for i in range(6):
        filter4[:, :, i, i] = baseflt
    filter4 = filter4.astype('float32')

    Gx = tf.norm(tf.norm(tf.nn.conv2d(Diff, filter1, strides=[1, 1, 1, 1], padding='VALID'), ord=1, axis=2), ord=1,
                 axis=1) / MeanFactor
    Gy = tf.norm(tf.norm(tf.nn.conv2d(Diff, filter2, strides=[1, 1, 1, 1], padding='VALID'), ord=1, axis=2), ord=1,
                 axis=1) / MeanFactor
    GD = tf.norm(tf.norm(tf.nn.conv2d(Diff, filter3, strides=[1, 1, 1, 1], padding='VALID'), ord=1, axis=2), ord=1,
                 axis=1) / MeanFactor
    Gd = tf.norm(tf.norm(tf.nn.conv2d(Diff, filter4, strides=[1, 1, 1, 1], padding='VALID'), ord=1, axis=2), ord=1,
                 axis=1) / MeanFactor

    LStruct = tf.reduce_mean(tf.reduce_sum(tf.stack([Gx, Gy, GD, Gd], axis=0), 0)/4, axis=0)
    LSpec = tf.reduce_mean(tf.norm(tf.norm(Diff, ord=1, axis=2), ord=1, axis=1) / MeanFactor, axis=0)

    G_y_pred = tf.image.image_gradients(y_pred)
    G_y_pred = abs(G_y_pred[0]) + abs(G_y_pred[1])
    LReg = tf.reduce_mean(tf.norm(tf.norm(G_y_pred, ord=1, axis=2), ord=1, axis=1) / MeanFactor, axis=0)

    LambdaSpec = 1
    LambdaStruct = 0.1
    LambdaReg = 0.01

    L = tf.reduce_sum(LambdaSpec * LSpec + LambdaStruct * LStruct + LambdaReg * LReg)
    return L