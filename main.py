import tensorflow as tf
import tensorboard
import numpy as np
import glob
import os, sys
import gdal
from tensorflow.keras import layers, losses, optimizers, activations, models, callbacks, backend, utils
from matplotlib import pyplot as plt
import numpy as np
from matplotlib.pyplot import imshow, figure
import gdal
from scipy.ndimage import gaussian_filter
import maxnet
import dataset_preprocess as dp


## Dataset

NSamples = 5000
image_path = '/DATA/Senegal/S2/SENTINEL2A_20190607-114707-360_L2A_T28PDC_D/SENTINEL2A_20190607-114707-360_L2A_T28PDC_C_V2-2'

Test10, Test20, Val10, Val20, ValRef, Train10, Train20, TrainRef, temp_dir = dp.AutomaticPreProcessing(image_path, NSamples, SaveDataset=False, enable_cloud=False)

tf.config.experimental.set_visible_devices([], 'GPU')
##Model Fit

epochs = 200
callback = maxnet.CustomCallbacks()
MaxNet = maxnet.NetMax(Val20, Val10, load_default_weights = False)

MaxNet.fit(
           [Train20, Train10],
           TrainRef,
           batch_size = 256,
           epochs = epochs,
           verbose = 1,
           callbacks = callback,
           shuffle=True,
           validation_data=([Val20, Val10], ValRef),
           validation_freq=5
           )


##Model Val_Predict_for_control

res_1 = MaxNet.predict([Val20, Val10])

##Results visualization

plt.figure(1)
plt.imshow(ValRef[0,50:-50,50:-50,0])
plt.figure(2)
plt.imshow(res_1[0,50:-50,50:-50,0])
plt.figure(3)
plt.imshow(Val20[0,:,:,0])

sys.exit()

##Saving Results
NSamples = 1000
image_path = 'S2_test/S2-L2A-THEIA_0_20180525_30PVT/'

Test10, Test20, Val10, Val20, ValRef, Train10, Train20, TrainRef, temp_dir = dp.AutomaticPreProcessing(image_path, NSamples, SaveDataset=False, enable_cloud=True)

tf.config.experimental.set_visible_devices([], 'GPU')

resnet = maxnet.NetMax(Test20, Test10)
weights_path = maxnet.FindLastWeights()
resnet.load_weights(weights_path)

net_result = resnet.predict([Test20, Test10], batch_size=1, verbose=1)

results = dp.SaveResults(Test10, net_result, temp_dir, image_path)


plt.figure()
plt.imshow(results[:,:,0])

