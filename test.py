from tensorflow.keras import layers, optimizers, models, callbacks, backend
from tensorflow import math
import tensorflow as tf
import datetime
import os
from math import sqrt, log, floor
import numpy as np
from scipy.ndimage import gaussian_filter
from matplotlib import pyplot as plt
y_true = np.load('Risultati/Result_Reference_PaperLoss_NOtanh_Norm_std_1.npy')
y_pred = np.load('Risultati/Result_Reference_PaperLoss_tanh_Norm_std_1.npy')

MeanFactor = 1172*2146

y_y = tf.Variable(y_pred)

Diff = y_pred - y_true

tfDiff_0 = Diff[:, :, :, 0]
tfDiff_1 = Diff[:, :, :, 1]
tfDiff_2 = Diff[:, :, :, 2]
tfDiff_3 = Diff[:, :, :, 3]
tfDiff_4 = Diff[:, :, :, 4]
tfDiff_5 = Diff[:, :, :, 5]

tfDiff_0 = tf.expand_dims(tfDiff_0, axis=-1)
tfDiff_1 = tf.expand_dims(tfDiff_1, axis=-1)
tfDiff_2 = tf.expand_dims(tfDiff_2, axis=-1)
tfDiff_3 = tf.expand_dims(tfDiff_3, axis=-1)
tfDiff_4 = tf.expand_dims(tfDiff_4, axis=-1)
tfDiff_5 = tf.expand_dims(tfDiff_5, axis=-1)

y_pred_0 = tf.expand_dims(y_pred[:, :, :, 0], axis=-1)
y_pred_1 = tf.expand_dims(y_pred[:, :, :, 1], axis=-1)
y_pred_2 = tf.expand_dims(y_pred[:, :, :, 2], axis=-1)
y_pred_3 = tf.expand_dims(y_pred[:, :, :, 3], axis=-1)
y_pred_4 = tf.expand_dims(y_pred[:, :, :, 4], axis=-1)
y_pred_5 = tf.expand_dims(y_pred[:, :, :, 5], axis=-1)

filter1 = [[0, 1, 0], [0, 0, 0], [0, -1, 0]]
filter1 = np.asarray(filter1)
filter1 = np.expand_dims(filter1, axis=-1)
filter1 = np.expand_dims(filter1, axis=-1)
filter1 = filter1.astype('float32')

filter2 = [[0, 0, 0], [1, 0, -1], [0, 0, 0]]
filter2 = np.asarray(filter2)
filter2 = np.expand_dims(filter2, axis=-1)
filter2 = np.expand_dims(filter2, axis=-1)
filter2 = filter2.astype('float32')

filter3 = [[1, 0, 0], [0, 0, 0], [0, 0, -1]]
filter3 = np.asarray(filter3)
filter3 = np.expand_dims(filter3, axis=-1)
filter3 = np.expand_dims(filter3, axis=-1)
filter3 = filter3.astype('float32')

filter4 = [[0, 0, 1], [0, 0, 0], [-1, 0, 0]]
filter4 = np.asarray(filter4)
filter4 = np.expand_dims(filter4, axis=-1)
filter4 = np.expand_dims(filter4, axis=-1)
filter4 = filter4.astype('float32')

tfFilter1 = filter1
tfFilter2 = filter2
tfFilter3 = filter3
tfFilter4 = filter4

L0_Gx = tf.math.abs(tf.nn.conv2d(tfDiff_0, tfFilter1, strides=[1, 1, 1, 1], padding='VALID')) / MeanFactor
L0_Gy = tf.math.abs(tf.nn.conv2d(tfDiff_0, tfFilter2, strides=[1, 1, 1, 1], padding='VALID')) / MeanFactor
L0_GD = tf.math.abs(tf.nn.conv2d(tfDiff_0, tfFilter3, strides=[1, 1, 1, 1], padding='VALID')) / MeanFactor
L0_Gd = tf.math.abs(tf.nn.conv2d(tfDiff_0, tfFilter4, strides=[1, 1, 1, 1], padding='VALID')) / MeanFactor

L1_Gx = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_1, tfFilter1, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
L1_Gy = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_1, tfFilter2, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
L1_GD = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_1, tfFilter3, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
L1_Gd = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_1, tfFilter4, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor

L2_Gx = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_2, tfFilter1, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
L2_Gy = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_2, tfFilter2, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
L2_GD = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_2, tfFilter3, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
L2_Gd = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_2, tfFilter4, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor

L3_Gx = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_3, tfFilter1, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
L3_Gy = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_3, tfFilter2, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
L3_GD = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_3, tfFilter3, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
L3_Gd = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_3, tfFilter4, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor

L4_Gx = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_4, tfFilter1, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
L4_Gy = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_4, tfFilter2, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
L4_GD = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_4, tfFilter3, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
L4_Gd = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_4, tfFilter4, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor

L5_Gx = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_5, tfFilter1, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
L5_Gy = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_5, tfFilter2, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
L5_GD = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_5, tfFilter3, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor
L5_Gd = tf.norm(tf.math.abs(tf.nn.conv2d(tfDiff_5, tfFilter4, strides=[1, 1, 1, 1], padding='VALID')), ord=1) / MeanFactor

L0_G = []
L0_Gx_norm = (tf.reduce_sum(tf.math.sqrt(abs(L0_Gx))))**2
L0_Gy_norm = (tf.reduce_sum(tf.math.sqrt(abs(L0_Gy))))**2
L0_GD_norm = (tf.reduce_sum(tf.math.sqrt(abs(L0_GD))))**2
L0_Gd_norm = (tf.reduce_sum(tf.math.sqrt(abs(L0_Gd))))**2
L0_G = tf.reduce_sum([L0_Gx_norm, L0_Gy_norm, L0_GD_norm, L0_Gd_norm])
L0_G = tf.reduce_sum([L0_Gx_norm])


L1_G = []
L1_G = tf.reduce_sum([L1_Gx, L1_Gy, L1_GD, L1_Gd])

L2_G = []
L2_G = tf.reduce_sum([L2_Gx, L2_Gy, L2_GD, L2_Gd])

L3_G = []
L3_G = tf.reduce_sum([L3_Gx, L3_Gy, L3_GD, L3_Gd])

L4_G = []
L4_G = tf.reduce_sum([L4_Gx, L4_Gy, L4_GD, L4_Gd])

L5_G = []
L5_G = tf.reduce_sum([L5_Gx, L5_Gy, L5_GD, L5_Gd])

L0SpecArg = tf.norm(abs(tfDiff_0), ord=1)
L0Spec = L0SpecArg / MeanFactor

L1SpecArg = tf.norm(abs(tfDiff_1), ord=1)
L1Spec = L1SpecArg / MeanFactor

L2SpecArg = tf.norm(abs(tfDiff_2), ord=1)
L2Spec = L2SpecArg / MeanFactor

L3SpecArg = tf.norm(abs(tfDiff_3), ord=1)
L3Spec = L3SpecArg / MeanFactor

L4SpecArg = tf.norm(abs(tfDiff_4), ord=1)
L4Spec = L4SpecArg / MeanFactor

L5SpecArg = tf.norm(abs(tfDiff_5), ord=1)
L5Spec = L5SpecArg / MeanFactor

L0Struct = L0_G / 4
0

L1Struct = L1_G / 4

L2Struct = L2_G / 4

L3Struct = L3_G / 4

L4Struct = L4_G / 4

L5Struct = L5_G / 4

L0Reg = tf.cast((tf.norm(tf.image.image_gradients(y_pred_0), ord=1)) / MeanFactor, dtype='float64')
L1Reg = tf.cast((tf.norm(tf.image.image_gradients(y_pred_1), ord=1)) / MeanFactor, dtype='float64')
L2Reg = tf.cast((tf.norm(tf.image.image_gradients(y_pred_2), ord=1)) / MeanFactor, dtype='float64')
L3Reg = tf.cast((tf.norm(tf.image.image_gradients(y_pred_3), ord=1)) / MeanFactor, dtype='float64')
L4Reg = tf.cast((tf.norm(tf.image.image_gradients(y_pred_4), ord=1)) / MeanFactor, dtype='float64')
L5Reg = tf.cast((tf.norm(tf.image.image_gradients(y_pred_5), ord=1)) / MeanFactor, dtype='float64')

LambdaSpec = 1
LambdaStruct = 0.1
LambdaReg = 0.01

L0 = LambdaSpec * L0Spec + LambdaStruct * L0Struct + LambdaReg * L0Reg
L1 = LambdaSpec * L1Spec + LambdaStruct * L1Struct + LambdaReg * L1Reg
L2 = LambdaSpec * L2Spec + LambdaStruct * L2Struct + LambdaReg * L2Reg
L3 = LambdaSpec * L3Spec + LambdaStruct * L3Struct + LambdaReg * L3Reg
L4 = LambdaSpec * L4Spec + LambdaStruct * L4Struct + LambdaReg * L4Reg
L5 = LambdaSpec * L5Spec + LambdaStruct * L5Struct + LambdaReg * L5Reg
L = L0 + L1 + L2 + L3 + L4 + L5

