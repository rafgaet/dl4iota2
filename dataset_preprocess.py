import numpy as np
import os
import gdal
import math
import sys
import s2sampling as ss
import s2signal


def OpenTifinNpy(TifPath):
    Tif10_path = TifPath + os.sep + 'S2_10_norm.tif'
    Tif10_d20_path = TifPath + os.sep + 'S2_10_norm_d20.tif'
    Tif20_path = TifPath + os.sep + 'S2_20_norm.tif'
    Tif20_d20_path = TifPath + os.sep + 'S2_20_norm_d40.tif'

    File10 = []
    File10_d20 = []
    File20 = []
    File20_d40 = []

    try:
        ds_10 = gdal.Open(Tif10_path)
    except RuntimeError:
        print('Unable to open S2_10_norm.tif')
        sys.exit(1)

    File10 = ds_10.ReadAsArray()
    np.moveaxis(File10, 0, -1)

    #    GeoT10 = ds_10.GetGeoTransform()

    try:
        ds_10_d20 = gdal.Open(Tif10_d20_path)
    except RuntimeError:
        print('Unable to open S2_10_norm_d20.tif')
        sys.exit(1)

    File10_d20 = ds_10_d20.ReadAsArray()
    np.moveaxis(File10_d20, 0, -1)

    #    GeoT10 = ds_10_d20.GetGeoTransform()

    try:
        ds_20 = gdal.Open(Tif20_path)
    except RuntimeError:
        print('Unable to open S2_20_norm.tif')
        sys.exit(1)

    File20 = ds_20.ReadAsArray()
    np.moveaxis(File20, 0, -1)

    try:
        ds_20_d40 = gdal.Open(Tif20_d20_path)
    except RuntimeError:
        print('Unable to open S2_20_norm_d40.tif')
        sys.exit(1)

    File20_d40 = ds_20_d40.ReadAsArray()
    np.moveaxis(File20_d40, 0, -1)

    File10 = np.moveaxis(File10, 0, -1)
    File10 = np.expand_dims(File10, axis=0)

    File10_d20 = np.moveaxis(File10_d20, 0, -1)
    File10_d20 = np.expand_dims(File10_d20, axis=0)

    File20 = np.moveaxis(File20, 0, -1)
    File20 = np.expand_dims(File20, axis=0)

    File20_d40 = np.moveaxis(File20_d40, 0, -1)
    File20_d40 = np.expand_dims(File20_d40, axis=0)

    return File10, File10_d20, File20, File20_d40


def StdNormalization(File10, File20, FileRef):
    buf = []
    Mean10 = []
    Mean20 = []
    MeanRef = []

    Std10 = []
    Std20 = []
    StdRef = []

    for i in range(File10.shape[0]):
        Mean10.append(np.mean(File10[i]))
        Std10.append(np.std(File10[i]))
        buf.append((File10[i] - Mean10[i]) / Std10[i])
    File10 = np.asarray(buf)

    buf = []
    for i in range(File20.shape[0]):
        Mean20.append(np.mean(File20[i]))
        Std20.append(np.std(File20[i]))
        buf.append((File20[i] - Mean20[i]) / Std20[i])
    File20 = np.asarray(buf)

    buf = []
    for i in range(FileRef.shape[0]):
        MeanRef.append(np.mean(FileRef[i]))
        StdRef.append(np.std(FileRef[i]))
        buf.append((FileRef[i] - MeanRef[i]) / StdRef[i])
    FileRef = np.asarray(buf)

    ##Reshape for last-channelling

    File10 = np.moveaxis(File10, 0, -1)
    File20 = np.moveaxis(File20, 0, -1)
    FileRef = np.moveaxis(FileRef, 0, -1)

    ##Expand dimension for Validation purpose

    File10 = np.expand_dims(File10, axis=0)
    File20 = np.expand_dims(File20, axis=0)
    FileRef = np.expand_dims(FileRef, axis=0)

    ## Creation Validation Dataset

    np.save(os.path.curdir + os.sep + 'S2_test/S2_10_val.npy', File10)
    np.save(os.path.curdir + os.sep + 'S2_test/S2_20_val.npy', File20)
    np.save(os.path.curdir + os.sep + 'S2_test/S2_20_val_ref.npy', FileRef)

    ## Save Means and Stds

    np.save(os.path.curdir + os.sep + 'S2_test/Mean10.npy', Mean10)
    np.save(os.path.curdir + os.sep + 'S2_test/Mean20.npy', Mean20)
    np.save(os.path.curdir + os.sep + 'S2_test/MeanRef.npy', MeanRef)

    np.save(os.path.curdir + os.sep + 'S2_test/Std10.npy', Std10)
    np.save(os.path.curdir + os.sep + 'S2_test/Std20.npy', Std20)
    np.save(os.path.curdir + os.sep + 'S2_test/StdRef.npy', StdRef)

    return File10, File20, FileRef


def StdDeNormalization(FileRef, path, sel10=False):
    if sel10 is True:
        print('10 meters selected')
        denormpath = path + os.sep + 'S2_10_stats.xml'
    else:
        denormpath = path + os.sep + 'S2_20_stats.xml'

    MeanRef, StdRef = s2signal.readImageStats(denormpath)

    FileRef = np.moveaxis(FileRef, -1, 1)

    buf = []
    for i in range(FileRef.shape[1]):
        buf.append((FileRef[:, i] * StdRef[i]) + MeanRef[i])
    FileRef_1 = np.asarray(buf)

    ##Reshape for last-channelling

    FileRef_1 = np.moveaxis(FileRef_1, 0, 1)
    FileRef_1 = np.moveaxis(FileRef_1, 1, -1)

    return FileRef_1


def MaxForBandNormalization(File10, File20, FileRef):
    buf = []
    Max10 = []
    Max20 = []
    MaxRef = []

    for i in range(File10.shape[0]):
        Max10.append(np.max(File10[i]))
        buf.append(File10[i] / Max10[i])
    File10 = np.asarray(buf)

    buf = []
    for i in range(File20.shape[0]):
        Max20.append(np.max(File20[i]))
        buf.append(File20[i] / Max20[i])
    File20 = np.asarray(buf)

    buf = []
    for i in range(FileRef.shape[0]):
        MaxRef.append(np.mean(FileRef[i]))
        buf.append(FileRef[i] / MaxRef[i])
    FileRef = np.asarray(buf)

    ##Reshape for last-channelling

    File10 = np.moveaxis(File10, 0, -1)
    File20 = np.moveaxis(File20, 0, -1)
    FileRef = np.moveaxis(FileRef, 0, -1)

    ##Expand dimension for Validation purpose

    File10 = np.expand_dims(File10, axis=0)
    File20 = np.expand_dims(File20, axis=0)
    FileRef = np.expand_dims(FileRef, axis=0)

    ## Creation Validation Dataset

    np.save(os.path.curdir + os.sep + 'S2_test/S2_10_val.npy', File10)
    np.save(os.path.curdir + os.sep + 'S2_test/S2_20_val.npy', File20)
    np.save(os.path.curdir + os.sep + 'S2_test/S2_20_val_ref.npy', FileRef)

    ## Save Means and Stds

    np.save(os.path.curdir + os.sep + 'S2_test/Max10.npy', Max10)
    np.save(os.path.curdir + os.sep + 'S2_test/Max20.npy', Max20)
    np.save(os.path.curdir + os.sep + 'S2_test/MaxRef.npy', MaxRef)

    return File10, File20, FileRef


def PatchExtractionAfterNorm(File10, File20, FileRef, S2_20_Path=None, PatSize=None, NSamples=None,
                             cloud_mask_path=None):
    Patch10 = []
    Patch20 = []
    PatchRef = []

    if PatSize is None:
        PatchSize = [32, 32]
    else:
        PatchSize = PatSize

    if NSamples is None:
        Samples = 500
    else:
        Samples = NSamples

    LowValue_x = math.ceil(PatchSize[0] / 2)
    LowValue_y = math.ceil(PatchSize[1] / 2)
    HighValue_x = File20.shape[1] - math.ceil(PatchSize[0] / 2) - 1
    HighValue_y = File20.shape[2] - math.ceil(PatchSize[1] / 2) - 1

    if S2_20_Path is None:
        Cnt_x_20 = np.random.randint(low=LowValue_x, high=HighValue_x, size=Samples)
        Cnt_y_20 = np.random.randint(low=LowValue_y, high=HighValue_y, size=Samples)
    else:
        Centers = ss.getPatchCenters(S2_20_Path, min(PatchSize), Samples, cloud_mask_path)
        Centers = np.asarray(Centers)
        Cnt_x_20 = Centers[:, 0]
        Cnt_y_20 = Centers[:, 1]

    File10 = np.squeeze(File10, axis=0)
    File20 = np.squeeze(File20, axis=0)
    FileRef = np.squeeze(FileRef, axis=0)

    File10 = np.moveaxis(File10, -1, 0)
    File20 = np.moveaxis(File20, -1, 0)
    FileRef = np.moveaxis(FileRef, -1, 0)

    for i in range(Samples):
        MinPatch20x = Cnt_x_20[i] - math.ceil(PatchSize[0] / 2)
        MinPatch20y = Cnt_y_20[i] - math.ceil(PatchSize[1] / 2)
        MaxPatch20x = Cnt_x_20[i] + math.floor(PatchSize[0] / 2)
        MaxPatch20y = Cnt_y_20[i] + math.floor(PatchSize[1] / 2)

        TempPatch20 = File20[:, MinPatch20x:MaxPatch20x, MinPatch20y:MaxPatch20y]
        Patch20.append(TempPatch20)

        MinPatch10x = MinPatch20x * 2
        MinPatch10y = MinPatch20y * 2
        MaxPatch10x = MaxPatch20x * 2
        MaxPatch10y = MaxPatch20y * 2

        TempPatch10 = File10[:, MinPatch10x:MaxPatch10x, MinPatch10y:MaxPatch10y]
        Patch10.append(TempPatch10)

        TempPatchRef = FileRef[:, MinPatch10x:MaxPatch10x, MinPatch10y:MaxPatch10y]
        PatchRef.append(TempPatchRef)

    Patch10 = np.asarray(Patch10)
    Patch20 = np.asarray(Patch20)
    PatchRef = np.asarray(PatchRef)

    ##Reshape for last channeling

    Patch10 = np.moveaxis(Patch10, 1, -1)
    Patch20 = np.moveaxis(Patch20, 1, -1)
    PatchRef = np.moveaxis(PatchRef, 1, -1)

    return Patch10, Patch20, PatchRef


def AutomaticPreProcessing(image_path, TrainingSamplesNumber=1000, SaveDataset=None, enable_cloud=False):
    tmp_dir = s2signal.create_normalized_s2_stack(image_path, create_mask=enable_cloud)
    s2signal.downgrade_s2(tmp_dir)

    File10, File10_d20, File20, File20_d40 = OpenTifinNpy(tmp_dir)
    S2_20_d40_path = tmp_dir + os.sep + 'S2_20_norm_d40.tif'

    if enable_cloud is True:
        cloud_path = tmp_dir + os.sep + 'S2_20_d40_mask.tif'
        print("Centers based on cloud mask")
    else:
        cloud_path = None

    Patch10, Patch20, PatchRef = PatchExtractionAfterNorm(
        File10_d20,
        File20_d40,
        File20,
        S2_20_d40_path,
        [32, 32],
        TrainingSamplesNumber,
        cloud_path
    )

    '''
    print(File10_d20.shape)
    print(File20_d40.shape)

    if File10_d20.shape[1] / File20_d40.shape[1] != 2:
        File10_d20 = File10_d20[:,0:File10_d20.shape[1]-1, 0:File10_d20.shape[2] - 1, :]
        File20 = File20[:, 0:File20.shape[1] - 1, 0:File20.shape[2] - 1, :]
    '''

    if SaveDataset is not None:
        np.save(tmp_dir + os.sep + 'S2_10_train.npy', Patch10)
        np.save(tmp_dir + os.sep + 'S2_20_train.npy', Patch20)
        np.save(tmp_dir + os.sep + 'S2_Ref_train.npy', PatchRef)

        np.save(tmp_dir + os.sep + 'S2_10_val.npy', File10_d20)
        np.save(tmp_dir + os.sep + 'S2_20_val.npy', File20_d40)
        np.save(tmp_dir + os.sep + 'S2_10.npy', File10)
        np.save(tmp_dir + os.sep + 'S2_20.npy', File20)

    return File10, File20, File10_d20, File20_d40, File20, Patch10, Patch20, PatchRef, tmp_dir


def SaveResults(test10, result, tmp_dir, save_path):
    test10 = StdDeNormalization(test10, tmp_dir, sel10=True)
    test10 = np.round(test10)
    test10.astype('uint16')

    result20 = StdDeNormalization(result, tmp_dir)
    result20 = np.round(result20)
    result20.astype('uint16')

    results = []
    results = np.concatenate(
        [test10[0, :, :, 0:3],
         result20[0, :, :, 0:3],
         np.expand_dims(test10[0, :, :, 3], axis=2),
         result20[0, :, :, 3:6]],
        axis=2)
    results = np.asarray(results)
    results = np.moveaxis(results, -1, 0)
    results.astype('uint16')

    reference_path = tmp_dir + os.sep + 'S2_10_norm.tif'

    ref = gdal.Open(reference_path)
    name = save_path + os.sep + 'S2_Results.tif'
    geo_t = ref.GetGeoTransform()
    projection = ref.GetProjection()
    bands = 10
    type = gdal.GDT_UInt16
    driver = gdal.GetDriverByName('GTiff')

    out = driver.Create(name, results.shape[2], results.shape[1], bands, type)
    out.SetGeoTransform(geo_t)
    out.SetProjection(projection)

    for i in range(results.shape[0]):
        out.GetRasterBand(i + 1).WriteArray(results[i])

    out = None

    return results
