import s2signal
import s2sampling as ss
import dataset_preprocess as dt
import os
import numpy as np
from matplotlib import pyplot as plt
import gdal


NSamples = 1000
image_path = 'S2_test/S2-L2A-THEIA_0_20180525_30PVT/'

Test10, Test20, Val10, Val20, ValRef, Train10, Train20, TrainRef, tmp_dir = dt.AutomaticPreProcessing(
    image_path,
    NSamples,
    True,
    True)



result = np.load(tmp_dir + os.sep +'result.npy')

pippo = dt.SaveResults(Test10,result,tmp_dir,image_path)
"""""""""
Test10 = dt.StdDeNormalization(Test10, tmp_dir, sel10=True)
Test10 = np.round(Test10)
Test10.astype('uint16')

result20 = dt.StdDeNormalization(result, tmp_dir)
result20 = np.round(result20)
result20.astype('uint16')

results = []
results = np.concatenate(
    [Test10[0, :, :, 0:3],
     result20[0, :, :, 0:3],
     np.expand_dims(Test10[ 0, :, :, 3], axis = 2),
     result20[0, :, :, 3:6]],
     axis = 2)
results = np.asarray(results)
results = np.moveaxis(results, -1, 0)
results.astype('uint16')

reference_path = tmp_dir + os.sep + 'S2_10_norm.tif'

ref = gdal.Open(reference_path)
name = image_path + os.sep + 'S2_Results.tif'
GeoT = ref.GetGeoTransform()
Proj = ref.GetProjection()
bands = 10
type = gdal.GDT_UInt16
driver = gdal.GetDriverByName('GTiff')

out = driver.Create(name, results.shape[2], results.shape[1], bands, type)
out.SetGeoTransform(GeoT)
out.SetProjection(Proj)

for i in range(results.shape[0]):
    out.GetRasterBand(i+1).WriteArray(results[i])

out.FlushCache()


"""""""""
