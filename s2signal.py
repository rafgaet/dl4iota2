from math import sqrt, log, floor
import numpy as np
from matplotlib.pyplot import imshow, figure
import glob
import otbApplication as otb
from scipy.ndimage import gaussian_filter
import os
import xml.etree.ElementTree as ET


def build_S2_Gaussian_filters(filter_size):
    GNyq10 = [0.275, 0.28, 0.25, 0.24]
    GNyq20 = [0.365, 0.33, 0.34, 0.32, 0.205, 0.235]

    N = filter_size;
    fcut = 1 / 2;

    gauss10 = []
    gauss20 = []

    for ii in range(len(GNyq10)):
        sigma = sqrt(((N - 1) * (fcut / 2)) ** 2 / (-2 * log(GNyq10[ii])))
        x = np.zeros((N, N))
        x[floor(N / 2), floor(N / 2)] = 1
        gauss10.append(gaussian_filter(x, sigma))

    for ii in range(len(GNyq20)):
        sigma = sqrt(((N - 1) * (fcut / 2)) ** 2 / (-2 * log(GNyq20[ii])))
        x = np.zeros((N, N))
        x[floor(N / 2), floor(N / 2)] = 1
        gauss20.append(gaussian_filter(x, sigma))

    gauss10str = []
    gauss20str = []

    for g in gauss10:
        gauss10str.append(
            '{ ' + np.array2string(g, precision=15, suppress_small=True, max_line_width=1000, separator=' , ').replace(
                '\n', ';').replace(',;', ';').replace('[', '').replace(']', '') + ' }')
    for g in gauss20:
        gauss20str.append(
            '{ ' + np.array2string(g, precision=15, suppress_small=True, max_line_width=1000, separator=' , ').replace(
                '\n', ';').replace(',;', ';').replace('[', '').replace(']', '') + ' }')

    gauss10 = np.dstack(gauss10)
    gauss20 = np.dstack(gauss20)

    return gauss10, gauss20, gauss10str, gauss20str

def readImageStats(stat_file):

    vals = []
    root = ET.parse(stat_file).getroot()
    for item in root.findall('Statistic'):
        for child in item:
            vals.append(float(child.attrib['value'].replace(',','.')))
    mns = np.array(vals[:int(len(vals) / 2)])
    sds = np.array(vals[int(len(vals) / 2):])

    return mns, sds


def normalize_stack(lst, stat_file, stack_file, mask_file=None):

    concat = otb.Registry.CreateApplication('ConcatenateImages')
    concat.SetParameterStringList('il', lst)
    concat.Execute()

    stat = otb.Registry.CreateApplication('ComputeImagesStatistics')

    if mask_file is not None and os.path.exists(mask_file):
        msk = otb.Registry.CreateApplication('BandMathX')
        msk.AddImageToParameterInputImageList('il',concat.GetParameterOutputImage('out'))
        msk.AddParameterStringList('il',mask_file)
        expr = '{' + ';'.join([f"im2b1==0?im1b{i+1}:-10000" for i in range(len(lst))]) + '}'
        msk.SetParameterString('exp',expr)
        msk.Execute()
        stat.AddImageToParameterInputImageList('il', msk.GetParameterOutputImage('out'))
    else:
        stat.AddImageToParameterInputImageList('il', concat.GetParameterOutputImage('out'))

    stat.SetParameterString('bv', '-10000')
    stat.SetParameterString('out', stat_file)
    stat.ExecuteAndWriteOutput()

    mns, sds = readImageStats(stat_file)

    expr = '{' + ';'.join(
        ['(im1b%s-%s)/%s' % (str(i), str(m), str(s)) for i, m, s in
         zip(range(1, mns.shape[0] + 1), mns, sds)]) + '}'

    bm = otb.Registry.CreateApplication('BandMathX')
    bm.AddImageToParameterInputImageList('il', concat.GetParameterOutputImage('out'))
    bm.SetParameterString('exp', expr)
    #bm.SetParameterString('out', stack_file)
    #bm.SetParameterOutputImagePixelType('out', otb.ImagePixelType_float)
    #bm.ExecuteAndWriteOutput()
    bm.Execute()
    arr = bm.GetVectorImageAsNumpyArray('out')

    return np.copy(arr)


# Following function only works on Moringa-ready S2 THEIA images
# Cloud masks (CLM) must be +1'd and placed together with FRE files
def create_normalized_s2_stack(image_folder, out_dir = None, create_mask=False):

    if out_dir is None:
        out_dir = image_folder + os.sep + 'tmp'
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    lst = sorted(glob.glob(image_folder + os.sep + '*FRE_B*'))
    lst[-1], lst[-2] = lst[-2], lst[-1]
    lst = lst[2:] + lst[:2]

    lst10 = lst[:3] + [lst[6]]
    lst20 = lst[3:6] + lst[7:]

    s10 = out_dir + os.sep + 'S2_10_norm.tif'
    s20 = out_dir + os.sep + 'S2_20_norm.tif'

    s10_stats = out_dir + os.sep + 'S2_10_stats.xml'
    s20_stats = out_dir + os.sep + 'S2_20_stats.xml'

    normalize_stack(lst10, s10_stats, s10)
    normalize_stack(lst20, s20_stats, s20)

    if create_mask:
        s20_mask = out_dir + os.sep + 'S2_20_mask.tif'
        msk = glob.glob(image_folder + os.sep + '*CLM_R2*')[0]
        bm = otb.Registry.CreateApplication('BandMath')
        bm.SetParameterStringList('il', [msk])
        bm.SetParameterString('exp', 'im1b1==1')
        bm.SetParameterString('out', s20_mask)
        bm.SetParameterOutputImagePixelType('out', otb.ImagePixelType_uint8)
        bm.ExecuteAndWriteOutput()
    
    return out_dir


def downgrade_s2(stack_folder, filter_size=7):
    
    s10 = stack_folder + os.sep + 'S2_10_norm.tif'
    s20 = stack_folder + os.sep + 'S2_20_norm.tif'
    s10_d20 = stack_folder + os.sep + 'S2_10_norm_d20.tif'
    s20_d40 = stack_folder + os.sep + 'S2_20_norm_d40.tif'
    #s20_tgt = tmp_dir + os.sep + 'S2_20_target.tif'

    # OTB solution
    _, _, gauss10exp, gauss20exp = build_S2_Gaussian_filters(filter_size)

    flt10 = ['dotpr( ' + gauss10exp[i] + ', im1b%dN%dx%d )' % (i+1, filter_size, filter_size) for i in range(4)]
    flt20 = ['dotpr( ' + gauss20exp[i] + ', im1b%dN%dx%d )' % (i+1, filter_size, filter_size) for i in range(6)]

    ref20 = s20
    ref40app = otb.Registry.CreateApplication('RigidTransformResample')
    ref40app.SetParameterString('in', s20)
    ref40app.SetParameterFloat('transform.type.id.scalex', 0.5)
    ref40app.SetParameterFloat('transform.type.id.scaley', 0.5)
    ref40app.Execute()
    ref40 = ref40app.GetParameterOutputImage('out')

    bmx = []

    # Cannot perform a filter bank in a single BandMathX run
    # (delivers all zeros). Working if done band by band.
    concat10 = otb.Registry.CreateApplication('ConcatenateImages')
    for i in range(len(flt10)):
        bmx.append(otb.Registry.CreateApplication('BandMathX'))
        bmx[-1].SetParameterStringList('il', [s10])
        bmx[-1].SetParameterString('exp', flt10[i])
        bmx[-1].Execute()
        concat10.AddImageToParameterInputImageList('il', bmx[-1].GetParameterOutputImage('out'))
    concat10.Execute()

    concat20 = otb.Registry.CreateApplication('ConcatenateImages')
    for i in range(len(flt20)):
        bmx.append(otb.Registry.CreateApplication('BandMathX'))
        bmx[-1].SetParameterStringList('il', [s20])
        bmx[-1].SetParameterString('exp', flt20[i])
        bmx[-1].Execute()
        concat20.AddImageToParameterInputImageList('il', bmx[-1].GetParameterOutputImage('out'))
    concat20.Execute()

    sup10 = otb.Registry.CreateApplication('Superimpose')
    sup10.SetParameterInputImage('inm', concat10.GetParameterOutputImage('out'))
    sup10.SetParameterString('inr', ref20)
    sup10.SetParameterString('interpolator', 'nn')

    sup10.SetParameterString('out', s10_d20)
    sup10.SetParameterOutputImagePixelType('out', otb.ImagePixelType_float)
    sup10.ExecuteAndWriteOutput()

    sup20 = otb.Registry.CreateApplication('Superimpose')
    sup20.SetParameterInputImage('inm', concat20.GetParameterOutputImage('out'))
    sup20.SetParameterInputImage('inr', ref40)
    sup20.SetParameterString('interpolator', 'nn')

    sup20.SetParameterString('out', s20_d40)
    sup20.SetParameterOutputImagePixelType('out', otb.ImagePixelType_float)
    sup20.ExecuteAndWriteOutput()

    msk20 = stack_folder + os.sep + 'S2_20_mask.tif'
    msk40 = stack_folder + os.sep + 'S2_20_d40_mask.tif'
    if os.path.exists(msk20):
        supm = otb.Registry.CreateApplication('Superimpose')
        supm.SetParameterString('inm', msk20)
        supm.SetParameterInputImage('inr', ref40)
        supm.SetParameterString('interpolator', 'nn')
        supm.SetParameterString('out', msk40)
        supm.SetParameterOutputImagePixelType('out', otb.ImagePixelType_uint8)
        supm.ExecuteAndWriteOutput()

    return


if __name__ == '__main__':
    print('Launching test...')
    out_fld = create_normalized_s2_stack('S2_test/S2-L2A-THEIA_0_20180206_47PQR', create_mask=True)
    downgrade_s2(out_fld)
    print('Done! Check "tmp" subdirectory.')
