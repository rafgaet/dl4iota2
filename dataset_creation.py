import tensorflow as tf
import tensorboard
import numpy as np
import glob
import os
import gdal
from osgeo import ogr
from tensorflow.keras import layers, losses, optimizers, activations, models, callbacks, backend
from sklearn.feature_extraction import image
from skimage.util import view_as_windows
from matplotlib import pyplot as plt

## Open Images Files
File_10 =[]
File_20 =[]

#geoTarray10 = []
#geoTarray20 = []

#path = os.path.curdir + os.sep + 'S2_test/S2-L2A-THEIA_0_20180206_47PQR/*.tif'
path = os.path.curdir + os.sep + 'S2_test/S2-L2A-THEIA_0_20180206_47PQR/*.tif'

for infile in sorted(glob.glob(path)):
    ds = gdal.Open(infile)
    buffer = ds.ReadAsArray()
    np.moveaxis(buffer, 0, -1)
#    geoT = ds.GetGeoTransform()
"""""""""
    if geoT[1] <15:
        File_10.append(buffer)
        if geoTarray10.__len__() == 0:
            geoTarray10 = geoT
    else:
        File_20.append(buffer)
        if geoTarray20.__len__() == 0:
            geoTarray20 = geoT

"""""""""
File_10 = np.asarray(File_10)
File_20 = np.asarray(File_20)


##Patches Application

Patches10 = []
Patches20 = []


## Setting OGR variables and open Vector

DriverName = "ESRI Shapefile"

driver = ogr.GetDriverByName(DriverName)
vectorDataPath = os.path.curdir + os.sep + 'S2_test/S2-L2A-THEIA_0_20180206_47PQR/*.shp'

centro = []

for infile in sorted(glob.glob(vectorDataPath)):
    dataSource = driver.Open(vectorDataPath, 0)
    if dataSource is None:
        print
        'Could not open %s' % (vectorDataPath)
    else:
        print('Opened %s' % (vectorDataPath))
        layer = dataSource.GetLayer()
        featureCount = layer.GetFeatureCount()
        print("Number of features in %s: %d" % (os.path.basename(vectorDataPath), featureCount))
        for feature in layer:
            geom = feature.GetGeometryRef()
            centro.append(geom.Centroid().ExportToWkt())


geoT = ds.GetGeoTransform()
geoTarray10.append(np.asarray(geoT))
geoTarray10.append(np.asarray(geoT))








for i in range(0, File_10.shape[0]):
    Patches10.append(image.extract_patches_2d(File_10[i], [66,66],  max_patches=1000, random_state=1000))

for i in range(0, File_20.shape[0]):
    Patches20.append(image.extract_patches_2d(File_20[i], [33,33],  max_patches=1000, random_state=1000))

Patches10 = np.asarray(Patches10)
Patches20 = np.asarray(Patches20)



##Pictures

S2_10_down = []
S2_20 = []
S2_20_down = []

S2_10_down = gdal.Open(os.path.curdir + os.sep + 'S2_test/S2_20_input.tif')
S2_20 = gdal.Open(os.path.curdir + os.sep + 'S2_test/S2_20_target.tif')
S2_20_down = gdal.Open(os.path.curdir + os.sep + 'S2_test/S2_40_input.tif')

S2_10_down = S2_10_down.ReadAsArray()
S2_20 = S2_20.ReadAsArray()
S2_20_down = S2_20_down.ReadAsArray()

patches_S2_10_down = np.reshape(np.moveaxis(S2_10_down,0,-1), (int(S2_10_down.shape[1]/66), 66, 66, 4))
patches_S2_20 = np.reshape(np.moveaxis(S2_20,0,-1), (int(S2_20.shape[1]/66), 66, 66, 6))
patches_S2_20_down = np.reshape(np.moveaxis(S2_20_down,0,-1), (int(S2_20_down.shape[1]/33), 33, 33, 6))

np.save(os.path.curdir + os.sep + 'S2_test/S2_10_down.npy', patches_S2_10_down)
np.save(os.path.curdir + os.sep + 'S2_test/S2_20.npy', patches_S2_20)
np.save(os.path.curdir + os.sep + 'S2_test/S2_20_down.npy', patches_S2_20_down)
