import os
import gdal
import ogr
import osr
import numpy as np
from math import ceil, floor
# from skimage.morphology import binary_erosion
from scipy.ndimage.morphology import distance_transform_cdt
import otbApplication as otb


def create_points_with_distance_constraint(n, shape, min_dist):
    # thanks Samir
    # https://stackoverflow.com/users/5231231/samir

    # specify params
    shape = np.roll(np.array(shape), 1)
    d = floor(min_dist / 2)

    # compute grid shape based on number of points
    width_ratio = shape[1] / shape[0]
    num_y = np.int32(np.sqrt(n / width_ratio)) + 1
    num_x = np.int32(n / num_y) + 1

    # create regularly spaced neurons
    x = np.linspace(d, shape[1] - d, num_x, dtype=np.float32)
    y = np.linspace(d, shape[0] - d, num_y, dtype=np.float32)
    coords = np.stack(np.meshgrid(x, y), -1).reshape(-1, 2)

    # compute spacing
    init_dist = np.min((x[1] - x[0], y[1] - y[0]))

    if init_dist <= min_dist:
        print('Grid too small for the requested number of patches, returning regular grid without distance constraint.')
        return np.round(coords).astype(np.int)

    # perturb points
    max_movement = floor((init_dist - min_dist) / 2)
    noise = np.random.uniform(
        low=-max_movement,
        high=max_movement,
        size=(len(coords), 2))
    coords += noise

    # Push points close to border (< d) to distance d for patching purposes
    coords[coords < d] = d
    coords[coords[:, 0] > shape[1] - d, 0] = shape[1] - d
    coords[coords[:, 1] > shape[0] - d, 1] = shape[0] - d

    return np.round(coords).astype(np.int)


def get_patch_centers(img_file, patch_size, n_patches, mask_file=None, out_vec=None, invert_mask=False, cls=1):
    ds = gdal.Open(img_file)
    shape_img = (ds.RasterYSize, ds.RasterXSize)
    ds = None
    npx_img = shape_img[0] * shape_img[1]

    if mask_file is not None:
        ds = gdal.Open(mask_file)
        arr = ds.ReadAsArray()
        if invert_mask:
            arr = 1 - arr
        ds = None
        # Solution with binary erosion more precise but less efficient
        # Taxicab distance gives very close results, excludes very few close-to-mask positions
        # sel = np.ones((patch_size, patch_size))
        # in_arr = binary_erosion(arr, selem=sel)
        in_arr = (distance_transform_cdt(arr) > patch_size / 2)
        npx_msk = np.sum(in_arr == 1)
    else:
        arr = np.ones(shape_img)
        in_arr = arr
        npx_msk = npx_img

    n_req_px = n_patches * (npx_img / npx_msk)
    coords = create_points_with_distance_constraint(ceil(n_req_px), shape_img, patch_size)

    out_coords = []

    q = floor(patch_size / 2)
    for c in coords:
        if q <= c[0] <= shape_img[0] - q and q <= c[1] <= shape_img[1] - q and in_arr[c[0], c[1]] == 1:
            out_coords.append(c)

    if out_vec is not None:
        srs = osr.SpatialReference()
        ds = gdal.Open(img_file)
        srs.ImportFromWkt(ds.GetProjection())
        geot = ds.GetGeoTransform()
        ds = None

        drv = ogr.GetDriverByName('ESRI Shapefile')
        ds = drv.CreateDataSource(out_vec)
        ly = ds.CreateLayer(os.path.splitext(os.path.basename(out_vec))[0], geom_type=ogr.wkbPoint, srs=srs)
        idfield = ogr.FieldDefn("id", ogr.OFTInteger)
        ly.CreateField(idfield)
        clsfield = ogr.FieldDefn("class", ogr.OFTInteger)
        ly.CreateField(clsfield)
        ld = ly.GetLayerDefn()

        id = 1

        for c in out_coords:
            x = geot[0] + geot[1] * c[1]
            y = geot[3] + geot[5] * c[0]
            wkt = 'POINT(%10.10f %10.10f)' % (x, y)
            ft = ogr.Feature(ld)
            ft.SetGeometry(ogr.CreateGeometryFromWkt(wkt))
            ft.SetField('id', id)
            ft.SetField('class', cls)
            ly.CreateFeature(ft)
            ft = None
            id += 1

        ds = None

    return np.asarray(out_coords)

def extract_patches(img_file, patch_size, patch_centers_vector, patches_out, field='cls'):

    ext = otb.Registry.CreateApplication('PatchesExtraction')
    ext.SetParameterStringList('source1.il', [img_file])
    ext.SetParameterString('source1.out', patches_out)
    ext.SetParameterInt('source1.patchsizex', patch_size)
    ext.SetParameterInt('source1.patchsizey', patch_size)
    ext.SetParameterString('vec', patch_centers_vector)
    ext.SetParameterString('field', field)
    ext.ExecuteAndWriteOutput()

    return patches_out

