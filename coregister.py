import warnings

from osgeo import gdal
import otbApplication as otb
import os
import tempfile

def coregister(src, ref, dst,
               src_band=3, src_ref=1,
               px_prec=3, init_step=256,
               min_points=40):
    temp_name = next(tempfile._get_candidate_names())
    temp_tif = os.path.join(os.path.dirname(src), temp_name + '.tif')
    temp_txt = os.path.join(os.path.dirname(src), temp_name + '.txt')

    ds_src = gdal.Open(src, gdal.GA_ReadOnly)
    ds_ref = gdal.Open(ref, gdal.GA_ReadOnly)

    src_srs_wkt = ds_src.GetSpatialRef().ExportToWkt()

    if (ds_src.GetSpatialRef().GetAuthorityCode('PROJCS') != ds_ref.GetSpatialRef().GetAuthorityCode('PROJCS') or
        ds_src.GetGeoTransform() != ds_ref.GetGeoTransform()):
        oRef = otb.Registry.CreateApplication('Superimpose')
        oRef.SetParameterString('inm', ref)
        oRef.SetParameterString('inr', src)
        oRef.Execute()
    else:
        oRef = otb.Registry.CreateApplication('ExtractROI')
        oRef.SetParameterString('in', ref)
        oRef.Execute()

    step = init_step
    gcp_file = temp_txt

    gcps = []
    while (len(gcps) < min_points and step > 1):
        gcps = []
        pm = otb.Registry.CreateApplication('HomologousPointsExtraction')
        pm.SetParameterString('in1', src)
        pm.SetParameterInt('band1', src_band)
        pm.SetParameterInputImage('in2', oRef.GetParameterOutputImage('out'))
        pm.SetParameterInt('band2', src_ref)
        pm.SetParameterInt('precision', px_prec)
        pm.SetParameterInt('mfilter', 1)
        pm.SetParameterInt('backmatching', 1)
        pm.SetParameterString('algorithm', 'sift')
        pm.SetParameterString('mode', 'geobins')
        pm.SetParameterInt('mode.geobins.binstep', step)
        pm.SetParameterString('out', gcp_file)
        pm.ExecuteAndWriteOutput()

        gt = ds_src.GetGeoTransform()
        with open(gcp_file, 'r') as f:
            x = f.readlines()
            for l in x:
                l = [float(v) for v in l.split('\n')[0].split('\t')]
                l[0] = (l[0] - gt[0]) / gt[1]
                l[1] = (l[1] - gt[3]) / gt[5]
                gcps.append(gdal.GCP(l[2], l[3], 0., l[0], l[1]))
        os.remove(temp_txt)

        step = int(step/2)

    if (len(gcps) < 40) :
        warnings.warn('On ' + src + ':\n Not enough GCPs to coregister.')
        return

    drv = gdal.GetDriverByName('GTiff')
    ds_tmp = drv.CreateCopy(temp_tif, ds_src)
    ds_tmp.SetGCPs(gcps, src_srs_wkt)

    resampler = gdal.GRA_Cubic
    err = 0.01

    ds_tmp_warped = gdal.AutoCreateWarpedVRT(ds_tmp,
                                             None,  # src_wkt : left to default value --> will use the one from source
                                             src_srs_wkt,
                                             resampler,
                                             err)

    ds_out = drv.Create(dst, ds_tmp_warped.RasterXSize, ds_tmp_warped.RasterYSize,
                        ds_src.RasterCount, ds_src.GetRasterBand(1).DataType)
    ds_out.SetProjection(ds_src.GetProjection())
    ds_out.SetGeoTransform(ds_tmp_warped.GetGeoTransform())
    ds_out.GetRasterBand(1).SetNoDataValue(ds_src.GetRasterBand(1).GetNoDataValue())

    gdal.ReprojectImage(ds_tmp,
                        ds_out,
                        None,
                        None,
                        resampler,
                        0,
                        err,
                        None,
                        None)

    ds_src = None
    ds_ref = None
    ds_tmp = None
    ds_tmp_warped = None
    ds_out = None

    os.remove(temp_tif)

    return

coregister('/DATA/tests_coreg/SRC.tif', '/DATA/tests_coreg/REF.tif', '/DATA/tests_coreg/COR.tif')
