from GapFilling.torch_gapfilling import *

n_epochs_gen = 4
n_epochs_spc = 10
n_patchs = 500
patch_size = 15

proc_name = '_'.join(['full', str(patch_size), str(n_epochs_gen), str(n_epochs_spc), str(n_patchs)])
print('[INFO] Launching processor ' + proc_name + '.')

'''
test_areas = {
    #format 'yyyymmdd': ( start_row, start_col, size_row, size_col )
    '20200921': (980, 1900, 300, 300),
    '20200703': (385, 470, 300, 300),
    '20200524': (1550, 850, 300, 300)
}
'''

bgp = BandwiseGapfillingProcessor('/DATA/Koumbia/Sample_2020/S2',
                                  use_s1='/DATA/Koumbia/Sample_2020/S1',
                                  dem='/DATA/Koumbia/Koumbia_DEM_SMALL.tif',
                                  bands=[0,1,2],
                                  patch_size=patch_size, n_patchs=n_patchs, n_epochs_gen=n_epochs_gen,
                                  n_epochs_spc=n_epochs_spc, max_side_img_cloudiness=0.05, verbose=1,
                                  gen_to_spec_sampling_ratio=0.25, proc_name=proc_name)#, test_areas=test_areas)

bgp.save()
