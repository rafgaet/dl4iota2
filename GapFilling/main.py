import tensorflow as tf
import numpy as np
from matplotlib import pyplot as plt
from GapFilling import GFNet
from GapFilling import dataset_utils as dt
from sklearn.metrics import mean_absolute_error
from math import floor

#img_path = os.path.curdir + "/SAR2NDVI_Set1"
img_path_01 = '/home/matteo/PycharmProjects/dl4iota2/GapFilling/dataset/01'
img_path_02 = '/home/matteo/PycharmProjects/dl4iota2/GapFilling/dataset/02'
img_path_03 = '/home/matteo/PycharmProjects/dl4iota2/GapFilling/dataset/03'
img_path_04 = '/home/matteo/PycharmProjects/dl4iota2/GapFilling/dataset/04'
img_path_05 = '/home/matteo/PycharmProjects/dl4iota2/GapFilling/dataset/05'
img_path_06 = '/home/matteo/PycharmProjects/dl4iota2/GapFilling/dataset/06'
img_path_07 = '/home/matteo/PycharmProjects/dl4iota2/GapFilling/dataset/07'
img_path_08 = '/home/matteo/PycharmProjects/dl4iota2/GapFilling/dataset/08'
img_path_09 = '/home/matteo/PycharmProjects/dl4iota2/GapFilling/dataset/09'
img_path_10 = '/home/matteo/PycharmProjects/dl4iota2/GapFilling/dataset/10'
N_samples = 6000

"""""""""
## Causale
B4_past_01, B4_reference_01, test_dataset_01, train_dataset_01, train_reference_01, B4_past_mean_01, B4_past_std_01 = \
   dt.DatasetCreation(img_path_01, N_samples)
B4_past_02, B4_reference_02, test_dataset_02, train_dataset_02, train_reference_02, B4_past_mean_02, B4_past_std_02 = \
   dt.DatasetCreation(img_path_02, N_samples)
B4_past_03, B4_reference_03, test_dataset_03, train_dataset_03, train_reference_03, B4_past_mean_03, B4_past_std_03 = \
   dt.DatasetCreation(img_path_03, N_samples)
B4_past_04, B4_reference_04, test_dataset_04, train_dataset_04, train_reference_04, B4_past_mean_04, B4_past_std_04 = \
   dt.DatasetCreation(img_path_04, N_samples)
B4_past_05, B4_reference_05, test_dataset_05, train_dataset_05, train_reference_05, B4_past_mean_05, B4_past_std_05 = \
   dt.DatasetCreation(img_path_05, N_samples)
B4_past_06, B4_reference_06, test_dataset_06, train_dataset_06, train_reference_06, B4_past_mean_06, B4_past_std_06 = \
   dt.DatasetCreation(img_path_06, N_samples)
B4_past_07, B4_reference_07, test_dataset_07, train_dataset_07, train_reference_07, B4_past_mean_07, B4_past_std_07 = \
   dt.DatasetCreation(img_path_07, N_samples)
B4_past_08, B4_reference_08, test_dataset_08, train_dataset_08, train_reference_08, B4_past_mean_08, B4_past_std_08 = \
   dt.DatasetCreation(img_path_08, N_samples)
B4_past_09, B4_reference_09, test_dataset_09, train_dataset_09, train_reference_09, B4_past_mean_09, B4_past_std_09 = \
   dt.DatasetCreation(img_path_09, N_samples)
B4_past_10, B4_reference_10, test_dataset_10, train_dataset_10, train_reference_10, B4_past_mean_10, B4_past_std_10 = \
   dt.DatasetCreation(img_path_10, N_samples)


# Non Causale
B4_past_01, B4_reference_01, B4_future_01, test_dataset_01, train_dataset_01, train_reference_01, B4_past_mean_01, \
B4_past_std_01 = dt.DatasetCreationWFuture(img_path_01, N_samples)
B4_past_02, B4_reference_02, B4_future_02, test_dataset_02, train_dataset_02, train_reference_02, B4_past_mean_02, \
B4_past_std_02 = dt.DatasetCreationWFuture(img_path_02, N_samples)
B4_past_03, B4_reference_03, B4_future_03, test_dataset_03, train_dataset_03, train_reference_03, B4_past_mean_03, \
B4_past_std_03 = dt.DatasetCreationWFuture(img_path_03, N_samples)
B4_past_04, B4_reference_04, B4_future_04, test_dataset_04, train_dataset_04, train_reference_04, B4_past_mean_04, \
B4_past_std_04 = dt.DatasetCreationWFuture(img_path_04, N_samples)
B4_past_05, B4_reference_05, B4_future_05, test_dataset_05, train_dataset_05, train_reference_05, B4_past_mean_05, \
B4_past_std_05 = dt.DatasetCreationWFuture(img_path_05, N_samples)
B4_past_06, B4_reference_06, B4_future_06, test_dataset_06, train_dataset_06, train_reference_06, B4_past_mean_06, \
B4_past_std_06 = dt.DatasetCreationWFuture(img_path_06, N_samples)
B4_past_07, B4_reference_07, B4_future_07, test_dataset_07, train_dataset_07, train_reference_07, B4_past_mean_07, \
B4_past_std_07 = dt.DatasetCreationWFuture(img_path_07, N_samples)
B4_past_08, B4_reference_08, B4_future_08, test_dataset_08, train_dataset_08, train_reference_08, B4_past_mean_08, \
B4_past_std_08 = dt.DatasetCreationWFuture(img_path_08, N_samples)
B4_past_09, B4_reference_09, B4_future_09, test_dataset_09, train_dataset_09, train_reference_09, B4_past_mean_09, \
B4_past_std_09 = dt.DatasetCreationWFuture(img_path_09, N_samples)
B4_past_10, B4_reference_10, B4_future_10, test_dataset_10, train_dataset_10, train_reference_10, B4_past_mean_10, \
B4_past_std_10 = dt.DatasetCreationWFuture(img_path_10, N_samples)

"""""""""""
##Multiband
B4_past_01, B4_reference_01, B4_future_01, test_dataset_01, train_dataset_01, train_reference_01, B4_past_mean_01, \
B4_past_std_01 = dt.DatasetCreationMultiBand(img_path_01, N_samples)
B4_past_02, B4_reference_02, B4_future_02, test_dataset_02, train_dataset_02, train_reference_02, B4_past_mean_02, \
B4_past_std_02 = dt.DatasetCreationMultiBand(img_path_02, N_samples)
B4_past_03, B4_reference_03, B4_future_03, test_dataset_03, train_dataset_03, train_reference_03, B4_past_mean_03, \
B4_past_std_03 = dt.DatasetCreationMultiBand(img_path_03, N_samples)
B4_past_04, B4_reference_04, B4_future_04, test_dataset_04, train_dataset_04, train_reference_04, B4_past_mean_04, \
B4_past_std_04 = dt.DatasetCreationMultiBand(img_path_04, N_samples)
B4_past_05, B4_reference_05, B4_future_05, test_dataset_05, train_dataset_05, train_reference_05, B4_past_mean_05, \
B4_past_std_05 = dt.DatasetCreationMultiBand(img_path_05, N_samples)
B4_past_06, B4_reference_06, B4_future_06, test_dataset_06, train_dataset_06, train_reference_06, B4_past_mean_06, \
B4_past_std_06 = dt.DatasetCreationMultiBand(img_path_06, N_samples)
B4_past_07, B4_reference_07, B4_future_07, test_dataset_07, train_dataset_07, train_reference_07, B4_past_mean_07, \
B4_past_std_07 = dt.DatasetCreationMultiBand(img_path_07, N_samples)
B4_past_08, B4_reference_08, B4_future_08, test_dataset_08, train_dataset_08, train_reference_08, B4_past_mean_08, \
B4_past_std_08 = dt.DatasetCreationMultiBand(img_path_08, N_samples)
B4_past_09, B4_reference_09, B4_future_09, test_dataset_09, train_dataset_09, train_reference_09, B4_past_mean_09, \
B4_past_std_09 = dt.DatasetCreationMultiBand(img_path_09, N_samples)
B4_past_10, B4_reference_10, B4_future_10, test_dataset_10, train_dataset_10, train_reference_10, B4_past_mean_10, \
B4_past_std_10 = dt.DatasetCreationMultiBand(img_path_10, N_samples)



"""""

train_dataset = np.concatenate([train_dataset_01, train_dataset_02, train_dataset_03, train_dataset_04,
                                train_dataset_05, train_dataset_06, train_dataset_07, train_dataset_08,
                                train_dataset_09, train_dataset_10])

train_reference = np.concatenate([train_reference_01, train_reference_02, train_reference_03, train_reference_04,
                                  train_reference_05, train_reference_06, train_reference_07, train_reference_08,
                                  train_reference_09, train_reference_10])
"""

train_number = 3000

train_dataset = np.concatenate([train_dataset_01[0:train_number], train_dataset_02[0:train_number], train_dataset_03[0:train_number], train_dataset_04[0:train_number],
                                train_dataset_05[0:train_number], train_dataset_06[0:train_number], train_dataset_07[0:train_number], train_dataset_08[0:train_number],
                                train_dataset_09[0:train_number], train_dataset_10[0:train_number]])

train_reference = np.concatenate([train_reference_01[0:train_number], train_reference_02[0:train_number], train_reference_03[0:train_number], train_reference_04[0:train_number],
                                  train_reference_05[0:train_number], train_reference_06[0:train_number], train_reference_07[0:train_number], train_reference_08[0:train_number],
                                  train_reference_09[0:train_number], train_reference_10[0:train_number]])


callback = GFNet.CustomCallbacks()

gfnet = GFNet.gapfillingnet_v1c (train_dataset)

rfsize = gfnet.compute_output_shape(train_dataset.shape)[1]

train_reference = dt.cut_patches_to(train_reference, rfsize)

gfnet.fit(
   x = train_dataset,
   y = train_reference,
   batch_size = 256,
   epochs = 1000,
   verbose = 1,
   callbacks = callback,
   shuffle = True,
   )


#tf.config.experimental.set_visible_devices([], 'GPU')

test_dataset = np.concatenate([test_dataset_01, test_dataset_02, test_dataset_03])
B4_past = np.concatenate([B4_past_01, B4_past_02, B4_past_03])
B4_reference = np.concatenate([B4_reference_01, B4_reference_02, B4_reference_03])
B4_past_mean = [B4_past_mean_01, B4_past_mean_02, B4_past_mean_03]
B4_past_std =  [B4_past_std_01, B4_past_std_02, B4_past_std_03]

topad = floor((train_dataset.shape[1]-rfsize)/2)
test_dataset_01 = np.pad(test_dataset_01,((0,0),(topad,topad),(topad,topad),(0,0)),mode='symmetric')
test_dataset_02 = np.pad(test_dataset_02,((0,0),(topad,topad),(topad,topad),(0,0)),mode='symmetric')
test_dataset_03 = np.pad(test_dataset_03,((0,0),(topad,topad),(topad,topad),(0,0)),mode='symmetric')

test_net_1 = GFNet.gapfillingnet_v1c (test_dataset_01, 1e-5)
test_net_2 = GFNet.gapfillingnet_v1c (test_dataset_01, 1e-5)
test_net_3 = GFNet.gapfillingnet_v1c (test_dataset_01, 1e-5)

#weights_path = '/home/matteo/PycharmProjects/dl4iota2/GapFilling/checkpoints/gfnet_2020-05-14_13-53-32/ckpt_1000.h5'
weights_path = GFNet.FindLastWeights()
test_net_1.load_weights(weights_path)
test_net_2.load_weights(weights_path)
test_net_3.load_weights(weights_path)


test_net_1.fit(
   x = train_dataset_01[train_number:train_dataset_01.shape[0]],
   y = dt.cut_patches_to(train_reference_01[train_number:train_dataset_01.shape[0]], rfsize),
   batch_size = 128,
   epochs = 500,
   verbose = 1,
   callbacks = callback,
   shuffle = True,
   )

test_net_2.fit(
   x = train_dataset_02[train_number:train_dataset_02.shape[0]],
   y = dt.cut_patches_to(train_reference_02[train_number:train_dataset_02.shape[0]], rfsize),
   batch_size = 128,
   epochs = 500,
   verbose = 1,
   callbacks = callback,
   shuffle = True,
   )

test_net_3.fit(
   x = train_dataset_03[train_number:train_dataset_03.shape[0]],
   y = dt.cut_patches_to(train_reference_03[train_number:train_dataset_03.shape[0]], rfsize),
   batch_size = 128,
   epochs = 500,
   verbose = 1,
   callbacks = callback,
   shuffle = True,
   )

result_01 = test_net_1.predict(test_dataset_01)
result_02 = test_net_2.predict(test_dataset_02)
result_03 = test_net_3.predict(test_dataset_03)

result = np.concatenate([result_01, result_02, result_03])

past_1 = B4_past[2]
past_2 = B4_past[3]

ref_1 = B4_reference[2]
ref_2 = B4_reference[3]

res_1 = result[2,:,:,0]
res_2 = result[3,:,:,0]

"""""""""""
plt.figure(1)
plt.imshow(past_1)

plt.figure(2)
plt.imshow(ref_1)

plt.figure(3)
plt.imshow(res_1)

#plt.figure(300)
#plt.imshow((past_1+ref_1)/2)

plt.figure(4)
plt.imshow(res_1-ref_1)

plt.figure(5)
plt.imshow(past_1, vmin=0, vmax=0.3)

plt.figure(6)
plt.imshow(ref_1, vmin=0, vmax=0.3)

plt.figure(7)
plt.imshow(res_1, vmin=0, vmax=0.3)

plt.figure(10)
plt.imshow(past_2)

plt.figure(20)
plt.imshow(ref_2)

plt.figure(30)
plt.imshow(res_2)

plt.figure(40)
plt.imshow(res_2-ref_2)

plt.figure(50)
plt.imshow(past_2, vmin=0, vmax=0.3)

plt.figure(60)
plt.imshow(ref_2, vmin=0, vmax=0.3)

plt.figure(70)
plt.imshow(res_2, vmin=0, vmax=0.3)
"""""


## De-Normalization
for i in range (0, len(B4_past_mean)):
   B4_past[i*2] = (B4_past[i*2] * B4_past_std[i]) + B4_past_mean[i]
   B4_reference[i*2] = (B4_reference[i*2] * B4_past_std[i]) + B4_past_mean[i]
   result[i*2] = (result[i*2] * B4_past_std[i]) + B4_past_mean[i]

for i in range (0, len(B4_past_mean)):
   B4_past[i*2+1] = (B4_past[i*2+1] * B4_past_std[i]) + B4_past_mean[i]
   B4_reference[i*2+1] = (B4_reference[i*2+1] * B4_past_std[i]) + B4_past_mean[i]
   result[i*2+1] = (result[i*2+1] * B4_past_std[i]) + B4_past_mean[i]


mae_1_1 = mean_absolute_error(B4_reference[0], result[0,:,:,0])

print (mae_1_1)

mae_1_2 = mean_absolute_error(B4_reference[1], result[1,:,:,0])

print (mae_1_2)

mae_2_1 = mean_absolute_error(B4_reference[2], result[2,:,:,0])

print (mae_2_1)

mae_2_2 = mean_absolute_error(B4_reference[3], result[3,:,:,0])

print (mae_2_2)

mae_3_1 = mean_absolute_error(B4_reference[4], result[4,:,:,0])

print (mae_3_1)

mae_3_2 = mean_absolute_error(B4_reference[5], result[5,:,:,0])

print (mae_3_2)

np.save('cut_past.npy', B4_past)
np.save('V1_MULTIBANDS_CAUSAL_3layer_wFINETUNING_MAGGIORATA_t1_MAE_LeakyReLu', result)
np.save('cut_ref.npy', B4_reference)


layer = test_net_3.layers[5]
layer_config = layer.get_config()
layer_weights = layer.get_weights()
weight = layer_weights[0]

energy_in = []
energy_out= []
for i in range (weight.shape[2]):
   buf = np.mean(np.linalg.norm(weight[:,:,i,:], ord=2, axis=2))
   energy_in.append(buf)

for i in range (weight.shape[3]):
   buf = np.mean(np.linalg.norm(weight[:,:,:,i], ord=2, axis=2))
   energy_out.append(buf)
max_in = np.max(energy_in)
max_out = np.max(energy_out)

energy_in = energy_in / max_in
energy_out = energy_out / max_out

out_true = energy_out > 0.3
count = 0
for i in range (len(out_true)):
   if out_true[i] == True:
      count= count + 1
