import os.path
import datetime

from .time_series_tools import *
from .torch_gdal_loader import *
from .torch_time_series_nets import *

import pickle

import glob

def get_s1_date(fn):
    # based on legacy Moringa's S1 Processor
    # return os.path.basename(fn).split('_')[1]

    # based on new Moringa's S1 Processor
    return os.path.basename(fn).split('-')[4].split('t')[0]

def get_date(fn):
    return os.path.basename(fn).split('_')[1][:8]

def get_name(fn):
    return os.path.basename(os.path.dirname(fn))

def get_basename(fn):
    return os.path.basename(fn)

class GapFillingMonoS2Processor:
    def __init__(self, time_series_folder, img_template, band=None, mask_template = '*BINARY_MASK.tif',
                 max_side_img_cloudiness=0.05, verbose=0, proc_name=None, test_areas=None):
        self.verbose = verbose
        self.time_series_folder = time_series_folder

        tmp_mask_list = glob.glob(os.path.join(time_series_folder, '*/' + mask_template))
        tmp_image_list = glob.glob(os.path.join(time_series_folder, '*/' + img_template))
        assert len(tmp_mask_list) == len(tmp_image_list)

        self.mask_list = []
        self.image_list = []
        for i in range(len(tmp_mask_list)):
            ds = gdal.Open(tmp_mask_list[i])
            bnd = ds.GetRasterBand(1)
            arr = bnd.ReadAsArray()
            bnd, ds = None, None
            if np.sum(arr==0) > 0:
                self.mask_list.append(tmp_mask_list[i])
                self.image_list.append(tmp_image_list[i])
            else:
                if self.verbose:
                    print('[WARN] Skipping ' + os.path.basename(tmp_image_list[i]) + ' with no valid pixels.')

        #self.mask_list = glob.glob(os.path.join(time_series_folder, '*/' + mask_template))
        self.mask_list = sorted(self.mask_list, key=lambda x: get_date(x))
        #self.image_list = glob.glob(os.path.join(time_series_folder, '*/' + img_template))
        self.image_list = sorted(self.image_list, key=lambda x: get_date(x))
        self.date_list = [datetime.datetime.strptime(get_date(x),'%Y%m%d') for x in self.image_list]
        self.band = band

        self.cloud_conf = get_cloudiness_configurations(self.mask_list, max_side_img_cloudiness)

        # Compute statistics over the whole time series
        full_ds = GDALFullPatchDataset(self.image_list, 1, band=band)
        self.stat_min, self.stat_max = full_ds.stats['min'], full_ds.stats['max']

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.ref_vector_list = None
        self.N = len(self.mask_list)
        self.patch_size = -1

        self.proc_name = proc_name
        if proc_name is None:
            self.aux_path = os.path.join(time_series_folder,'gapf_aux')
        else:
            self.aux_path = os.path.join(time_series_folder, 'gapf_aux_' + proc_name)
        if not os.path.exists(self.aux_path):
            os.makedirs(self.aux_path)
        self.gen_model = None
        self.spc_models = []
        self.spc_model_map = []
        self.gen_losses = None
        self.spc_losses = []

        ds = gdal.Open(self.image_list[0])
        self.geoT = ds.GetGeoTransform()
        self.proj = ds.GetProjection()
        self.data_type = ds.GetRasterBand(1).DataType
        ds = None

        self.processing_date = None

        self.test_areas = []
        if test_areas is not None:
            self.set_test_areas(test_areas)

        self.estimates = []
        self.originals = []
        self.RMSE = []
        self.MAE = []

    def set_test_areas(self, test_areas):
        for k, v in test_areas.items():
            dt = datetime.datetime.strptime(k, '%Y%m%d')
            idx = self.date_list.index(dt)
            self.test_areas.append((idx, v))

    def generate_reference_samples(self, patch_size, num_patches_per_img,
                                   split_dict = {'general': 0.25, 'finetune': 0.75}):

        if self.verbose:
            print('[INFO] Generating reference samples.')
        self.ref_vector_list = []
        self.ref_coverage = []
        self.patch_size = patch_size

        cmasks = get_sampling_masks(self.mask_list, self.cloud_conf, self.test_areas)

        # Generate reference samples for general and date-wise models
        for i in range(self.N):
            if cmasks[i] is not None: #maybe useless, get_patch_centers returns None if no valid patches available.
                coords, cvr, cvr_ext = get_patch_centers(self.mask_list[i], patch_size, num_patches_per_img, cmasks[i],
                                                         min_coverage=0.25, min_cov_extent=0.75)
                self.ref_coverage.append((cvr, cvr_ext))
                if coords is not None:
                    out_vec = os.path.join(self.aux_path, 'validPatches_' + get_name(self.mask_list[i]) + '.shp')
                    self.ref_vector_list.append(create_point_vector(coords, self.mask_list[i], out_vec,
                                                                    split_dict=split_dict))
                else:
                    if self.verbose:
                        print('[WARN] Skipping patch extraction from ' + self.image_list[i])
                        print('[WARN] with effective coverage ' + str(cvr) + ', cov. extent ' + str(cvr_ext))
                        self.ref_vector_list.append(None)
            else:
                self.ref_vector_list.append(None)
                self.ref_coverage.append(None)

        return

    def set_existing_reference_samples(self, patch_size, folder=None):
        if folder is None:
            folder = self.aux_path

        self.ref_vector_list = []
        self.patch_size = patch_size

        gen_lst = sorted(glob.glob(os.path.join(folder, 'validPatches_*_general.shp')),
                         key=lambda x:os.path.basename(x).split('_')[2][0:8])
        spc_lst = sorted(glob.glob(os.path.join(folder, 'validPatches_*_finetune.shp')),
                         key=lambda x:os.path.basename(x).split('_')[2][0:8])

        #cmasks = get_sampling_masks(self.mask_list, self.cloud_conf)

        for i in range(self.N):
            if len(gen_lst) > 0 and get_name(self.image_list[i]) in gen_lst[0]:
            #if cmasks[i] is not None:
                self.ref_vector_list.append([gen_lst.pop(0),spc_lst.pop(0)])
            else:
                self.ref_vector_list.append(None)

        return

    def get_training_data_set(self, i, specific=False):
        c = int(specific)
        bef, aft = self.cloud_conf[i][0], self.cloud_conf[i][1]
        ds = GDALSamplePatchDataset([self.image_list[bef], self.image_list[aft], self.image_list[i]],
                                    self.ref_vector_list[i][c],
                                    self.patch_size, band=self.band,
                                    normalize=[self.stat_min, self.stat_max])
        return ds

    def train_general_model(self, n_epochs, batch_size, learning_rate=1e-3, keep_existing=False):
        if self.verbose:
            if self.band is not None:
                print('[INFO] Training general model for band ' + str(self.band) + '.')
            else:
                print('[INFO] Training general model.')
        gen_ds_list = []
        gen_model_filename = 'general_model'
        if self.band is not None:
            gen_model_filename += '_band' + str(self.band)
        gen_model_path = os.path.join(self.aux_path, gen_model_filename + '.pth')
        self.gen_model = gen_model_path
        if keep_existing and os.path.exists(self.gen_model):
            return
        for i in range(self.N):
            if self.ref_vector_list[i] is not None:
                gen_ds_list.append(self.get_training_data_set(i))
        ds = GDALMultiSamplePatchDataset(gen_ds_list)

        # General model Training
        model,losses = S1S2GF_train(ds, n_epochs=n_epochs, batch_size=batch_size, lr=learning_rate)
        torch.save(model, gen_model_path)
        self.gen_losses = losses
        del model

    def train_specific_models(self, n_epochs, batch_size, learning_rate=1e-3, timely=True, keep_existing=False,
                              n_epochs_first_spc=None):
        first = True
        last_spc = None
        for i in range(self.N):
            if self.verbose:
                if self.band is not None:
                    print('[INFO] Training specific model for band ' + str(self.band) + ' at date ' + self.date_list[i].strftime('%Y%m%d') + '.')
                else:
                    print('[INFO] Training general model at date ' + self.date_list[i].strftime('%Y%m%d') + '.')
            bef,aft = self.cloud_conf[i][0], self.cloud_conf[i][1]
            if bef == -1 or aft == -1:
                self.spc_models.append(None)
                self.spc_model_map.append(last_spc)
                self.spc_losses.append(None)
                continue
            if timely and any(self.spc_models):
                curr_model = next(x for x in reversed(self.spc_models) if x is not None)
            else:
                curr_model = self.gen_model
            if self.ref_vector_list[i] is None:
                self.spc_models.append(curr_model)
                self.spc_model_map.append(last_spc)
                continue
            spc_model_filename = 'finetuned_model_' + get_name(self.image_list[i])
            if self.band is not None:
                spc_model_filename += '_band' + str(self.band)
            spc_model_path = os.path.join(self.aux_path, spc_model_filename + '.pth')
            self.spc_models.append(spc_model_path)
            self.spc_model_map.append(i)
            last_spc = i
            if keep_existing and os.path.exists(self.spc_models[-1]):
                continue
            spc_ds = self.get_training_data_set(i, True)
            # Specific model training (fine-tuning)
            if first and n_epochs_first_spc is not None:
                model,losses = S1S2GF_train(spc_ds, n_epochs=n_epochs_first_spc, batch_size=batch_size, lr=learning_rate,
                                     from_pretrained=curr_model)
                first = False
            else:
                model,losses = S1S2GF_train(spc_ds, n_epochs=n_epochs, batch_size=batch_size, lr=learning_rate,
                                     from_pretrained=curr_model)
            torch.save(model, spc_model_path)
            self.spc_losses.append(losses)
            del model

    def format_input_dataset(self, i, roi=(0,0,None,None)):
        bef, aft = self.cloud_conf[i][0], self.cloud_conf[i][1]
        ds_in_bef = gdal.Open(self.image_list[bef])
        ds_in_aft = gdal.Open(self.image_list[aft])
        if self.band is None:
            x_in_bef = ds_in_bef.ReadAsArray(xoff=roi[1], yoff=roi[0], xsize=roi[3], ysize=roi[2])
            x_in_aft = ds_in_aft.ReadAsArray(xoff=roi[1], yoff=roi[0], xsize=roi[3], ysize=roi[2])
        else:
            bnd_bef = ds_in_bef.GetRasterBand(self.band + 1)
            x_in_bef = bnd_bef.ReadAsArray(xoff=roi[1], yoff=roi[0], xsize=roi[3], ysize=roi[2])
            bnd_aft = ds_in_aft.GetRasterBand(self.band + 1)
            x_in_aft = bnd_aft.ReadAsArray(xoff=roi[1], yoff=roi[0], xsize=roi[3], ysize=roi[2])

        ds_in_bef, ds_in_aft, ds_msk, ds_ref = None, None, None, None

        # format input
        x_in = np.pad(np.array([[x_in_bef, x_in_aft]]), ((0, 0), (0, 0), (4, 4), (4, 4)), mode='symmetric')
        # norm
        x_in = (x_in - self.stat_min) / (self.stat_max - self.stat_min)

        return x_in

    def tiled_inference(self, i, tile_size=100, roi=(0,0,None,None)):
        if self.spc_models[i] is not None:
            model = torch.load(self.spc_models[i])
        else:
            model = torch.load(self.gen_model)
        x_in = self.format_input_dataset(i, roi)
        out_shape = (x_in.shape[2] - 8, x_in.shape[3] - 8)
        # Tiled inference
        y_out = np.empty(out_shape)
        for u in range(4, x_in.shape[2] - 4, tile_size):
            for v in range(4, x_in.shape[3] - 4, tile_size):
                x_in_tile = x_in[:, :, u - 4:u + tile_size + 4, v - 4:v + tile_size + 4]
                x_in_tile = torch.Tensor(x_in_tile).to(self.device)
                model.eval()
                with torch.no_grad():
                    y_out_tile = model(x_in_tile)
                y_out[u - 4:u + tile_size - 4, v - 4:v + tile_size - 4] = np.squeeze(y_out_tile.cpu().numpy())
        # de-norm
        y_out = (self.stat_max - self.stat_min) * y_out + self.stat_min
        # clamp negative values to 0
        y_out[y_out<0] = 0
        return y_out

    def get_closest_s2_dates(self, input_date):
        dts = [self.date_list.index(min(self.date_list, key=lambda x:abs(x - input_date))),
               self.date_list.index(min(self.date_list, key=lambda x:abs(x - input_date) if (x - input_date) < datetime.timedelta(0) else datetime.timedelta(1e8))),
               self.date_list.index(min(self.date_list, key=lambda x:abs(x - input_date) if (x - input_date) > datetime.timedelta(0) else datetime.timedelta(1e8)))]
        val = [self.cloud_conf[x][0] != -1 and self.cloud_conf[x][1] != -1 for x in dts]
        return dts, val

    def get_available_date_models(self):
        lst = sorted(set([self.date_list[i] for i in self.spc_model_map if i is not None]))
        return [datetime.datetime.strftime(x, '%Y%m%d') for x in lst]

    def get_closest_s2_date_models(self, input_date):
        model_date_list = sorted(set([self.date_list[i] for i in self.spc_model_map if i is not None]))
        dts = [self.date_list.index(min(model_date_list, key=lambda x:abs(x - input_date))),
               self.date_list.index(min(model_date_list, key=lambda x:abs(x - input_date) if (x - input_date) < datetime.timedelta(0) else datetime.timedelta(1e8))),
               self.date_list.index(min(model_date_list, key=lambda x:abs(x - input_date) if (x - input_date) > datetime.timedelta(0) else datetime.timedelta(1e8)))]
        val = [self.cloud_conf[x][0] != -1 and self.cloud_conf[x][1] != -1 for x in dts]
        return dts, val

    def check_date_models(self, input_dates):
        model_date_list = sorted(set([self.date_list[i] for i in self.spc_model_map if i is not None]))
        dts, val = [], []
        for d in input_dates:
            dd = datetime.datetime.strptime(d, '%Y%m%d')
            if dd in model_date_list:
                x = self.date_list.index(dd)
                dts.append(x)
                val.append(self.cloud_conf[x][0] != -1 and self.cloud_conf[x][1] != -1)
            else:
                print('[WARN] Requested model at date ' + d + ' does not exist.')
        return dts, val


    def get_estimate(self, est_date, tile_size=100, use_models=None):
        # Found closest dates
        est_date = datetime.datetime.strptime(est_date, '%Y%m%d')
        self.processing_date = est_date
        if self.verbose:
            print('[INFO] Requested image at date ' + est_date.strftime('%Y%m%d'))

        # dts,val = self.get_closest_s2_dates(est_date)
        # probably we won't need the spc_model_map anymore (dts already contains "model" dates)
        if use_models is None:
            dts, val = self.get_closest_s2_date_models(est_date)
            if self.date_list[dts[0]] == est_date and val[0]:
                if self.verbose:
                    print(
                        '[INFO] Found exact date model. Computing estimate at date ' + self.date_list[dts[0]].strftime('%Y%m%d') + '.')
                y_out = self.tiled_inference(dts[0], tile_size)
            else:
                dates_to_avg = [dts[x] for x in range(1,3) if val[x]]
                # weights = [1/abs((est_date - self.date_list[self.spc_model_map[dts[x]]]).days) for x in range(1,3) if val[x]]
                weights = [1 / abs((est_date - self.date_list[dts[x]]).days) for x in range(1, 3) if val[x]]
                y_to_avg = []
                if self.verbose:
                    print('[INFO] Required date not available on source. Providing model average.')
                for k in range(len(dates_to_avg)):
                    i = dates_to_avg[k]
                    if self.verbose:
                        print('[INFO] Using model from ' + self.date_list[i].strftime('%Y%m%d') +
                              ' with weight ' + str(weights[k]) + '.')
                    y_to_avg.append(self.tiled_inference(i, tile_size))
                y_out = np.average(np.array(y_to_avg), axis=0, weights=weights)
        else:
            dts, val = self.check_date_models(use_models)
            dates_to_avg = [dts[x] for x in range(len(dts)) if val[x]]
            if len(dates_to_avg) > 0:
                weights = [1 / abs((est_date - self.date_list[dts[x]]).days) for x in range(len(dts)) if val[x]]
                y_to_avg = []
                for k in range(len(dates_to_avg)):
                    i = dates_to_avg[k]
                    if self.verbose:
                        print('[INFO] Using model from ' + self.date_list[i].strftime('%Y%m%d') +
                              ' with weight ' + str(weights[k]) + '.')
                    y_to_avg.append(self.tiled_inference(i, tile_size))
                y_out = np.average(np.array(y_to_avg), axis=0, weights=weights)
            else:
                raise Exception('[ERROR] No available model found at given dates.')

        self.processing_date = None

        return y_out

    def compute_test_estimates(self, tile_size=100):
        self.estimates = []
        self.originals = []
        self.RMSE = []
        self.MAE = []
        for ta in self.test_areas:
            i, roi = ta[0], ta[1]
            self.processing_date = self.date_list[i]
            ds_msk = gdal.Open(self.mask_list[i])
            ds_ref = gdal.Open(self.image_list[i])
            msk = 1 - ds_msk.ReadAsArray(xoff=roi[1], yoff=roi[0], xsize=roi[3],ysize=roi[2])
            if self.band is None:
                ref = ds_ref.ReadAsArray(xoff=roi[1], yoff=roi[0], xsize=roi[3],ysize=roi[2])
            else:
                bnd_ref = ds_ref.GetRasterBand(self.band + 1)
                ref = bnd_ref.ReadAsArray(xoff=roi[1], yoff=roi[0], win_xsize=roi[3], win_ysize=roi[2])
            y_out = self.tiled_inference(i, roi=roi)

            if np.sum(msk) > 0:
                self.RMSE.append(np.sqrt(np.sum(((ref - y_out) ** 2)[msk == 1]) / np.sum(msk)))
                self.MAE.append(np.sum(np.abs((ref - y_out))[msk == 1]) / np.sum(msk))

            self.processing_date = None
            self.estimates.append(y_out)
            self.originals.append(ref)

        return


class GapFillingMonoS1S2Processor(GapFillingMonoS2Processor):
    def __init__(self, s2_time_series_folder, s1_time_series_folder, img_template, s1_img_template, band=None,
                 mask_template='*BINARY_MASK.tif', dem=None, max_side_img_cloudiness=0.05, verbose=0, proc_name=None,
                 test_areas=None, force_s1_date_as_model=False):
        super().__init__(s2_time_series_folder, img_template, band=band, mask_template=mask_template,
                         verbose=verbose, proc_name=proc_name, test_areas=test_areas)
        self.s1_time_series_folder = s1_time_series_folder
        self.s1_image_list = glob.glob(os.path.join(s1_time_series_folder, s1_img_template))
        self.s1_image_list = sorted(self.s1_image_list, key=lambda x: get_s1_date(x))
        self.s1_date_as_model = force_s1_date_as_model

        self.s1_date_list = [datetime.datetime.strptime(get_s1_date(x), '%Y%m%d') for x in self.s1_image_list]
        self.s2_s1_map = [min(range(len(self.s1_date_list)), key=lambda t:abs(self.s1_date_list[t]-x)) for x in self.date_list]

        self.dem, self.dem_min, self.dem_max = None, None, None
        if dem is not None:
            if not os.path.exists(dem):
                import sys
                sys.exit('No DEM at the provided path : ' + dem)
            self.dem = dem
            full_dem_ds = GDALFullPatchDataset([self.dem], 1)
            self.dem_min, self.dem_max = full_dem_ds.stats['min'], full_dem_ds.stats['max']

        # Compute statistics over the s1 time series
        full_s1_ds = GDALFullPatchDataset(self.s1_image_list, 1)
        self.s1_stat_min, self.s1_stat_max = full_s1_ds.stats['min'], full_s1_ds.stats['max']

    def get_closest_s1_date(self, input_date):
        return self.s1_date_list.index(min(self.s1_date_list, key=lambda x: abs(x - input_date)))

    def get_training_data_set(self, i, specific=False):
        c = int(specific)
        bef, aft = self.cloud_conf[i][0], self.cloud_conf[i][1]
        ds_s2 = GDALSamplePatchDataset([self.image_list[bef], self.image_list[aft],self.image_list[i]],
                                        self.ref_vector_list[i][c],
                                        self.patch_size, band=self.band,
                                        normalize=[self.stat_min, self.stat_max])
        ds_s1 = GDALSamplePatchDataset([self.s1_image_list[self.s2_s1_map[bef]],
                                        self.s1_image_list[self.s2_s1_map[i]],
                                        self.s1_image_list[self.s2_s1_map[aft]]],
                                        self.ref_vector_list[i][c],
                                        self.patch_size,
                                        normalize=[self.s1_stat_min, self.s1_stat_max],
                                        as_stack=True)
        if self.dem is not None:
            ds_dem = GDALSamplePatchDataset([self.dem],
                                            self.ref_vector_list[i][c],
                                            self.patch_size,
                                            normalize=[self.dem_min, self.dem_max])
            ds = GDALMultiSamplePatchDataset([ds_dem, ds_s1, ds_s2], axis=1)
        else:
            ds = GDALMultiSamplePatchDataset([ds_s1, ds_s2], axis=1)
        return ds

    def format_input_dataset(self, i, roi=(0,0,None,None)):
        bef, aft = self.cloud_conf[i][0], self.cloud_conf[i][1]

        # S2
        ds_in_bef = gdal.Open(self.image_list[bef])
        ds_in_aft = gdal.Open(self.image_list[aft])
        if self.band is None:
            x_in_bef = ds_in_bef.ReadAsArray(xoff=roi[1], yoff=roi[0], xsize=roi[3], ysize=roi[2])
            x_in_aft = ds_in_aft.ReadAsArray(xoff=roi[1], yoff=roi[0], xsize=roi[3], ysize=roi[2])
        else:
            bnd_bef = ds_in_bef.GetRasterBand(self.band + 1)
            x_in_bef = bnd_bef.ReadAsArray(xoff=roi[1], yoff=roi[0], win_xsize=roi[3], win_ysize=roi[2])
            bnd_aft = ds_in_aft.GetRasterBand(self.band + 1)
            x_in_aft = bnd_aft.ReadAsArray(xoff=roi[1], yoff=roi[0], win_xsize=roi[3], win_ysize=roi[2])

        ds_in_bef, ds_in_aft = None, None
        x_in_bef = (x_in_bef - self.stat_min) / (self.stat_max - self.stat_min)
        x_in_aft = (x_in_aft - self.stat_min) / (self.stat_max - self.stat_min)

        # S1
        ds_in_bef = gdal.Open(self.s1_image_list[self.s2_s1_map[bef]])
        if self.s1_date_as_model:
            dtr = self.s2_s1_map[i]
        else:
            dtr = self.get_closest_s1_date(self.processing_date)
        if self.verbose:
            print('[INFO] Using S1 date ' + self.s1_date_list[dtr].strftime('%Y%m%d') + '.')
        ds_in_pres = gdal.Open(self.s1_image_list[dtr])
        ds_in_aft = gdal.Open(self.s1_image_list[self.s2_s1_map[aft]])
        v_in_bef = ds_in_bef.ReadAsArray(xoff=roi[1], yoff=roi[0], xsize=roi[3], ysize=roi[2]).astype(float)
        v_in_pres = ds_in_pres.ReadAsArray(xoff=roi[1], yoff=roi[0], xsize=roi[3], ysize=roi[2]).astype(float)
        v_in_aft = ds_in_aft.ReadAsArray(xoff=roi[1], yoff=roi[0], xsize=roi[3], ysize=roi[2]).astype(float)
        for k in range(v_in_pres.shape[0]):
            v_in_bef[k] = (v_in_bef[k] - self.s1_stat_min[k]) / (self.s1_stat_max[k] - self.s1_stat_min[k])
            v_in_pres[k] = (v_in_pres[k] - self.s1_stat_min[k]) / (self.s1_stat_max[k] - self.s1_stat_min[k])
            v_in_aft[k] = (v_in_aft[k] - self.s1_stat_min[k]) / (self.s1_stat_max[k] - self.s1_stat_min[k])

        #DEM
        if self.dem is not None:
            ds_dem = gdal.Open(self.dem)
            dem_in = ds_dem.ReadAsArray(xoff=roi[1], yoff=roi[0], xsize=roi[3], ysize=roi[2])
            dem_in = (dem_in - self.dem_min) / (self.dem_max - self.dem_min)
            tmp_in = np.concatenate([np.expand_dims(dem_in, axis=0),
                                     v_in_bef,
                                     v_in_pres,
                                     v_in_aft,
                                     np.expand_dims(x_in_bef, axis=0),
                                     np.expand_dims(x_in_aft, axis=0)], axis=0)
        else:
            tmp_in = np.concatenate([v_in_bef,
                                     v_in_pres,
                                     v_in_aft,
                                     np.expand_dims(x_in_bef, axis=0),
                                     np.expand_dims(x_in_aft, axis=0)], axis=0)

        # format input
        x_in = np.pad(np.array([tmp_in]), ((0, 0), (0, 0), (4, 4), (4, 4)), mode='symmetric')

        return x_in


class BandwiseGapfillingProcessor:

    def __init__(self, time_series_folder, img_template = '*_FRE_STACK.tif',
                 mask_template = '*BINARY_MASK.tif', use_s1=None, s1_template='*feat.tif', dem=None,
                 bands=[0,1,2,6], n_patchs=5000, n_epochs_gen=50, n_epochs_spc=200,
                 patch_size=15, batch_size=64, gen_learning_rate=1e-3, spc_learning_rate=1e-4,
                 gen_to_spec_sampling_ratio=0.25, n_epochs_first_spc=None, max_side_img_cloudiness=0.05,
                 verbose=0, proc_name=None, test_areas=None):

        if os.path.isfile(time_series_folder):
            with open(time_series_folder, 'rb') as fid:
                tmp_dict = pickle.load(fid)
                self.__dict__.update(tmp_dict)
        else:
            self.gpfs = []

            ref_exists = False
            for b in bands:
                if use_s1 is not None and os.path.exists(use_s1):
                    self.gpfs.append(GapFillingMonoS1S2Processor(time_series_folder, use_s1, img_template, s1_template,
                                                                 band=b, mask_template=mask_template, dem=dem,
                                                                 max_side_img_cloudiness=max_side_img_cloudiness,
                                                                 verbose=verbose, proc_name=proc_name,
                                                                 test_areas=test_areas))
                else:
                    self.gpfs.append(
                        GapFillingMonoS2Processor(time_series_folder, img_template, band=b, mask_template=mask_template,
                                                  max_side_img_cloudiness=max_side_img_cloudiness,
                                                  verbose=verbose, proc_name=proc_name, test_areas=test_areas))
                if not ref_exists:
                    self.gpfs[-1].generate_reference_samples(patch_size, n_patchs,
                                                             split_dict={'general':gen_to_spec_sampling_ratio,
                                                                         'finetune':1-gen_to_spec_sampling_ratio})
                    ref_exists = True
                else:
                    self.gpfs[-1].set_existing_reference_samples(patch_size)
                if n_epochs_gen > 0:
                    self.gpfs[-1].train_general_model(n_epochs_gen, batch_size, gen_learning_rate)
                self.gpfs[-1].train_specific_models(n_epochs_spc, batch_size, spc_learning_rate, n_epochs_first_spc=n_epochs_first_spc)

            self.estimates = []
            self.originals = []
            self.RMSE = []
            self.MAE = []
            self.RMSE_tot = []
            self.MAE_tot = []

    def save(self, fn=None):
        if fn is None:
            fn = os.path.join(self.gpfs[0].time_series_folder, self.gpfs[0].proc_name + '.pkl')
        with open(fn, 'wb') as fid:
            pickle.dump(self.__dict__, fid, protocol=pickle.HIGHEST_PROTOCOL)

    def set_test_areas(self, test_areas):
        [x.set_test_areas(test_areas) for x in self.gpfs]
        return

    def compute_test_estimates(self):
        self.estimates = []
        self.originals = []
        self.RMSE = []
        self.MAE = []
        self.RMSE_tot = []
        self.MAE_tot = []
        [x.compute_test_estimates() for x in self.gpfs]
        for ta in range(len(self.gpfs[0].test_areas)):
            self.estimates.append(np.moveaxis(np.array([x.estimates[ta] for x in self.gpfs]), 0, -1))
            self.originals.append(np.moveaxis(np.array([x.originals[ta] for x in self.gpfs]), 0, -1))
            self.RMSE.append([x.RMSE[ta] for x in self.gpfs])
            self.MAE.append([x.MAE[ta] for x in self.gpfs])
            self.RMSE_tot.append(np.sqrt(np.mean(np.array(self.RMSE[ta])**2)))
            self.MAE_tot.append(np.mean(np.array(self.MAE[ta])))

    def get_available_date_models(self):
        return self.gpfs[0].get_available_date_models()

    def single_date_gapfill(self, dt, output_folder, stack_name=None, use_models=None):
        fname = os.path.splitext(os.path.basename(self.gpfs[0].image_list[0]))[0].split('_')
        fname[0] = 'SENTINEL2X'
        fname[1] = dt + '-GAPFILLED'
        if stack_name is not None:
            fname.append(stack_name)
        fname = os.path.join(output_folder, '_'.join(fname)) + '.tif'

        ests = []
        for g in self.gpfs:
            ests.append(g.get_estimate(dt, use_models=use_models))

        drv = gdal.GetDriverByName('GTiff')
        ds_out = drv.Create(fname, ests[0].shape[1], ests[0].shape[0], len(ests), self.gpfs[0].data_type)
        ds_out.SetProjection(self.gpfs[0].proj)
        ds_out.SetGeoTransform(self.gpfs[0].geoT)
        for i in range(len(ests)):
            ds_out.GetRasterBand(i + 1).WriteArray(ests[i])
        ds_out = None

    def gapfill(self, output_folder, output_dates=None, stack_name=None):
        if output_dates is None:
            output_dates = [x.strftime('%Y%m%d') for x in self.gpfs[0].date_list]

        if not os.path.exists(output_folder):
            os.makedirs(output_folder)
        for dt in output_dates:
            self.single_date_gapfill(dt, output_folder, stack_name)

    def get_losses(self):
        return [[x.gen_losses] + x.spc_losses for x in self.gpfs]
