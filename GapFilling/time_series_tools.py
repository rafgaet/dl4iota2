import os
from osgeo import gdal
from osgeo import ogr
from osgeo import osr
from math import ceil, floor
import numpy as np
from scipy.ndimage.morphology import distance_transform_cdt
from skimage.measure import find_contours
import otbApplication as otb
import glob
import subprocess

def get_cloudiness_configurations(mask_list, threshold=0.1):
    cloud_perc = []
    for f in mask_list:
        ds = gdal.Open(f)
        arr = ds.ReadAsArray()
        cloud_perc.append(np.sum(arr == 1) / (arr.shape[0]*arr.shape[1]))
        ds = None
    cloud_config = []
    for i in range(len(mask_list)):
        tmp = [t for t in range(i) if cloud_perc[t] < threshold]
        before = tmp[-1] if len(tmp) > 0 else -1
        tmp = [t for t in range(i+1, len(cloud_perc)) if cloud_perc[t] < threshold]
        after = tmp[0] if len(tmp) > 0 else -1
        cloud_config.append((before, after))
    return cloud_config

def get_sampling_masks(mask_list, cl_conf, force_masked_rois=None):
    combined_masks = []
    for i in range(len(mask_list)):
        if cl_conf[i][0] == -1 or cl_conf[i][1] == -1:
            combined_masks.append(None)
        else:
            ds = gdal.Open(mask_list[cl_conf[i][0]])
            arr_before = ds.ReadAsArray().astype(np.uint8)
            ds = gdal.Open(mask_list[i])
            arr_current = ds.ReadAsArray().astype(np.uint8)
            ds = gdal.Open(mask_list[cl_conf[i][1]])
            arr_after = ds.ReadAsArray().astype(np.uint8)
            comb = (1-arr_before)*(1-arr_current)*(1-arr_after)
            if isinstance(force_masked_rois, list):
                idx_list = [x[0] for x in force_masked_rois]
                if i in idx_list:
                    t = idx_list.index(i)
                    comb[force_masked_rois[t][1][0]:force_masked_rois[t][1][0]+force_masked_rois[t][1][2],
                         force_masked_rois[t][1][1]:force_masked_rois[t][1][1]+force_masked_rois[t][1][3]] = 0
            if np.sum(comb) == 0: # or np.sum(comb) == arr_current.shape[0] * arr_current.shape[1]:
                combined_masks.append(None)
            else:
                combined_masks.append(comb)
    return combined_masks

def distance_constrained_2d_sampling(n, shape, min_dist):
    # thanks Samir
    # https://stackoverflow.com/users/5231231/samir

    # specify params
    shape = np.roll(np.array(shape), 1)
    d = floor(min_dist / 2)

    # compute grid shape based on number of points
    width_ratio = shape[1] / shape[0]
    num_y = np.int32(np.sqrt(n / width_ratio)) + 1
    num_x = np.int32(n / num_y) + 1

    # create regularly spaced neurons
    x = np.linspace(d, shape[1] - d, num_x, dtype=np.float32)
    y = np.linspace(d, shape[0] - d, num_y, dtype=np.float32)
    coords = np.stack(np.meshgrid(x, y), -1).reshape(-1, 2)

    # compute spacing
    init_dist = np.min((x[1] - x[0], y[1] - y[0]))

    if init_dist <= min_dist:
        print(
            '[INFO] Grid too small for the requested number of patches, returning regular grid without distance constraint.')
        return np.round(coords).astype(np.int)

    # perturb points
    max_movement = floor((init_dist - min_dist) / 2)
    noise = np.random.uniform(
        low=-max_movement,
        high=max_movement,
        size=(len(coords), 2))
    coords += noise

    # Push points close to border (< d) to distance d for patching purposes
    coords[coords < d] = d
    coords[coords[:, 0] > shape[1] - d, 0] = shape[1] - d
    coords[coords[:, 1] > shape[0] - d, 1] = shape[0] - d

    return np.round(coords).astype(np.int)

def get_patch_centers(ref_raster_file, patch_size, n_patches, mask = None, min_coverage=0, min_cov_extent=0):
    ds = gdal.Open(ref_raster_file)
    shape_img = (ds.RasterYSize, ds.RasterXSize)
    ds = None
    npx_img = shape_img[0] * shape_img[1]

    if mask is not None and np.sum(mask==0) > 0:
        # Solution with binary erosion more precise but less efficient
        # Taxicab distance gives very close results, excludes very few close-to-mask positions
        # sel = np.ones((patch_size, patch_size))
        # in_arr = binary_erosion(arr, selem=sel)
        in_arr = (distance_transform_cdt(mask) > patch_size / 2)
        npx_msk = np.sum(in_arr == 1)
        coverage = (npx_msk / npx_img)
        cnt = find_contours(np.pad(in_arr,(1,1)),0)
        if len(cnt) > 0:
            bbox = (np.min(np.array([np.min(c,axis=0) for c in cnt]), axis=0),
                np.max(np.array([np.max(c,axis=0) for c in cnt]), axis=0))
            cov_extent = (bbox[1][0]-bbox[0][0]) * (bbox[1][1]-bbox[0][1]) / npx_img
        else:
            cov_extent = 0
    else:
        arr = np.ones(shape_img)
        in_arr = arr
        npx_msk = npx_img
        coverage, cov_extent = 1, 1

    # approximate compensation for "filtered" centers --> OLD
    # n_patches = floor(1.1 * n_patches)

    if coverage > min_coverage and cov_extent > min_cov_extent:
        n_req_px = n_patches * (npx_img / npx_msk)
        coords = distance_constrained_2d_sampling(ceil(n_req_px), shape_img, patch_size)

        out_coords = []

        q = floor(patch_size / 2)
        for c in coords:
            if q <= c[0] <= shape_img[0] - q and q <= c[1] <= shape_img[1] - q and in_arr[c[0], c[1]] == 1:
                #arr[c[0] - q:c[0] - q + patch_size, c[1] - q:c[1] - q + patch_size] = 2
                #arr[c[0], c[1]] = 3
                out_coords.append(c)

        return np.asarray(out_coords), coverage, cov_extent
    else:
        return None, coverage, cov_extent

def create_point_vector(coords, ref_raster_file, out_vector_file, split_dict=None):
    srs = osr.SpatialReference()
    ds = gdal.Open(ref_raster_file)
    srs.ImportFromWkt(ds.GetProjection())
    geot = ds.GetGeoTransform()
    ds = None

    ds_dict = {}
    ly_dict = {}
    ld_dict = {}
    drv = ogr.GetDriverByName('ESRI Shapefile')
    idfield = ogr.FieldDefn("id", ogr.OFTInteger)

    out_files = []

    if split_dict is None:
        out_files.append(out_vector_file)
        ds_dict['all'] = drv.CreateDataSource(out_vector_file)
        ly_dict['all'] = ds_dict['all'].CreateLayer(os.path.splitext(os.path.basename(out_vector_file))[0], geom_type=ogr.wkbPoint, srs=srs)
        ly_dict['all'].CreateField(idfield)
        ld_dict['all'] = ly_dict['all'].GetLayerDefn()
    else:
        assert(np.sum(list(split_dict.values())) == 1)
        cum_prob = np.cumsum(list(split_dict.values()))
        for k in split_dict.keys():
            tmp_file = os.path.splitext(out_vector_file)[0] + '_' + k + '.shp'
            out_files.append(tmp_file)
            ds_dict[k] = drv.CreateDataSource(tmp_file)
            ly_dict[k] = ds_dict[k].CreateLayer(os.path.splitext(os.path.basename(tmp_file))[0], geom_type=ogr.wkbPoint, srs=srs)
            ly_dict[k].CreateField(idfield)
            ld_dict[k] = ly_dict[k].GetLayerDefn()

    id = 1

    layers = list(ly_dict.values())
    lds = list(ld_dict.values())

    for c in coords:
        if split_dict is None:
            ly = layers[0]
            ld = lds[0]
        else:
            seed = np.random.random()
            idx = next(i for i in range(len(layers)) if cum_prob[i] > seed)
            ly = layers[idx]
            ld = lds[idx]

        x = geot[0] + geot[1] * c[1]
        y = geot[3] + geot[5] * c[0]
        wkt = 'POINT(%10.10f %10.10f)' % (x, y)
        ft = ogr.Feature(ld)
        ft.SetGeometry(ogr.CreateGeometryFromWkt(wkt))
        ft.SetField('id', id)
        ly.CreateFeature(ft)
        ft = None
        id += 1

    for k in ds_dict.keys():
        ds_dict[k] = None

    return out_files

def animate_series(pattern, image_width=1000, bbox=None, bands=[3,2,1], quantiles=[2,2], text_size=40, delay=50,
                   output_file='series_output.gif', deleting=False):

    szx = image_width
    if bbox is not None:
        szx = min(szx, bbox[2])

    lst = sorted(glob.glob(pattern))
    to_del = []

    for f in lst:
        # Temporary, adapted for THEIA naming
        dt = os.path.basename(f).split('_')[1][0:8]
        dt = '-'.join([dt[0:4], dt[4:6], dt[6:]])
        png_name = f.replace(os.path.splitext(f)[1], '_QKL.png')
        png_name_txt = f.replace(os.path.splitext(f)[1], '_QKL_TXT.png')
        dc = otb.Registry.CreateApplication('DynamicConvert')
        if bbox is not None:
            png_name = f.replace(os.path.splitext(f)[1], '_QKL_CLIP.png')
            png_name_txt = f.replace(os.path.splitext(f)[1], '_QKL_CLIP_TXT.png')
            er = otb.Registry.CreateApplication('ExtractROI')
            er.SetParameterString('in', f)
            er.SetParameterInt('startx', bbox[0])
            er.SetParameterInt('starty', bbox[1])
            er.SetParameterInt('sizex', bbox[2])
            er.SetParameterInt('sizey', bbox[3])
            er.Execute()
            dc.SetParameterInputImage('in', er.GetParameterOutputImage('out'))
        else:
            dc.SetParameterString('in', f)
        dc.SetParameterInt('quantile.low', quantiles[0])
        dc.SetParameterInt('quantile.high', quantiles[1])
        dc.SetParameterString('channels', 'rgb')
        dc.SetParameterInt('channels.rgb.red', bands[0])
        dc.SetParameterInt('channels.rgb.green', bands[1])
        dc.SetParameterInt('channels.rgb.blue', bands[2])
        dc.SetParameterInt('outmin', 0)
        dc.SetParameterInt('outmax', 255)
        dc.Execute()
        qkl = otb.Registry.CreateApplication('Quicklook')
        qkl.SetParameterInputImage('in', dc.GetParameterOutputImage('out'))
        qkl.SetParameterInt('sx', szx)
        qkl.SetParameterString('out', png_name)
        qkl.SetParameterOutputImagePixelType('out', otb.ImagePixelType_uint8)
        qkl.ExecuteAndWriteOutput()
        cmd = ['convert', png_name, '-font', 'helvetica', '-pointsize', str(text_size), '-gravity', 'south',
               '-stroke', '#000C', '-strokewidth', '2', '-annotate', '0', dt,
               '-stroke', 'none', '-fill', 'white', '-annotate', '0', dt,
               png_name_txt]
        subprocess.call(cmd, shell=False)
        to_del.append(png_name)
        to_del.append(png_name_txt)

    curr_dir = os.getcwd()
    os.chdir(os.path.dirname(pattern))
    cmd = ['convert', '-delay', str(delay), '-loop', '0', '*_QKL_TXT.png', '-layers', 'optimize', output_file]
    subprocess.call(cmd, shell=False)
    os.chdir(curr_dir)

    if deleting:
        [os.remove(x) for x in to_del]

    return