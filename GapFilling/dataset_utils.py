import numpy as np
import os
import gdal
import math
import sys
import s2sampling as ss
import s2signal
import otbApplication
from datetime import datetime

def DatasetCreation (img_path, samples_number):
    img_list = next(os.walk(img_path))[2]

    S1A_list = sorted(list(filter(lambda f: f.startswith("s1a"), img_list)))
    VV_list = sorted(list(filter(lambda f: 'vv' in f, S1A_list)))
    VH_list = sorted(list(filter(lambda f: 'vh' in f, S1A_list)))

    S2_list = sorted(list(filter(lambda f: f.startswith("SENTINEL2"), img_list)))
    B4_list = sorted(list(filter(lambda f: 'B4' in f, S2_list)), key=lambda x: x.split('_')[1])
    Mask_list = sorted(list(filter(lambda f: 'MASK' in f, S2_list)), key=lambda x: x.split('_')[1])

    DEM_list = sorted(list(filter(lambda f: f.startswith("DEM"), img_list)))

    ##Variable Creation
    VV_img = []
    VH_img = []
    B4_img = []


    ## S1A pictures import and normalization
    buf = []
    for i in range(len(VV_list)):
        open_path = os.path.join(img_path, VV_list[i])
        ds = gdal.Open(open_path)
        buf = ds.ReadAsArray()
        #    buf = np.moveaxis(buf, 0, -1)
        VV_img.append(buf)

    VV_img = np.asarray(VV_img)

    buf = []
    for i in range(len(VH_list)):
        open_path = os.path.join(img_path, VH_list[i])
        ds = gdal.Open(open_path)
        buf = ds.ReadAsArray()
        #    buf = np.moveaxis(buf, 0, -1)
        VH_img.append(buf)

    VH_img = np.asarray(VH_img)

    epsilon = 1e-6
    S1A_VV = 10 * np.log10(abs(VV_img) + epsilon)
    S1A_VH = 10 * np.log10(abs(VH_img) + epsilon)

    VV_mean = S1A_VV.mean()
    VV_std = S1A_VV.std()
    S1A_VV = (S1A_VV - VV_mean) / VV_std

    VV_past = S1A_VV[0]
    VV_present = S1A_VV[1]
    #VV_future = S1A_VV[2]

    VH_mean = S1A_VH.mean()
    VH_std = S1A_VH.std()
    S1A_VH = (S1A_VH - VH_mean) / VH_std

    VH_past = S1A_VH[0]
    VH_present = S1A_VH[1]
    #VH_future = S1A_VH[2]

    ## S2A pictures import and normalization

    buf = []
    for i in range(len(B4_list)):
        open_path = os.path.join(img_path, B4_list[i])
        ds = gdal.Open(open_path)
        buf = ds.ReadAsArray()
        #    buf = np.moveaxis(buf, 0, -1)
        B4_img.append(buf)
    B4_img = np.asarray(B4_img)
    B4_img = B4_img.astype('float32')
    B4_img[B4_img == -10000] = np.nan

    B4_past = B4_img[0]
    B4_reference = B4_img[1]
#    B4_future = B4_img[2]

    B4_past_mean = np.nanmean(B4_past)
    B4_past_std =  3 * np.nanstd(B4_past)

#    B4_future_mean = np.nanmean(B4_future)
#    B4_future_std = np.nanstd(B4_future)

    # B4_past = (B4_past - B4_past_mean) / B4_past_std

#    B4_reference_mean = np.nanmean(B4_reference)
#    B4_reference_std = np.nanstd(B4_reference)

    B4_past = (B4_past - B4_past_mean) / B4_past_std
    B4_reference = (B4_reference - B4_past_mean) / B4_past_std
#    B4_future = (B4_future - B4_past_mean) / B4_past_std

    # B4_future = (B4_future - B4_future_mean) / B4_future_std

    ## combined Cloud Mask creation
    mask_path = []
    for i in range(len(Mask_list)):
        mask_path.append(os.path.join(img_path, Mask_list[i]))

    combined_mask_path = (os.path.join(img_path, "combined_mask.tif"))
    test_mask_path = (os.path.join(img_path, "test_mask.tif"))
    CloudMaskCombination([mask_path[0], mask_path[1]], combined_mask_path)
    CloudMaskCombination([test_mask_path, combined_mask_path], combined_mask_path)

    open_path = os.path.join(img_path, DEM_list[0])
    dem = gdal.Open(open_path)
    dem = dem.ReadAsArray()
    # dem = np.moveaxis(dem, 0, -1)
    dem_max = dem.max()

    dem = dem / dem_max

    test_dataset = []
    test_dataset.append(
        [VV_past, VH_past, B4_past, VV_present, VH_present, dem])  # VV_future, VH_future, B4_future, dem])
    test_dataset = np.asarray(test_dataset)
    test_dataset = np.squeeze(test_dataset, axis=0)

    ndvi_path = os.path.join(img_path, S2_list[1])
    train_dataset, train_reference = PatchExtraction(test_dataset, B4_reference, ndvi_path, [33, 33], samples_number,
                                                   combined_mask_path)

    train_dataset = np.moveaxis(train_dataset, 1, -1)

    test_dataset = np.expand_dims(test_dataset, axis=0)
    t1 = test_dataset[:, :, 400:900, 1300:1800]
    t2 = test_dataset[:, :, 400: 900, 0: 500]

    t1 = np.moveaxis(t1, 1, -1)
    t2 = np.moveaxis(t2, 1, -1)
    test_dataset = np.concatenate([t1, t2])

    ref_1 = B4_reference[400:900, 1300:1800]
    ref_1 = np.expand_dims(ref_1, axis=0)

    ref_2 = B4_reference[400: 900, 0: 500]
    ref_2 = np.expand_dims(ref_2, axis=0)

    ref = np.concatenate([ref_1, ref_2])

    past_1 = B4_past[400:900, 1300:1800]
    past_1 = np.expand_dims(past_1, axis=0)

    past_2 = B4_past[400: 900, 0: 500]
    past_2 = np.expand_dims(past_2, axis=0)

    past = np.concatenate([past_1, past_2])


    return past, ref, test_dataset, train_dataset, train_reference, B4_past_mean, B4_past_std


def DatasetCreationWFuture (img_path, samples_number):
    img_list = next(os.walk(img_path))[2]

    S1A_list = sorted(list(filter(lambda f: f.startswith("s1a"), img_list)))
    VV_list = sorted(list(filter(lambda f: 'vv' in f, S1A_list)))
    VH_list = sorted(list(filter(lambda f: 'vh' in f, S1A_list)))

    S2_list = sorted(list(filter(lambda f: f.startswith("SENTINEL2"), img_list)))
    B4_list = sorted(list(filter(lambda f: 'B4' in f, S2_list)), key=lambda x: x.split('_')[1])
    Mask_list = sorted(list(filter(lambda f: 'MASK' in f, S2_list)), key=lambda x: x.split('_')[1])

    DEM_list = sorted(list(filter(lambda f: f.startswith("DEM"), img_list)))

    ##Variable Creation
    VV_img = []
    VH_img = []
    B4_img = []


    ## S1A pictures import and normalization
    buf = []
    for i in range(len(VV_list)):
        open_path = os.path.join(img_path, VV_list[i])
        ds = gdal.Open(open_path)
        buf = ds.ReadAsArray()

        VV_img.append(buf)

    VV_img = np.asarray(VV_img)

    buf = []
    for i in range(len(VH_list)):
        open_path = os.path.join(img_path, VH_list[i])
        ds = gdal.Open(open_path)
        buf = ds.ReadAsArray()

        VH_img.append(buf)

    VH_img = np.asarray(VH_img)

    epsilon = 1e-6
    S1A_VV = 10 * np.log10(abs(VV_img) + epsilon)
    S1A_VH = 10 * np.log10(abs(VH_img) + epsilon)

    VV_mean = S1A_VV.mean()
    VV_std = S1A_VV.std()
    S1A_VV = (S1A_VV - VV_mean) / VV_std

    VV_past = S1A_VV[0]
    VV_present = S1A_VV[1]
    VV_future = S1A_VV[2]

    VH_mean = S1A_VH.mean()
    VH_std = S1A_VH.std()
    S1A_VH = (S1A_VH - VH_mean) / VH_std

    VH_past = S1A_VH[0]
    VH_present = S1A_VH[1]
    VH_future = S1A_VH[2]

    ## S2A pictures import and normalization

    buf = []
    for i in range(len(B4_list)):
        open_path = os.path.join(img_path, B4_list[i])
        ds = gdal.Open(open_path)
        buf = ds.ReadAsArray()
        #    buf = np.moveaxis(buf, 0, -1)
        B4_img.append(buf)
    B4_img = np.asarray(B4_img)
    B4_img = B4_img.astype('float32')
    B4_img[B4_img == -10000] = np.nan

    B4_past = B4_img[0]
    B4_reference = B4_img[1]
    B4_future = B4_img[2]

    B4_past_mean = np.nanmean(B4_past)
    B4_past_std =  3 * np.nanstd(B4_past)


    B4_past = (B4_past - B4_past_mean) / B4_past_std
    B4_reference = (B4_reference - B4_past_mean) / B4_past_std
    B4_future = (B4_future - B4_past_mean) / B4_past_std



    ## combined Cloud Mask creation
    mask_path = []
    for i in range(len(Mask_list)):
        mask_path.append(os.path.join(img_path, Mask_list[i]))

    combined_mask_path = (os.path.join(img_path, "combined_mask.tif"))
    test_mask_path = (os.path.join(img_path, "test_mask.tif"))
    CloudMaskCombination(mask_path, combined_mask_path)
    CloudMaskCombination([test_mask_path, combined_mask_path], combined_mask_path)

    open_path = os.path.join(img_path, DEM_list[0])
    dem = gdal.Open(open_path)
    dem = dem.ReadAsArray()
    dem_max = dem.max()

    dem = dem / dem_max

    test_dataset = []
    test_dataset.append(
        [VV_past, VH_past, B4_past, VV_present, VH_present, VV_future, VH_future, B4_future, dem])
    test_dataset = np.asarray(test_dataset)
    test_dataset = np.squeeze(test_dataset, axis=0)

    ndvi_path = os.path.join(img_path, S2_list[1])
    train_dataset, train_reference = PatchExtraction(test_dataset, B4_reference, ndvi_path, [33, 33], samples_number,
                                                   combined_mask_path)

    train_dataset = np.moveaxis(train_dataset, 1, -1)

    test_dataset = np.expand_dims(test_dataset, axis=0)
    t1 = test_dataset[:, :, 400:900, 1300:1800]
    t2 = test_dataset[:, :, 400: 900, 0: 500]

    t1 = np.moveaxis(t1, 1, -1)
    t2 = np.moveaxis(t2, 1, -1)
    test_dataset = np.concatenate([t1, t2])

    ref_1 = B4_reference[400:900, 1300:1800]
    ref_1 = np.expand_dims(ref_1, axis=0)

    ref_2 = B4_reference[400: 900, 0: 500]
    ref_2 = np.expand_dims(ref_2, axis=0)

    ref = np.concatenate([ref_1, ref_2])

    past_1 = B4_past[400:900, 1300:1800]
    past_1 = np.expand_dims(past_1, axis=0)

    past_2 = B4_past[400: 900, 0: 500]
    past_2 = np.expand_dims(past_2, axis=0)

    past = np.concatenate([past_1, past_2])


    future_1 = B4_future[400:900, 1300:1800]
    future_1 = np.expand_dims(future_1, axis=0)

    future_2 = B4_future[400: 900, 0: 500]
    future_2 = np.expand_dims(future_2, axis=0)

    future = np.concatenate([future_1, future_2])

    return past, ref, future, test_dataset, train_dataset, train_reference, B4_past_mean, B4_past_std



def DatasetCreationMultiBand (img_path, samples_number):
    img_list = next(os.walk(img_path))[2]

    S1A_list = sorted(list(filter(lambda f: f.startswith("s1a"), img_list)))
    VV_list = sorted(list(filter(lambda f: 'vv' in f, S1A_list)))
    VH_list = sorted(list(filter(lambda f: 'vh' in f, S1A_list)))

    S2_list = sorted(list(filter(lambda f: f.startswith("SENTINEL2"), img_list)))
    B2_list = sorted(list(filter(lambda f: 'B2' in f, S2_list)), key=lambda x: x.split('_')[1])
    B3_list = sorted(list(filter(lambda f: 'B3' in f, S2_list)), key=lambda x: x.split('_')[1])
    B4_list = sorted(list(filter(lambda f: 'B4' in f, S2_list)), key=lambda x: x.split('_')[1])
    B8_list = sorted(list(filter(lambda f: 'B8' in f, S2_list)), key=lambda x: x.split('_')[1])
    Mask_list = sorted(list(filter(lambda f: 'MASK' in f, S2_list)), key=lambda x: x.split('_')[1])

    DEM_list = sorted(list(filter(lambda f: f.startswith("DEM"), img_list)))

    ##Variable Creation
    VV_img = []
    VH_img = []
    B2_img = []
    B3_img = []
    B4_img = []
    B8_img = []

    ## S1A pictures import and normalization
    buf = []
    for i in range(len(VV_list)):
        open_path = os.path.join(img_path, VV_list[i])
        ds = gdal.Open(open_path)
        buf = ds.ReadAsArray()

        VV_img.append(buf)

    VV_img = np.asarray(VV_img)

    buf = []
    for i in range(len(VH_list)):
        open_path = os.path.join(img_path, VH_list[i])
        ds = gdal.Open(open_path)
        buf = ds.ReadAsArray()

        VH_img.append(buf)

    VH_img = np.asarray(VH_img)

    epsilon = 1e-6
    S1A_VV = 10 * np.log10(abs(VV_img) + epsilon)
    S1A_VH = 10 * np.log10(abs(VH_img) + epsilon)

    VV_mean = S1A_VV.mean()
    VV_std = S1A_VV.std()
    S1A_VV = (S1A_VV - VV_mean) / VV_std

    VV_past = S1A_VV[0]
    VV_present = S1A_VV[1]
    VV_future = S1A_VV[2]

    VH_mean = S1A_VH.mean()
    VH_std = S1A_VH.std()
    S1A_VH = (S1A_VH - VH_mean) / VH_std

    VH_past = S1A_VH[0]
    VH_present = S1A_VH[1]
    VH_future = S1A_VH[2]

    ## B2 pictures import and normalization

    buf = []
    for i in range(len(B2_list)):
        open_path = os.path.join(img_path, B2_list[i])
        ds = gdal.Open(open_path)
        buf = ds.ReadAsArray()
        #    buf = np.moveaxis(buf, 0, -1)
        B2_img.append(buf)
    B2_img = np.asarray(B2_img)
    B2_img = B2_img.astype('float32')
    B2_img[B2_img == -10000] = np.nan

    B2_past = B2_img[0]
    B2_reference = B2_img[1]
    B2_future = B2_img[2]

    B2_past_mean = np.nanmean(B2_past)
    B2_past_std = 3 * np.nanstd(B2_past)

    B2_past = (B2_past - B2_past_mean) / B2_past_std
    B2_reference = (B2_reference - B2_past_mean) / B2_past_std
    B2_future = (B2_future - B2_past_mean) / B2_past_std

    ## B3 pictures import and normalization

    buf = []
    for i in range(len(B3_list)):
        open_path = os.path.join(img_path, B3_list[i])
        ds = gdal.Open(open_path)
        buf = ds.ReadAsArray()
        #    buf = np.moveaxis(buf, 0, -1)
        B3_img.append(buf)
    B3_img = np.asarray(B3_img)
    B3_img = B3_img.astype('float32')
    B3_img[B3_img == -10000] = np.nan

    B3_past = B3_img[0]
    B3_reference = B3_img[1]
    B3_future = B3_img[2]

    B3_past_mean = np.nanmean(B3_past)
    B3_past_std = 3 * np.nanstd(B3_past)

    B3_past = (B3_past - B3_past_mean) / B3_past_std
    B3_reference = (B3_reference - B3_past_mean) / B3_past_std
    B3_future = (B3_future - B3_past_mean) / B3_past_std

    ## B4 pictures import and normalization

    buf = []
    for i in range(len(B4_list)):
        open_path = os.path.join(img_path, B4_list[i])
        ds = gdal.Open(open_path)
        buf = ds.ReadAsArray()
        #    buf = np.moveaxis(buf, 0, -1)
        B4_img.append(buf)
    B4_img = np.asarray(B4_img)
    B4_img = B4_img.astype('float32')
    B4_img[B4_img == -10000] = np.nan

    B4_past = B4_img[0]
    B4_reference = B4_img[1]
    B4_future = B4_img[2]

    B4_past_mean = np.nanmean(B4_past)
    B4_past_std = 3 * np.nanstd(B4_past)

    B4_past = (B4_past - B4_past_mean) / B4_past_std
    B4_reference = (B4_reference - B4_past_mean) / B4_past_std
    B4_future = (B4_future - B4_past_mean) / B4_past_std

    ## B8 pictures import and normalization

    buf = []
    for i in range(len(B8_list)):
        open_path = os.path.join(img_path, B8_list[i])
        ds = gdal.Open(open_path)
        buf = ds.ReadAsArray()
        #    buf = np.moveaxis(buf, 0, -1)
        B8_img.append(buf)
    B8_img = np.asarray(B8_img)
    B8_img = B8_img.astype('float32')
    B8_img[B8_img == -10000] = np.nan

    B8_past = B8_img[0]
    B8_reference = B8_img[1]
    B8_future = B8_img[2]

    B8_past_mean = np.nanmean(B8_past)
    B8_past_std = 3 * np.nanstd(B8_past)

    B8_past = (B8_past - B8_past_mean) / B8_past_std
    B8_reference = (B8_reference - B8_past_mean) / B8_past_std
    B8_future = (B8_future - B8_past_mean) / B8_past_std

    ## combined Cloud Mask creation
    mask_path = []
    for i in range(len(Mask_list)):
        mask_path.append(os.path.join(img_path, Mask_list[i]))

    combined_mask_path = (os.path.join(img_path, "combined_mask.tif"))
    test_mask_path = (os.path.join(img_path, "test_mask.tif"))
    CloudMaskCombination(mask_path, combined_mask_path)
    CloudMaskCombination([test_mask_path, combined_mask_path], combined_mask_path)

    open_path = os.path.join(img_path, DEM_list[0])
    dem = gdal.Open(open_path)
    dem = dem.ReadAsArray()
    dem_max = dem.max()

    dem = dem / dem_max

    test_dataset = []
    test_dataset.append([
        VV_past,
        VH_past,
        B2_past,
        B3_past,
        B4_past,
        B8_past,
        VV_present,
        VH_present,
        dem,
        VV_future,
        VH_future,
        B2_future,
        B3_future,
        B4_future,
        B8_future,
    ])

    test_dataset = np.asarray(test_dataset)
    test_dataset = np.squeeze(test_dataset, axis=0)

    past = []
    past.append([B2_past, B3_past, B4_past, B8_past])

    past = np.asarray(past)
    past = np.squeeze(past, axis=0)
    past = np.moveaxis(past, 0, -1)


    reference = []
    reference.append([B2_reference, B3_reference, B4_reference, B8_reference])

    reference = np.asarray(reference)
    reference = np.squeeze(reference, axis=0)
    reference = np.moveaxis(reference, 0, -1)

    future = []
    future.append([B2_future, B3_future, B4_future, B8_future])

    future = np.asarray(future)
    future = np.squeeze(future, axis=0)
    future = np.moveaxis(future, 0, -1)



    past = B4_past
    reference = B4_reference
    future = B4_future

    ndvi_path = os.path.join(img_path, S2_list[1])
    train_dataset, train_reference = PatchExtraction(test_dataset, reference, ndvi_path, [33, 33], samples_number,
                                                        combined_mask_path)

    train_dataset = np.moveaxis(train_dataset, 1, -1)

    test_dataset = np.expand_dims(test_dataset, axis=0)
    t1 = test_dataset[:, :, 400:900, 1300:1800]
    t2 = test_dataset[:, :, 400: 900, 0: 500]

    t1 = np.moveaxis(t1, 1, -1)
    t2 = np.moveaxis(t2, 1, -1)
    test_dataset = np.concatenate([t1, t2])

    ref_1 = reference[400:900, 1300:1800]
    ref_1 = np.expand_dims(ref_1, axis=0)

    ref_2 = reference[400: 900, 0: 500]
    ref_2 = np.expand_dims(ref_2, axis=0)

    ref = np.concatenate([ref_1, ref_2])

    past_1 = past[400:900, 1300:1800]
    past_1 = np.expand_dims(past_1, axis=0)

    past_2 = past[400: 900, 0: 500]
    past_2 = np.expand_dims(past_2, axis=0)

    past = np.concatenate([past_1, past_2])

    future_1 = future[400:900, 1300:1800]
    future_1 = np.expand_dims(future_1, axis=0)

    future_2 = future[400: 900, 0: 500]
    future_2 = np.expand_dims(future_2, axis=0)

    future = np.concatenate([future_1, future_2])

    past_mean = [B2_past_mean, B3_past_mean, B4_past_mean, B8_past_mean]
    past_std = [B2_past_std, B3_past_std, B4_past_std, B8_past_std]

    return past, ref, future, test_dataset, train_dataset, train_reference, B4_past_mean, B4_past_std




def CloudMaskCombination(path_list, temp_dir):
    expr = '*'.join(['im%db1' % (s + 1) for s in range(len(path_list))])
    app = otbApplication.Registry.CreateApplication("BandMathX")

    app.SetParameterStringList("il", path_list)
    app.SetParameterString("out", temp_dir)
    app.SetParameterString("exp", expr)

    app.ExecuteAndWriteOutput()


def PatchExtraction (FileTrain, FileRef, NDVI_Path=None, PatSize=None, NSamples=None,
                             cloud_mask_path=None):
    PatchTrain = []
    PatchRef = []

    if PatSize is None:
        PatchSize = [32, 32]
    else:
        PatchSize = PatSize

    if NSamples is None:
        Samples = 500
    else:
        Samples = NSamples

    LowValue_x = math.ceil(PatchSize[0] / 2)
    LowValue_y = math.ceil(PatchSize[1] / 2)
    HighValue_x = FileTrain.shape[1] - math.ceil(PatchSize[0] / 2) - 1
    HighValue_y = FileTrain.shape[2] - math.ceil(PatchSize[1] / 2) - 1

    if NDVI_Path is None:
        Cnt_x_20 = np.random.randint(low=LowValue_x, high=HighValue_x, size=Samples)
        Cnt_y_20 = np.random.randint(low=LowValue_y, high=HighValue_y, size=Samples)
    else:
        Centers = ss.getPatchCenters(NDVI_Path, max(PatchSize), Samples, cloud_mask_path)
        Centers = np.asarray(Centers)
        Cnt_x_20 = Centers[:, 0]
        Cnt_y_20 = Centers[:, 1]


    for i in range(len(Cnt_x_20)):
        MinPatchx = Cnt_x_20[i] - math.ceil(PatchSize[0] / 2)
        MinPatchy = Cnt_y_20[i] - math.ceil(PatchSize[1] / 2)
        MaxPatchx = Cnt_x_20[i] + math.floor(PatchSize[0] / 2)
        MaxPatchy = Cnt_y_20[i] + math.floor(PatchSize[1] / 2)

        TempPatchTrain = FileTrain[:, MinPatchx:MaxPatchx, MinPatchy:MaxPatchy]
        PatchTrain.append(TempPatchTrain)

        TempPatchRef = FileRef[MinPatchx:MaxPatchx, MinPatchy:MaxPatchy]
        PatchRef.append(TempPatchRef)

    PatchTrain = np.asarray(PatchTrain)
    PatchRef = np.asarray(PatchRef)

    ##Reshape for last channeling

    return PatchTrain, PatchRef



def ricorsive_best_past (img_path, folder_list, index):
    index = index - 1
    if index == -1:
        return None
    else:
        past_path = os.path.join(img_path, folder_list[index])
        img_list = next(os.walk(past_path))[2]
        cloud_mask = list(filter(lambda f: 'MASK' in f, img_list))[0]
        ds = gdal.Open(os.path.join(past_path, cloud_mask))
        cloud_img = ds.ReadAsArray()
        zero_values = np.count_nonzero(cloud_img==0)
        quality_index = zero_values / (cloud_img.shape[0]*cloud_img.shape[1])
        if quality_index < 0.1:
            return index
        else:
            return ricorsive_best_past(img_path, folder_list, index)




def ricorsive_best_future (img_path, folder_list, index):
    index = index + 1
    if index == folder_list.__len__():
        return None
    else:
        past_path = os.path.join(img_path, folder_list[index])
        img_list = next(os.walk(past_path))[2]
        cloud_mask = list(filter(lambda f: 'MASK' in f, img_list))[0]
        ds = gdal.Open(os.path.join(past_path, cloud_mask))
        cloud_img = ds.ReadAsArray()
        zero_values = np.count_nonzero(cloud_img==0)
        quality_index = zero_values / (cloud_img.shape[0]*cloud_img.shape[1])
        if quality_index < 0.1:
            return index
        else:
            return ricorsive_best_future(img_path, folder_list, index)



def DatasetImport(in_date, S1_img_path, S2_img_path, dem_img_path, NSamples=3000, only_past=True, test_area_1=None, test_area_2=None):


    tmp_path = os.getcwd() + os.sep + 'tmp'
    if os.path.exists(tmp_path) is False:
        os.mkdir(tmp_path)

    selected_date = datetime.strptime(in_date, '%Y-%m-%d')

    S2_folder_list = sorted(next(os.walk(S2_img_path))[1], key=lambda x: x.split("_")[2])

    s2_date_dictionary = []
    for x in S2_folder_list:
        s2_date_dictionary.append(datetime.strptime(x.split("_")[2], '%Y%m%d'))

    try:
        present_index = s2_date_dictionary.index(selected_date)
    except:
        print("selected date is not in list; nearest date returned")
        nearest_data = {abs(selected_date - date): date for date in s2_date_dictionary}
        nearest_data = nearest_data[min(nearest_data.keys())]
        present_index = s2_date_dictionary.index(nearest_data)

    past_index = ricorsive_best_past(S2_img_path, S2_folder_list, present_index)
    future_index = ricorsive_best_future(S2_img_path, S2_folder_list, present_index)

    S1_img_list = sorted(next(os.walk(S1_img_path))[2], key=lambda x: x.split("-")[4])

    ## PER ESSERE SICURI AL MILLE PER CENTO

    vv_list = sorted(list(filter(lambda f: 'vv' in f, S1_img_list)), key=lambda x: x.split("-")[4])
    vh_list = sorted(list(filter(lambda f: 'vh' in f, S1_img_list)), key=lambda x: x.split("-")[4])

    s1_date_dictionary = []
    for x in vv_list:
        s1_date_dictionary.append(datetime.strptime(x.split("-")[4], '%Y%m%dt%H%M%S'))

    nearest_past_data = {abs(s2_date_dictionary[past_index] - date): date for date in s1_date_dictionary}
    nearest_past_data = nearest_past_data[min(nearest_past_data.keys())]
    s1_past_index = s1_date_dictionary.index(nearest_past_data)

    nearest_present_data = {abs(s2_date_dictionary[present_index] - date): date for date in s1_date_dictionary}
    nearest_present_data = nearest_present_data[min(nearest_present_data.keys())]
    s1_present_index = s1_date_dictionary.index(nearest_present_data)

    nearest_future_data = {abs(s2_date_dictionary[future_index] - date): date for date in s1_date_dictionary}
    nearest_future_data = nearest_future_data[min(nearest_future_data.keys())]
    s1_future_index = s1_date_dictionary.index(nearest_future_data)

    ## Open and Normalize S1 File

    ds = []
    ds = gdal.Open(os.path.join(S1_img_path, vh_list[s1_past_index]))
    vh_past = ds.ReadAsArray()
    vh_past = np.expand_dims(vh_past, axis=0)

    ds = []
    ds = gdal.Open(os.path.join(S1_img_path, vv_list[s1_past_index]))
    vv_past = ds.ReadAsArray()
    vv_past = np.expand_dims(vv_past, axis=0)

    ds = []
    ds = gdal.Open(os.path.join(S1_img_path, vh_list[s1_present_index]))
    vh_present = ds.ReadAsArray()
    vh_present = np.expand_dims(vh_present, axis=0)

    ds = []
    ds = gdal.Open(os.path.join(S1_img_path, vv_list[s1_present_index]))
    vv_present = ds.ReadAsArray()
    vv_present = np.expand_dims(vv_present, axis=0)

    ds = []
    ds = gdal.Open(os.path.join(S1_img_path, vh_list[s1_future_index]))
    vh_future = ds.ReadAsArray()
    vh_future = np.expand_dims(vh_future, axis=0)

    ds = []
    ds = gdal.Open(os.path.join(S1_img_path, vv_list[s1_future_index]))
    vv_future = ds.ReadAsArray()
    vv_future = np.expand_dims(vv_future, axis=0)

    epsilon = 1e-6

    vh = np.concatenate([vh_past, vh_present, vh_future])
    vv = np.concatenate([vv_past, vv_present, vv_future])

    vv = 10 * np.log10(abs(vv) + epsilon)
    vh = 10 * np.log10(abs(vh) + epsilon)

    vh_mean = vh.mean()
    vh_std = vh.std()

    vh = (vh - vh_mean) / vh_std

    vv_mean = vv.mean()
    vv_std = vv.std()

    vv = (vv - vv_mean) / vv_std

    vh_past = vh[0]
    vh_present = vh[1]
    vh_future = vh[2]

    vv_past = vv[0]
    vv_present = vv[1]
    vv_future = vv[2]

    ## Open and Normalize b4 images

    ds = []
    path = os.path.join(S2_img_path, S2_folder_list[past_index])
    img_list = next(os.walk(path))[2]
    img = list(filter(lambda f: 'B4' in f, img_list))[0]

    ds = gdal.Open(os.path.join(path, img))
    b4_past = ds.ReadAsArray()

    ds = []
    path = os.path.join(S2_img_path, S2_folder_list[present_index])
    img_list = next(os.walk(path))[2]
    img = list(filter(lambda f: 'B4' in f, img_list))[0]
    ref_path = os.path.join(path, img)
    ds = gdal.Open(ref_path)
    b4_present = ds.ReadAsArray()

    ds = []
    path = os.path.join(S2_img_path, S2_folder_list[future_index])
    img_list = next(os.walk(path))[2]
    img = list(filter(lambda f: 'B4' in f, img_list))[0]

    ds = gdal.Open(os.path.join(path, img))
    b4_future = ds.ReadAsArray()

    b4_mean = b4_past.mean()
    b4_std = 3 * b4_past.std()

    b4_past = (b4_past - b4_mean) / b4_std
    b4_present = (b4_present - b4_mean) / b4_std
    b4_future = (b4_future - b4_mean) / b4_std

    ## cloud mask creation

    if test_area_1 is not None:
        tmask = np.ones(b4_past.shape)
        tmask[test_area_1[0]:test_area_1[2], test_area_1[1]:test_area_1[3]] = 0
        if test_area_2 is not None:
            tmask[test_area_2[0]:test_area_2[2], test_area_2[1]:test_area_2[3]] = 0
        test_mask_path = (os.path.join(tmp_path, "test_mask.tif"))
        drv = gdal.GetDriverByName('GTiff')
        ds = drv.Create(test_mask_path, b4_past.shape[1], b4_past.shape[0], 1, gdal.GDT_Byte)
        dsr = gdal.Open(ref_path)
        ds.SetGeoTransform(dsr.GetGeoTransform())
        ds.SetProjection(dsr.GetProjection())
        dsr = None
        ds.GetRasterBand(1).WriteArray(tmask.astype(np.uint8))
        ds = None

    cloud_mask_path = []

    path = os.path.join(S2_img_path, S2_folder_list[past_index])
    img_list = next(os.walk(path))[2]
    cloud_mask = list(filter(lambda f: 'MASK' in f, img_list))[0]

    cloud_mask_path.append(os.path.join(path, cloud_mask))

    path = os.path.join(S2_img_path, S2_folder_list[present_index])
    img_list = next(os.walk(path))[2]
    cloud_mask = list(filter(lambda f: 'MASK' in f, img_list))[0]

    cloud_mask_path.append(os.path.join(path, cloud_mask))

    if only_past is False:
        path = os.path.join(S2_img_path, S2_folder_list[future_index])
        img_list = next(os.walk(path))[2]
        cloud_mask = list(filter(lambda f: 'MASK' in f, img_list))[0]

        cloud_mask_path.append(os.path.join(path, cloud_mask))

    if test_area_1 is not None:
        cloud_mask_path.append(test_mask_path)

    cloud_mask_out = tmp_path + os.sep + 'cloud_mask.tif'
    CloudMaskCombination(cloud_mask_path, cloud_mask_out)

    ## Open DEM

    dem = gdal.Open(dem_img_path)
    dem = dem.ReadAsArray()
    dem = dem / dem.max()

    test_dataset = []

    if only_past is not True:
        test_dataset.append([vv_past, vh_past, b4_past, vv_present, vh_present, vv_future, vh_future, b4_future, dem])
    else:
        test_dataset.append([vv_past, vh_past, b4_past, vv_present, vh_present, dem])

    test_dataset = np.squeeze(np.asarray(test_dataset), axis=0)

    train_dataset, train_reference = PatchExtraction(test_dataset, b4_present, ref_path, [33, 33], NSamples,
                                                        cloud_mask_out)
    train_dataset = np.moveaxis(train_dataset, 1, -1)

    test_dataset = np.moveaxis(test_dataset, 0, -1)

    t = []
    pres = []
    past = []

    if test_area_1 is not None:
        t.append(test_dataset[test_area_1[0]:test_area_1[1], test_area_1[2]:test_area_1[3], :])
        pres.append(b4_present[test_area_1[0]:test_area_1[1], test_area_1[2]:test_area_1[3]])
        past.append(b4_past[test_area_1[0]:test_area_1[1], test_area_1[2]:test_area_1[3]])

        if test_area_2 is not None:
            t.append(test_dataset[test_area_2[0]:test_area_2[1], test_area_2[2]:test_area_2[3], :])
            pres.append(b4_present[test_area_2[0]:test_area_2[1], test_area_2[2]:test_area_2[3]])
            past.append(b4_past[test_area_2[0]:test_area_2[1], test_area_2[2]:test_area_2[3]])

        t = np.asarray(t)
        pres = np.asarray(pres)
        past = np.asarray(past)

        test_dataset = t
        b4_present = pres
        b4_past = past

    return b4_past, b4_present, test_dataset, train_dataset, train_reference, b4_mean, b4_std



def cut_patches_to(patches, sz):
    actual = patches.shape[-1]
    off = math.floor(float(actual-sz)/2)
    return patches[:,off:actual-off,off:actual-off]
