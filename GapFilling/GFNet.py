from tensorflow.keras import layers, optimizers, models, callbacks, backend, losses
import tensorboard
import numpy as np
import tensorflow as tf
import os
import datetime
import math


def scheduler(epoch):
    if epoch < 50:
        return 1e-3
    if 50 <= epoch < 300:
        return 1e-3
    if 300 <= epoch < 450:
        return 1e-4
    else:
        return 1e-5 * math.exp(1 ** (450 - epoch))


def CustomCallbacks():
    TensorBoardDir = os.path.curdir + os.sep + 'logs' + os.sep + 'gfnet_' + datetime.datetime.now().strftime(
        '%Y-%m-%d_%H-%M-%S')

    if os.path.exists(os.path.curdir + os.sep + 'logs') is False:
        os.mkdir(os.path.curdir + os.sep + 'logs')

    CheckPointDir = os.path.curdir + os.sep + 'checkpoints' + os.sep + 'gfnet_' + datetime.datetime.now().strftime(
        '%Y-%m-%d_%H-%M-%S')

    if os.path.exists(os.path.curdir + os.sep + 'checkpoints') is False:
        os.mkdir(os.path.curdir + os.sep + 'checkpoints')

    if os.path.exists(CheckPointDir) is False:
        os.mkdir(CheckPointDir)
    CheckPointPrefix = os.path.join(CheckPointDir, "ckpt_{epoch:03d}.h5")

    callback = [
        callbacks.TensorBoard(log_dir=TensorBoardDir),
        callbacks.ModelCheckpoint(filepath=CheckPointPrefix, save_weights_only=True)]
    #        callbacks.LearningRateScheduler(scheduler)
    #    ]

    return callback


def FindLastWeights():
    CheckPointRoot = os.path.curdir + os.sep + 'checkpoints'
    CheckDir = next(os.walk(CheckPointRoot))[1]
    LastDir = list(filter(lambda f: f.startswith("gfnet_"), CheckDir))
    LastDir = sorted(LastDir)
    LastDirPath = os.path.join(CheckPointRoot, LastDir[-1])
    Checkpoints = next(os.walk(LastDirPath))[2]
    LastCheck = list(filter(lambda f: f.startswith("ckpt_"), Checkpoints))
    LastCheck = sorted(LastCheck)
    last = os.path.join(LastDirPath, LastCheck[-1])
    return last


def gapfillingnet_v1(train_dataset):
    inp = layers.Input(shape=(None, None, train_dataset.shape[-1]), name="net_input")
    l1 = layers.Conv2D(48, [9, 9], padding='valid', data_format='channels_last', name='conv_layer_1')(
        inp)
    n1 = layers.BatchNormalization()(l1)
    a1 = layers.Activation('relu')(n1)

    l2 = layers.Conv2D(32, [5, 5], padding='valid', data_format='channels_last', name='conv_layer_2')(
        a1)
    n2 = layers.BatchNormalization()(l2)
    a2 = layers.Activation('relu')(n2)

    net_out = layers.Conv2D(1, [5, 5], padding='valid', data_format='channels_last', activation='linear',
                            name='conv_layer_out')(a2)

    out = net_out

    gfnet = models.Model(inputs=inp, outputs=out)

    opt = optimizers.Adam(learning_rate=1e-4)

    loss_1 = "mean_absolute_error"
    met_1 = ['mae']

    loss_2 = ProvLoss
    met_2 = [MAESpec, L1_MaxLoss, 'mae']

    loss_3 = RafLoss
    met_3 = [MAESpec, L1_RafLoss, 'mae']

    loss_4 = NapoliLoss
    met_4 = [MAESpec, sCC, 'mae']

    loss_5 = MatteoLoss
    met_5 = [MAESpec, SSIM, sCC, 'mae']

    gfnet.compile(optimizer=opt, loss=loss_1, metrics= met_1)

    return gfnet



def gapfillingnet_v1b(train_dataset):
    inp = layers.Input(shape=(None, None, train_dataset.shape[-1]), name="net_input")
    l1 = layers.Conv2D(16, [5, 5], padding='valid', data_format='channels_last', name='conv_layer_1')(
        inp)
    n1 = layers.BatchNormalization()(l1)
    a1 = layers.LeakyReLU()(n1)

    l2 = layers.Conv2D(32, [3, 3], padding='valid', data_format='channels_last', name='conv_layer_2')(
        a1)
    n2 = layers.BatchNormalization()(l2)
    a2 = layers.LeakyReLU()(n2)

    l3 = layers.Conv2D(64, [3, 3], padding='valid', data_format='channels_last', name='conv_layer_3')(
        a2)
    n3 = layers.BatchNormalization()(l3)
    a3 = layers.LeakyReLU()(n3)

    net_out = layers.Conv2D(1, [3, 3], padding='valid', data_format='channels_last', activation='linear',
                            name='conv_layer_out')(a3)

    out = net_out

    gfnet = models.Model(inputs=inp, outputs=out)

    opt = optimizers.Adam(learning_rate=1e-3)

    loss_1 = "mean_absolute_error"
    met_1 = ['mae']

    loss_2 = ProvLoss
    met_2 = [MAESpec, L1_MaxLoss, 'mae']

    loss_3 = RafLoss
    met_3 = [MAESpec, L1_RafLoss, 'mae']

    loss_4 = NapoliLoss
    met_4 = [MAESpec, sCC, 'mae']

    loss_5 = MatteoLoss
    met_5 = [MAESpec, SSIM, sCC, 'mae']

    gfnet.compile(optimizer=opt, loss=loss_1, metrics= met_1)

    return gfnet


def gapfillingnet_v1c(train_dataset, lr=1e-3):
    inp = layers.Input(shape=(None, None, train_dataset.shape[-1]), name="net_input")
    l1 = layers.Conv2D(512, [3, 3], padding='valid', data_format='channels_last', name='conv_layer_1')(
        inp)
    n1 = layers.BatchNormalization()(l1)
    a1 = layers.LeakyReLU()(n1)

    l2 = layers.Conv2D(512, [3, 3], padding='valid', data_format='channels_last', name='conv_layer_2')(
        a1)
    n2 = layers.BatchNormalization()(l2)
    a2 = layers.LeakyReLU()(n2)

    l3 = layers.Conv2D(512, [3, 3], padding='valid', data_format='channels_last', name='conv_layer_3')(
        a2)
    n3 = layers.BatchNormalization()(l3)
    a3 = layers.LeakyReLU()(n3)

    #l4 = layers.Conv2D(16, [3, 3], padding='valid', data_format='channels_last', name='conv_layer_4')(
    #    a3)
    #n4 = layers.BatchNormalization()(l4)
    #a4 = layers.LeakyReLU()(n4)

    #l5 = layers.Conv2D(8, [3, 3], padding='valid', data_format='channels_last', name='conv_layer_5')(
    #    a4)
    #n5 = layers.BatchNormalization()(l5)
    #a5 = layers.LeakyReLU()(n5)

    net_out = layers.Conv2D(1, [3, 3], padding='valid', data_format='channels_last', activation='linear',
                            name='conv_layer_out')(a3)

    out = net_out

    gfnet = models.Model(inputs=inp, outputs=out)

    opt = optimizers.Adam(learning_rate=lr)

    loss_1 = "mean_absolute_error"
    met_1 = ['mae']

    loss_2 = ProvLoss
    met_2 = [MAESpec, L1_MaxLoss, 'mae']

    loss_3 = RafLoss
    met_3 = [MAESpec, L1_RafLoss, 'mae']

    loss_4 = NapoliLoss
    met_4 = [MAESpec, sCC, 'mae']

    loss_5 = MatteoLoss
    met_5 = [MAESpec, SSIM, sCC, 'mae']

    gfnet.compile(optimizer=opt, loss=loss_1, metrics= met_1)

    return gfnet



def gapfillingnet_v2(train_dataset):
    inp = layers.Input(shape=(None, None, train_dataset.shape[-1]), name="net_input")
    l1 = layers.Conv2D(8, [9, 9], padding='valid', data_format='channels_last', name='conv_layer_1')(inp)
    n1 = layers.BatchNormalization()(l1)
    a1 = layers.Activation('relu')(n1)

    l2 = layers.Conv2D(8, [9, 9], padding='valid', data_format='channels_last', name='conv_layer_2')(a1)
    n2 = layers.BatchNormalization()(l2)
    sum1 = layers.Add()([a1, n2])
    a2 = layers.Activation('relu')(sum1)

    l3 = layers.Conv2D(16, [5, 5], padding='valid', data_format='channels_last', name='conv_layer_3')(a2)
    n3 = layers.BatchNormalization()(l3)
    a3 = layers.Activation('relu')(n3)

    l4 = layers.Conv2D(16, [5, 5], padding='valid', data_format='channels_last', name='conv_layer_4')(a3)
    n4 = layers.BatchNormalization()(l4)
    sum2 = layers.Add()([a3, n4])
    a4 = layers.Activation('relu')(sum2)

    l5 = layers.Conv2D(32, [3, 3], padding='valid', data_format='channels_last', name='conv_layer_5')(a4)
    n5 = layers.BatchNormalization()(l5)
    a5 = layers.Activation('relu')(n5)

    l6 = layers.Conv2D(32, [3, 3], padding='valid', data_format='channels_last', name='conv_layer_6')(a5)
    n6 = layers.BatchNormalization()(l6)
    sum3 = layers.Add()([a5, n6])
    a6 = layers.Activation('relu')(sum3)

    net_out = layers.Conv2D(1, [3, 3], padding='valid', data_format='channels_last', activation='linear')(a6)

    out = net_out + backend.expand_dims(inp[:, :, :, 2])

    gfnet = models.Model(inputs=inp, outputs=out)

    opt = optimizers.Adam(learning_rate=1e-4)

    loss_1 = "mean_absolute_error"
    met_1 = ['mae']

    loss_2 = ProvLoss
    met_2 = [MAESpec, L1_MaxLoss, 'mae']

    loss_3 = RafLoss
    met_3 = [MAESpec, L1_RafLoss, 'mae']

    loss_4 = NapoliLoss
    met_4 = [MAESpec, sCC, 'mae']

    loss_5 = MatteoLoss
    met_5 = [MAESpec, SSIM, sCC, 'mae']


    gfnet.compile(optimizer=opt, loss=loss_1, metrics=met_1)

    return gfnet


def gapfillingnet_v3(train_dataset):

    inp = layers.Input(shape=(None, None, train_dataset.shape[-1]), name="net_input")
    n0 = layers.BatchNormalization()(inp)
    l1 = layers.Conv2D(16, [9, 9], padding='valid', data_format='channels_last', name='conv_layer_1')(
        n0)
    n1 = layers.BatchNormalization()(l1)
    a1 = layers.Activation('relu')(n1)
    inp_c1 = layers.Cropping2D(cropping=4, data_format='channels_last', name="crop_layer_inp_1")(inp)
    a1_c1 = layers.Cropping2D(cropping=4, data_format='channels_last', name="crop_layer_a1_1")(a1)
    l2 = layers.Conv2D(16, [9, 9], padding='valid', data_format='channels_last', name='conv_layer_2')(
        a1)
    n2 = layers.BatchNormalization()(l2)
    sum1 = layers.Add()([a1_c1, n2])
    a2 = layers.Activation('relu')(sum1)
    inp_c2 = layers.Cropping2D(cropping=4, data_format='channels_last', name="crop_layer_inp_2")(inp_c1)

    c1 = layers.Concatenate()([a2, inp_c2])

    l3 = layers.Conv2D(32, [5, 5], padding='valid', data_format='channels_last', name='conv_layer_3')(
        c1)
    n3 = layers.BatchNormalization()(l3)
    a3 = layers.Activation('relu')(n3)
    c1_c1 = layers.Cropping2D(cropping=2, data_format='channels_last', name="crop_layer_c1_1")(c1)
    a3_c1 = layers.Cropping2D(cropping=2, data_format='channels_last', name="crop_layer_a3_1")(a3)


    l4 = layers.Conv2D(32, [5, 5], padding='valid', data_format='channels_last', name='conv_layer_4')(
        a3)
    n4 = layers.BatchNormalization()(l4)
    sum2 = layers.Add()([a3_c1, n4])
    a4 = layers.Activation('relu')(sum2)
    c1_c2 = layers.Cropping2D(cropping=2, data_format='channels_last', name="crop_layer_c1_2")(c1_c1)

    c2 = layers.Concatenate()([a4, c1_c2])

    l5 = layers.Conv2D(64, [3, 3], padding='valid', data_format='channels_last', name='conv_layer_5')(
        c2)
    n5 = layers.BatchNormalization()(l5)
    a5 = layers.Activation('relu')(n5)
    c2_c1 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_c2_1")(c2)
    a5_c1 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_a5_1")(a5)


    l6 = layers.Conv2D(64, [3, 3], padding='valid', data_format='channels_last', name='conv_layer_6')(
        a5)
    n6 = layers.BatchNormalization()(l6)
    sum3 = layers.Add()([a5_c1, n6])
    a6 = layers.Activation('relu')(sum3)
    c2_c2 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_c2_2")(c2_c1)


    c3 = layers.Concatenate()([a6, c2_c2])

    net_out = layers.Conv2D(1, [3, 3], padding='valid', data_format='channels_last', activation='linear',
                            name='conv_layer_out')(c3)

    out = net_out #+backend.expand_dims(inp[:, :, :, 2])

    gfnet = models.Model(inputs=inp, outputs=out)

    opt = optimizers.Adam(learning_rate=1e-4)

    loss_1 = "mean_absolute_error"
    met_1 = ['mae']

    loss_2 = ProvLoss
    met_2 = [MAESpec, L1_MaxLoss, 'mae']

    loss_3 = RafLoss
    met_3 = [MAESpec, L1_RafLoss, 'mae']

    loss_4 = NapoliLoss
    met_4 = [MAESpec, sCC, 'mae']

    loss_5 = MatteoLoss
    met_5 = [MAESpec, SSIM, sCC, 'mae']

    gfnet.compile(optimizer=opt, loss= loss_1, metrics= met_1)

    return gfnet


def gapfillingnet_v4(train_dataset):
    inp = layers.Input(shape=(None, None, train_dataset.shape[-1]), name="net_input")
    n0 = layers.BatchNormalization()(inp)
    l1 = layers.Conv2D(16, [9, 9], padding='same', data_format='channels_last', name='conv_layer_1')(
        n0)
    n1 = layers.BatchNormalization()(l1)
    a1 = layers.LeakyReLU()(n1)

    l2 = layers.Conv2D(16, [9, 9], padding='same', data_format='channels_last', name='conv_layer_2')(
        a1)
    n2 = layers.BatchNormalization()(l2)
    sum1 = layers.Add()([a1, n2])
    a2 = layers.LeakyReLU()(sum1)

    c1 = layers.Concatenate()([a2, inp])

    l3 = layers.Conv2D(32, [5, 5], padding='same', data_format='channels_last', name='conv_layer_3')(
        c1)
    n3 = layers.BatchNormalization()(l3)
    a3 = layers.LeakyReLU()(n3)

    l4 = layers.Conv2D(32, [3, 3], padding='same', data_format='channels_last', name='conv_layer_4')(
        a3)
    n4 = layers.BatchNormalization()(l4)
    sum2 = layers.Add()([a3, n4])
    a4 = layers.LeakyReLU()(sum2)

    c2 = layers.Concatenate()([a4, a2, inp])

    l5 = layers.Conv2D(64, [3, 3], padding='same', data_format='channels_last', name='conv_layer_5')(
        c2)
    n5 = layers.BatchNormalization()(l5)
    a5 = layers.LeakyReLU()(n5)

    l6 = layers.Conv2D(64, [3, 3], padding='same', data_format='channels_last', name='conv_layer_6')(
        a5)
    n6 = layers.BatchNormalization()(l6)
    sum3 = layers.Add()([a5, n6])
    a6 = layers.LeakyReLU()(sum3)

    c3 = layers.Concatenate()([a6, a4, a2, inp])

    net_out = layers.Conv2D(1, [3, 3], padding='same', data_format='channels_last', activation='linear',
                            name='conv_layer_out')(c3)

    out = net_out + backend.expand_dims(inp[:, :, :, 2])

    gfnet = models.Model(inputs=inp, outputs=out)

    opt = optimizers.Adam(learning_rate=1e-4)

    loss_1 = "mean_absolute_error"
    met_1 = ['mae']

    loss_2 = ProvLoss
    met_2 = [MAESpec, L1_MaxLoss, 'mae']

    loss_3 = RafLoss
    met_3 = [MAESpec, L1_RafLoss, 'mae']

    loss_4 = NapoliLoss
    met_4 = [MAESpec, sCC, 'mae']

    loss_5 = MatteoLoss
    met_5 = [MAESpec, SSIM, sCC, 'mae']

    gfnet.compile(optimizer=opt, loss=loss_1, metrics=met_1)

    return gfnet


def gapfillingnet_v5(train_dataset):
    inp = layers.Input(shape=(None, None, train_dataset.shape[-1]), name="net_input")
    n0 = layers.BatchNormalization()(inp)
    l1 = layers.Conv2D(16, [5, 5], padding='valid', data_format='channels_last', name='conv_layer_1')(
        n0)
    n1 = layers.BatchNormalization()(l1)
    a1 = layers.LeakyReLU()(n1)
    inp_c1 = layers.Cropping2D(cropping=2, data_format='channels_last', name="crop_layer_inp_1")(inp)

    l2 = layers.Conv2D(16, [5, 5], padding='valid', data_format='channels_last', name='conv_layer_2')(
        a1)
    n2 = layers.BatchNormalization()(l2)
    sum1 = layers.Add()([a1, n2])
    a2 = layers.LeakyReLU()(sum1)
    inp_c2 = layers.Cropping2D(cropping=2, data_format='channels_last', name="crop_layer_inp_2")(inp_c1)

    c1 = layers.Concatenate()([a2, inp_c2])

    l3 = layers.Conv2D(32, [3, 3], padding='same', data_format='channels_last', name='conv_layer_3')(
        c1)
    n3 = layers.BatchNormalization()(l3)
    a3 = layers.LeakyReLU()(n3)
    c1_c1 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_c1_1")(c1)

    l4 = layers.Conv2D(32, [3, 3], padding='same', data_format='channels_last', name='conv_layer_4')(
        a3)
    n4 = layers.BatchNormalization()(l4)
    sum2 = layers.Add()([a3, n4])
    a4 = layers.LeakyReLU()(sum2)
    c1_c2 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_c1_2")(c1_c1)


    c2 = layers.Concatenate()([a4, c1_c2])

    l5 = layers.Conv2D(64, [3, 3], padding='same', data_format='channels_last', name='conv_layer_5')(
        c2)
    n5 = layers.BatchNormalization()(l5)
    a5 = layers.LeakyReLU()(n5)
    c2_c1 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_c2_1")(c2)


    l6 = layers.Conv2D(64, [3, 3], padding='same', data_format='channels_last', name='conv_layer_6')(
        a5)
    n6 = layers.BatchNormalization()(l6)
    sum3 = layers.Add()([a5, n6])
    a6 = layers.LeakyReLU()(sum3)
    c2_c2 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_c2_2")(c2_c1)


    c3 = layers.Concatenate()([a6, c2_c2])

    net_out = layers.Conv2D(1, [3, 3], padding='same', data_format='channels_last', activation='linear',
                            name='conv_layer_out')(c3)

    out = net_out #+ backend.expand_dims(inp[:, :, :, 2])

    gfnet = models.Model(inputs=inp, outputs=out)

    opt = optimizers.Adam(learning_rate=1e-4)

    loss_1 = "mean_absolute_error"
    met_1 = ['mae']

    loss_2 = ProvLoss
    met_2 = [MAESpec, L1_MaxLoss, 'mae']

    loss_3 = RafLoss
    met_3 = [MAESpec, L1_RafLoss, 'mae']

    loss_4 = NapoliLoss
    met_4 = [MAESpec, sCC, 'mae']

    loss_5 = MatteoLoss
    met_5 = [MAESpec, SSIM, sCC, 'mae']

    gfnet.compile(optimizer=opt, loss=loss_1, metrics=met_1)

    return gfnet



def gapfillingnet_v4b (train_dataset):
    inp = layers.Input(shape=(None, None, train_dataset.shape[-1]), name="net_input")

    l1 = layers.Conv2D(16, [9, 9], padding='valid', data_format='channels_last', name='conv_layer_1')(
        inp)
    inp_c1 = layers.Cropping2D(cropping=4, data_format='channels_last', name="crop_layer_inp_1")(inp)
    n1 = layers.BatchNormalization()(l1)
    a1 = layers.Activation('tanh')(n1)

    l2 = layers.Conv2D(16, [9, 9], padding='valid', data_format='channels_last', name='conv_layer_2')(
        a1)
    inp_c2 = layers.Cropping2D(cropping=4, data_format='channels_last', name="crop_layer_inp_2")(inp_c1)
    n2 = layers.BatchNormalization()(l2)
    a1_c1 = layers.Cropping2D(cropping=4, data_format='channels_last', name="crop_layer_a1_2")(a1)
    sum1 = layers.Add()([a1_c1, n2])
    a2 = layers.Activation('tanh')(sum1)

    c1 = layers.Concatenate()([a2, inp_c2])

    l3 = layers.Conv2D(32, [5, 5], padding='valid', data_format='channels_last', name='conv_layer_3')(
        c1)

    inp_c3 = layers.Cropping2D(cropping=2, data_format='channels_last', name="crop_layer_inp_3")(inp_c2)
    a2_c1 = layers.Cropping2D(cropping=2, data_format='channels_last', name="crop_layer_a2_1")(a2)
    n3 = layers.BatchNormalization()(l3)
    a3 = layers.Activation('tanh')(n3)
    a3_c1 = layers.Cropping2D(cropping=2, data_format='channels_last', name="crop_layer_a3_1")(a3)

    l4 = layers.Conv2D(32, [5, 5], padding='valid', data_format='channels_last', name='conv_layer_4')(
        a3)

    inp_c4 = layers.Cropping2D(cropping=2, data_format='channels_last', name="crop_layer_inp_4")(inp_c3)
    a2_c2 = layers.Cropping2D(cropping=2, data_format='channels_last', name="crop_layer_a2_2")(a2_c1)

    n4 = layers.BatchNormalization()(l4)
    sum2 = layers.Add()([a3_c1, n4])
    a4 = layers.Activation('tanh')(sum2)

    c2 = layers.Concatenate()([a4, a2_c2, inp_c4])

    l5 = layers.Conv2D(64, [3, 3], padding='valid', data_format='channels_last', name='conv_layer_5')(
        c2)

    c2_c1 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_c2_1")(c2)

    n5 = layers.BatchNormalization()(l5)
    a5 = layers.Activation('tanh')(n5)
    a5_c1 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_a5_1")(a5)

    l6 = layers.Conv2D(64, [3, 3], padding='valid', data_format='channels_last', name='conv_layer_6')(
        a5)

    c2_c2 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_c2_2")(c2_c1)
    n6 = layers.BatchNormalization()(l6)

    sum3 = layers.Add()([a5_c1, n6])
    a6 = layers.Activation('tanh')(sum3)

    c3 = layers.Concatenate()([a6, c2_c2])

    net_out = layers.Conv2D(1, [3, 3], padding='valid', data_format='channels_last', activation='linear',
                            name='conv_layer_out')(c3)

    out = net_out #+ backend.expand_dims(inp[:, :, :, 2])

    gfnet = models.Model(inputs=inp, outputs=out)

    opt = optimizers.Adam(learning_rate=1e-4)

    loss_1 = "mean_absolute_error"
    met_1 = ['mae']

    loss_2 = ProvLoss
    met_2 = [MAESpec, L1_MaxLoss, 'mae']

    loss_3 = RafLoss
    met_3 = [MAESpec, L1_RafLoss, 'mae']

    loss_4 = NapoliLoss
    met_4 = [MAESpec, sCC, 'mae']

    loss_5 = MatteoLoss
    met_5 = [MAESpec, SSIM, sCC, 'mae']

    gfnet.compile(optimizer=opt, loss=loss_1, metrics=met_1)

    return gfnet



def gapfillingnet_v5b (train_dataset):
    inp = layers.Input(shape=(None, None, train_dataset.shape[-1]), name="net_input")

    l1 = layers.Conv2D(16, [5, 5], padding='valid', data_format='channels_last', name='conv_layer_1')(
        inp)
    inp_c1 = layers.Cropping2D(cropping=2, data_format='channels_last', name="crop_layer_inp_1")(inp)
    n1 = layers.BatchNormalization()(l1)
    a1 = layers.Activation('tanh')(n1)

    l2 = layers.Conv2D(16, [5, 5], padding='valid', data_format='channels_last', name='conv_layer_2')(
        a1)
    inp_c2 = layers.Cropping2D(cropping=2, data_format='channels_last', name="crop_layer_inp_2")(inp_c1)
    n2 = layers.BatchNormalization()(l2)
    a1_c1 = layers.Cropping2D(cropping=2, data_format='channels_last', name="crop_layer_a1_2")(a1)
    sum1 = layers.Add()([a1_c1, n2])
    a2 = layers.Activation('tanh')(sum1)

    c1 = layers.Concatenate()([a2, inp_c2])

    l3 = layers.Conv2D(32, [3, 3], padding='valid', data_format='channels_last', name='conv_layer_3')(
        c1)

    inp_c3 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_inp_3")(inp_c2)
    a2_c1 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_a2_1")(a2)
    n3 = layers.BatchNormalization()(l3)
    a3 = layers.Activation('tanh')(n3)
    a3_c1 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_a3_1")(a3)

    l4 = layers.Conv2D(32, [3, 3], padding='valid', data_format='channels_last', name='conv_layer_4')(
        a3)

    inp_c4 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_inp_4")(inp_c3)
    a2_c2 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_a2_2")(a2_c1)

    n4 = layers.BatchNormalization()(l4)
    sum2 = layers.Add()([a3_c1, n4])
    a4 = layers.Activation('tanh')(sum2)

    c2 = layers.Concatenate()([a4, a2_c2, inp_c4])

    l5 = layers.Conv2D(64, [3, 3], padding='valid', data_format='channels_last', name='conv_layer_5')(
        c2)

    c2_c1 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_c2_1")(c2)

    n5 = layers.BatchNormalization()(l5)
    a5 = layers.Activation('tanh')(n5)
    a5_c1 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_a5_1")(a5)

    l6 = layers.Conv2D(64, [3, 3], padding='valid', data_format='channels_last', name='conv_layer_6')(
        a5)

    c2_c2 = layers.Cropping2D(cropping=1, data_format='channels_last', name="crop_layer_c2_2")(c2_c1)
    n6 = layers.BatchNormalization()(l6)

    sum3 = layers.Add()([a5_c1, n6])
    a6 = layers.Activation('tanh')(sum3)

    c3 = layers.Concatenate()([a6, c2_c2])

    net_out = layers.Conv2D(1, [3, 3], padding='valid', data_format='channels_last', activation='linear',
                            name='conv_layer_out')(c3)

    out = net_out #+ backend.expand_dims(inp[:, :, :, 2])

    gfnet = models.Model(inputs=inp, outputs=out)

    opt = optimizers.Adam(learning_rate=1e-3)

    loss_1 = "mean_absolute_error"
    met_1 = ['mae']

    loss_2 = ProvLoss
    met_2 = [MAESpec, L1_MaxLoss, 'mae']

    loss_3 = RafLoss
    met_3 = [MAESpec, L1_RafLoss, 'mae']

    loss_4 = NapoliLoss
    met_4 = [MAESpec, sCC, 'mae']

    loss_5 = MatteoLoss
    met_5 = [MAESpec, SSIM, sCC, 'mae']

    gfnet.compile(optimizer=opt, loss=loss_1, metrics=met_1)

    return gfnet









def sCC (y_true, y_pred):

    Diff = y_pred - y_true
    if len(Diff.shape) is 4:
        Diff = Diff[:, :, :, 0]
        y_true = y_true[:, :, :, 0]
        y_pred = y_pred[:, :, :, 0]

    y_pred = tf.expand_dims(y_pred, axis=-1)
    y_true = tf.expand_dims(y_true, axis=-1)

    sobel_filter = [[1, 0, -1], [2, 0, -2], [1, 0, -1]]
    sobel_filter_y = np.asarray(sobel_filter)
    sobel_filter_y = np.expand_dims(sobel_filter_y, axis=-1)
    sobel_filter_y = np.expand_dims(sobel_filter_y, axis=-1)
    sobel_filter_y = sobel_filter_y.astype('float32')

    sobel_filter_x = np.transpose(sobel_filter)
    sobel_filter_x = np.asarray(sobel_filter_x)
    sobel_filter_x = np.expand_dims(sobel_filter_x, axis=-1)
    sobel_filter_x = np.expand_dims(sobel_filter_x, axis=-1)
    sobel_filter_x = sobel_filter_x.astype('float32')

    sobel_pred_x = tf.nn.conv2d(y_pred, sobel_filter_x, strides=[1, 1, 1, 1], padding='VALID')
    sobel_pred_y = tf.nn.conv2d(y_pred, sobel_filter_y, strides=[1, 1, 1, 1], padding='VALID')
    sobel_pred = tf.sqrt(sobel_pred_x ** 2 + sobel_pred_y ** 2)

    sobel_true_x = tf.nn.conv2d(y_true, sobel_filter_x, strides=[1, 1, 1, 1], padding='VALID')
    sobel_true_y = tf.nn.conv2d(y_true, sobel_filter_y, strides=[1, 1, 1, 1], padding='VALID')
    sobel_true = tf.sqrt(sobel_true_x ** 2 + sobel_true_y ** 2)

    sCC = tf.reduce_sum(sobel_pred * sobel_true)
    sCC = sCC / tf.sqrt(tf.reduce_sum(sobel_pred ** 2))
    sCC = sCC / tf.sqrt(tf.reduce_sum(sobel_true ** 2))

    Lstruct = 1 - sCC

    return Lstruct



def MAESpec (y_true, y_pred):

    if y_true.shape[1] is not None:
        MeanFactor = y_true.shape[1] * y_true.shape[2]
    else:
        MeanFactor = 64 * 64

    Diff = y_pred - y_true
    if len(Diff.shape) is 4:
        Diff = Diff[:, :, :, 0]
    Diff = tf.expand_dims(Diff, axis=-1)
    maespec = tf.norm(Diff, ord=1) / MeanFactor

    return maespec



def SSIM (y_true, y_pred):

    Diff = y_pred - y_true
    if len(Diff.shape) is 4:
        y_true = y_true[:, :, :, 0]
        y_pred = y_pred[:, :, :, 0]

    y_pred = tf.expand_dims(y_pred, axis=-1)
    y_true = tf.expand_dims(y_true, axis=-1)

    SSIM = tf.image.ssim(y_pred, y_true, 40.0)

    return SSIM



def L1_MaxLoss (y_true, y_pred):

    if y_true.shape[1] is not None:
        MeanFactor = y_true.shape[1] * y_true.shape[2]
    else:
        MeanFactor = 64 * 64

    Diff = y_pred - y_true
    if len(Diff.shape) is 4:
        Diff = Diff[:, :, :, 0]
        y_pred = y_pred[:, :, :, 0]

    Diff = tf.expand_dims(Diff, axis=-1)
    y_pred = tf.expand_dims(y_pred, axis=-1)

    filter1 = [[0, 1, 0], [0, 0, 0], [0, -1, 0]]
    filter1 = np.asarray(filter1)
    filter1 = np.expand_dims(filter1, axis=-1)
    filter1 = np.expand_dims(filter1, axis=-1)
    filter1 = filter1.astype('float32')

    filter2 = [[0, 0, 0], [1, 0, -1], [0, 0, 0]]
    filter2 = np.asarray(filter2)
    filter2 = np.expand_dims(filter2, axis=-1)
    filter2 = np.expand_dims(filter2, axis=-1)
    filter2 = filter2.astype('float32')

    filter3 = [[1, 0, 0], [0, 0, 0], [0, 0, -1]]
    filter3 = np.asarray(filter3)
    filter3 = np.expand_dims(filter3, axis=-1)
    filter3 = np.expand_dims(filter3, axis=-1)
    filter3 = filter3.astype('float32')

    filter4 = [[0, 0, 1], [0, 0, 0], [-1, 0, 0]]
    filter4 = np.asarray(filter4)
    filter4 = np.expand_dims(filter4, axis=-1)
    filter4 = np.expand_dims(filter4, axis=-1)
    filter4 = filter4.astype('float32')

    L0_Gx = tf.norm(tf.math.abs(tf.nn.conv2d(Diff, filter1, strides=[1, 1, 1, 1], padding='VALID')),
                    ord=1) / MeanFactor
    L0_Gy = tf.norm(tf.math.abs(tf.nn.conv2d(Diff, filter2, strides=[1, 1, 1, 1], padding='VALID')),
                    ord=1) / MeanFactor
    L0_GD = tf.norm(tf.math.abs(tf.nn.conv2d(Diff, filter3, strides=[1, 1, 1, 1], padding='VALID')),
                    ord=1) / MeanFactor
    L0_Gd = tf.norm(tf.math.abs(tf.nn.conv2d(Diff, filter4, strides=[1, 1, 1, 1], padding='VALID')),
                    ord=1) / MeanFactor

    L0_G = tf.reduce_sum([L0_Gx, L0_Gy, L0_GD, L0_Gd])
    L0Struct = L0_G / 4

    return L0Struct



def L1_RafLoss (y_true, y_pred):

    if y_true.shape[1] is not None:
        MeanFactor = y_true.shape[1] * y_true.shape[2]
    else:
        MeanFactor = 64 * 64

    Diff = y_pred - y_true
    if len(Diff.shape) is 4:
        Diff = Diff[:, :, :, 0]
        y_true = y_true[:, :, :, 0]
        y_pred = y_pred[:, :, :, 0]

    Diff = tf.expand_dims(Diff, axis=-1)
    y_pred = tf.expand_dims(y_pred, axis=-1)
    y_true = tf.expand_dims(y_true, axis=-1)

    filter1 = [[0, 1, 0], [0, 0, 0], [0, -1, 0]]
    filter1 = np.asarray(filter1)
    filter1 = np.expand_dims(filter1, axis=-1)
    filter1 = np.expand_dims(filter1, axis=-1)
    filter1 = filter1.astype('float32')

    filter2 = [[0, 0, 0], [1, 0, -1], [0, 0, 0]]
    filter2 = np.asarray(filter2)
    filter2 = np.expand_dims(filter2, axis=-1)
    filter2 = np.expand_dims(filter2, axis=-1)
    filter2 = filter2.astype('float32')

    filter3 = [[1, 0, 0], [0, 0, 0], [0, 0, -1]]
    filter3 = np.asarray(filter3)
    filter3 = np.expand_dims(filter3, axis=-1)
    filter3 = np.expand_dims(filter3, axis=-1)
    filter3 = filter3.astype('float32')

    filter4 = [[0, 0, 1], [0, 0, 0], [-1, 0, 0]]
    filter4 = np.asarray(filter4)
    filter4 = np.expand_dims(filter4, axis=-1)
    filter4 = np.expand_dims(filter4, axis=-1)
    filter4 = filter4.astype('float32')

    pred_Gx = tf.nn.conv2d(y_pred, filter1, strides=[1, 1, 1, 1], padding='VALID')
    pred_Gy = tf.nn.conv2d(y_pred, filter2, strides=[1, 1, 1, 1], padding='VALID')
    pred_GD = tf.nn.conv2d(y_pred, filter3, strides=[1, 1, 1, 1], padding='VALID')
    pred_Gd = tf.nn.conv2d(y_pred, filter4, strides=[1, 1, 1, 1], padding='VALID')

    true_Gx = tf.nn.conv2d(y_true, filter1, strides=[1, 1, 1, 1], padding='VALID')
    true_Gy = tf.nn.conv2d(y_true, filter2, strides=[1, 1, 1, 1], padding='VALID')
    true_GD = tf.nn.conv2d(y_true, filter3, strides=[1, 1, 1, 1], padding='VALID')
    true_Gd = tf.nn.conv2d(y_true, filter4, strides=[1, 1, 1, 1], padding='VALID')

    Gx = tf.norm(tf.math.abs(pred_Gx - true_Gx), ord=1) / MeanFactor
    Gy = tf.norm(tf.math.abs(pred_Gy - true_Gy), ord=1) / MeanFactor
    GD = tf.norm(tf.math.abs(pred_GD - true_GD), ord=1) / MeanFactor
    Gd = tf.norm(tf.math.abs(pred_Gd - true_Gd), ord=1) / MeanFactor

    L0_G = tf.reduce_sum([Gx, Gy, GD, Gd])

    L0Struct = L0_G / 4

    return L0Struct



def NapoliLoss(y_true, y_pred):
    if y_true.shape[1] is not None:
        MeanFactor = y_true.shape[1] * y_true.shape[2]
    else:
        MeanFactor = 64 * 64

    Diff = y_pred - y_true
    if len(Diff.shape) is 4:
        Diff = Diff[:, :, :, 0]
        y_true = y_true[:, :, :, 0]
        y_pred = y_pred[:, :, :, 0]

    Diff = tf.expand_dims(Diff, axis=-1)
    y_pred = tf.expand_dims(y_pred, axis=-1)
    y_true = tf.expand_dims(y_true, axis=-1)

    sobel_filter = [[1, 0, -1], [2, 0, -2], [1, 0, -1]]
    sobel_filter_y = np.asarray(sobel_filter)
    sobel_filter_y = np.expand_dims(sobel_filter_y, axis=-1)
    sobel_filter_y = np.expand_dims(sobel_filter_y, axis=-1)
    sobel_filter_y = sobel_filter_y.astype('float32')

    sobel_filter_x = np.transpose(sobel_filter)
    sobel_filter_x = np.asarray(sobel_filter_x)
    sobel_filter_x = np.expand_dims(sobel_filter_x, axis=-1)
    sobel_filter_x = np.expand_dims(sobel_filter_x, axis=-1)
    sobel_filter_x = sobel_filter_x.astype('float32')

    sobel_pred_x = tf.nn.conv2d(y_pred, sobel_filter_x, strides=[1, 1, 1, 1], padding='VALID')
    sobel_pred_y = tf.nn.conv2d(y_pred, sobel_filter_y, strides=[1, 1, 1, 1], padding='VALID')
    sobel_pred = tf.sqrt(sobel_pred_x ** 2 + sobel_pred_y ** 2)

    sobel_true_x = tf.nn.conv2d(y_true, sobel_filter_x, strides=[1, 1, 1, 1], padding='VALID')
    sobel_true_y = tf.nn.conv2d(y_true, sobel_filter_y, strides=[1, 1, 1, 1], padding='VALID')
    sobel_true = tf.sqrt(sobel_true_x ** 2 + sobel_true_y ** 2)

    sCC = tf.reduce_sum(sobel_pred * sobel_true)
    sCC = sCC / tf.sqrt(tf.reduce_sum(sobel_pred ** 2))
    sCC = sCC / tf.sqrt(tf.reduce_sum(sobel_true ** 2))

    Lstruct = 1 - sCC

    LSpecL1 = tf.norm(Diff, ord=1) / MeanFactor
    LambdaSpec_L1 = 1
    LambdaStruct = 100

    Loss = LambdaStruct * Lstruct + LambdaSpec_L1 *LSpecL1

    return Loss



def MatteoLoss(y_true, y_pred):
    if y_true.shape[1] is not None:
        MeanFactor = y_true.shape[1] * y_true.shape[2]
    else:
        MeanFactor = 64 * 64

    Diff = y_pred - y_true
    if len(Diff.shape) is 4:
        Diff = Diff[:, :, :, 0]
        y_true = y_true[:, :, :, 0]
        y_pred = y_pred[:, :, :, 0]

    Diff = tf.expand_dims(Diff, axis=-1)
    y_pred = tf.expand_dims(y_pred, axis=-1)
    y_true = tf.expand_dims(y_true, axis=-1)

    sobel_filter = [[1, 0, -1], [2, 0, -2], [1, 0, -1]]
    sobel_filter_y = np.asarray(sobel_filter)
    sobel_filter_y = np.expand_dims(sobel_filter_y, axis=-1)
    sobel_filter_y = np.expand_dims(sobel_filter_y, axis=-1)
    sobel_filter_y = sobel_filter_y.astype('float32')

    sobel_filter_x = np.transpose(sobel_filter)
    sobel_filter_x = np.asarray(sobel_filter_x)
    sobel_filter_x = np.expand_dims(sobel_filter_x, axis=-1)
    sobel_filter_x = np.expand_dims(sobel_filter_x, axis=-1)
    sobel_filter_x = sobel_filter_x.astype('float32')

    sobel_pred_x = tf.nn.conv2d(y_pred, sobel_filter_x, strides=[1, 1, 1, 1], padding='VALID')
    sobel_pred_y = tf.nn.conv2d(y_pred, sobel_filter_y, strides=[1, 1, 1, 1], padding='VALID')
    sobel_pred = tf.sqrt(sobel_pred_x ** 2 + sobel_pred_y ** 2)

    sobel_true_x = tf.nn.conv2d(y_true, sobel_filter_x, strides=[1, 1, 1, 1], padding='VALID')
    sobel_true_y = tf.nn.conv2d(y_true, sobel_filter_y, strides=[1, 1, 1, 1], padding='VALID')
    sobel_true = tf.sqrt(sobel_true_x ** 2 + sobel_true_y ** 2)

    sCC = tf.reduce_sum(sobel_pred * sobel_true)
    sCC = sCC / tf.sqrt(tf.reduce_sum(sobel_pred ** 2))
    sCC = sCC / tf.sqrt(tf.reduce_sum(sobel_true ** 2))

    Lstruct = 1 - sCC

    LambdaSpec = 1
    LambdaStruct = 0.1

    LSpecL1 = tf.norm(Diff, ord=1) / MeanFactor
    SSIM = tf.image.ssim(y_pred, y_true, 40.0)
    LSpec_SSIM = 1 - SSIM
    LambdaSpec_SSIM = LambdaSpec * 0.84
    LambdaSpec_L1 = LambdaSpec * (1 - LambdaSpec_SSIM)


    Loss = LambdaSpec_SSIM * LSpec_SSIM + LambdaStruct * Lstruct + LambdaSpec_L1 *LSpecL1

    return Loss


def ProvLoss(y_true, y_pred):
    if y_true.shape[1] is not None:
        MeanFactor = y_true.shape[1] * y_true.shape[2]
    else:
        MeanFactor = 64 * 64

    Diff = y_pred - y_true
    if len(Diff.shape) is 4:
        Diff = Diff[:, :, :, 0]
        y_pred = y_pred[:, :, :, 0]

    Diff = tf.expand_dims(Diff, axis=-1)
    y_pred = tf.expand_dims(y_pred, axis=-1)

    filter1 = [[0, 1, 0], [0, 0, 0], [0, -1, 0]]
    filter1 = np.asarray(filter1)
    filter1 = np.expand_dims(filter1, axis=-1)
    filter1 = np.expand_dims(filter1, axis=-1)
    filter1 = filter1.astype('float32')

    filter2 = [[0, 0, 0], [1, 0, -1], [0, 0, 0]]
    filter2 = np.asarray(filter2)
    filter2 = np.expand_dims(filter2, axis=-1)
    filter2 = np.expand_dims(filter2, axis=-1)
    filter2 = filter2.astype('float32')

    filter3 = [[1, 0, 0], [0, 0, 0], [0, 0, -1]]
    filter3 = np.asarray(filter3)
    filter3 = np.expand_dims(filter3, axis=-1)
    filter3 = np.expand_dims(filter3, axis=-1)
    filter3 = filter3.astype('float32')

    filter4 = [[0, 0, 1], [0, 0, 0], [-1, 0, 0]]
    filter4 = np.asarray(filter4)
    filter4 = np.expand_dims(filter4, axis=-1)
    filter4 = np.expand_dims(filter4, axis=-1)
    filter4 = filter4.astype('float32')

    L0_Gx = tf.norm(tf.math.abs(tf.nn.conv2d(Diff, filter1, strides=[1, 1, 1, 1], padding='VALID')),
                    ord=1) / MeanFactor
    L0_Gy = tf.norm(tf.math.abs(tf.nn.conv2d(Diff, filter2, strides=[1, 1, 1, 1], padding='VALID')),
                    ord=1) / MeanFactor
    L0_GD = tf.norm(tf.math.abs(tf.nn.conv2d(Diff, filter3, strides=[1, 1, 1, 1], padding='VALID')),
                    ord=1) / MeanFactor
    L0_Gd = tf.norm(tf.math.abs(tf.nn.conv2d(Diff, filter4, strides=[1, 1, 1, 1], padding='VALID')),
                    ord=1) / MeanFactor

    L0_G = tf.reduce_sum([L0_Gx, L0_Gy, L0_GD, L0_Gd])

    L0SpecArg = tf.norm(abs(Diff), ord=1)
    L0Spec = L0SpecArg / MeanFactor
    L0Struct = L0_G / 4
    L0Reg = tf.cast((tf.norm(tf.image.image_gradients(y_pred), ord=1)) / MeanFactor, dtype='float32')

    LambdaSpec = 1
    LambdaStruct = 100
    LambdaReg = 0

    L0 = LambdaSpec * L0Spec + LambdaStruct * L0Struct + LambdaReg * L0Reg

    return L0



def RafLoss(y_true, y_pred):
    if y_true.shape[1] is not None:
        MeanFactor = y_true.shape[1] * y_true.shape[2]
    else:
        MeanFactor = 64 * 64

    Diff = y_pred - y_true
    if len(Diff.shape) is 4:
        Diff = Diff[:, :, :, 0]
        y_true = y_true[:, :, :, 0]
        y_pred = y_pred[:, :, :, 0]

    Diff = tf.expand_dims(Diff, axis=-1)
    y_pred = tf.expand_dims(y_pred, axis=-1)
    y_true = tf.expand_dims(y_true, axis=-1)

    filter1 = [[0, 1, 0], [0, 0, 0], [0, -1, 0]]
    filter1 = np.asarray(filter1)
    filter1 = np.expand_dims(filter1, axis=-1)
    filter1 = np.expand_dims(filter1, axis=-1)
    filter1 = filter1.astype('float32')

    filter2 = [[0, 0, 0], [1, 0, -1], [0, 0, 0]]
    filter2 = np.asarray(filter2)
    filter2 = np.expand_dims(filter2, axis=-1)
    filter2 = np.expand_dims(filter2, axis=-1)
    filter2 = filter2.astype('float32')

    filter3 = [[1, 0, 0], [0, 0, 0], [0, 0, -1]]
    filter3 = np.asarray(filter3)
    filter3 = np.expand_dims(filter3, axis=-1)
    filter3 = np.expand_dims(filter3, axis=-1)
    filter3 = filter3.astype('float32')

    filter4 = [[0, 0, 1], [0, 0, 0], [-1, 0, 0]]
    filter4 = np.asarray(filter4)
    filter4 = np.expand_dims(filter4, axis=-1)
    filter4 = np.expand_dims(filter4, axis=-1)
    filter4 = filter4.astype('float32')

    pred_Gx = tf.nn.conv2d(y_pred, filter1, strides=[1, 1, 1, 1], padding='VALID')
    pred_Gy = tf.nn.conv2d(y_pred, filter2, strides=[1, 1, 1, 1], padding='VALID')
    pred_GD = tf.nn.conv2d(y_pred, filter3, strides=[1, 1, 1, 1], padding='VALID')
    pred_Gd = tf.nn.conv2d(y_pred, filter4, strides=[1, 1, 1, 1], padding='VALID')

    true_Gx = tf.nn.conv2d(y_true, filter1, strides=[1, 1, 1, 1], padding='VALID')
    true_Gy = tf.nn.conv2d(y_true, filter2, strides=[1, 1, 1, 1], padding='VALID')
    true_GD = tf.nn.conv2d(y_true, filter3, strides=[1, 1, 1, 1], padding='VALID')
    true_Gd = tf.nn.conv2d(y_true, filter4, strides=[1, 1, 1, 1], padding='VALID')

    Gx = tf.norm(tf.math.abs(pred_Gx - true_Gx), ord=1) / MeanFactor
    Gy = tf.norm(tf.math.abs(pred_Gy - true_Gy), ord=1) / MeanFactor
    GD = tf.norm(tf.math.abs(pred_GD - true_GD), ord=1) / MeanFactor
    Gd = tf.norm(tf.math.abs(pred_Gd - true_Gd), ord=1) / MeanFactor

    L0_G = tf.reduce_sum([Gx, Gy, GD, Gd])

    L0SpecArg = tf.norm(abs(Diff), ord=1)
    L0Spec = L0SpecArg / MeanFactor
    L0Struct = L0_G / 4


    LambdaSpec = 1
    LambdaStruct = 100


    L0 = LambdaSpec * L0Spec + LambdaStruct * L0Struct

    return L0

