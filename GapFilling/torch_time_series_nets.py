import os.path

import torch
import numpy as np
from tqdm import tqdm

class S1S2FCNGapFilling(torch.nn.Module):

    def __init__(self, n_bands):
        super(S1S2FCNGapFilling, self).__init__()

        self.conv1 = torch.nn.Conv2d(in_channels=n_bands, out_channels=256, kernel_size=(3,3), padding='valid')
        self.bnrm1 = torch.nn.BatchNorm2d(256)
        self.relu1 = torch.nn.LeakyReLU()
        self.conv2 = torch.nn.Conv2d(in_channels=256, out_channels=128, kernel_size=(3, 3), padding='valid')
        self.bnrm2 = torch.nn.BatchNorm2d(128)
        self.relu2 = torch.nn.LeakyReLU()
        self.conv3 = torch.nn.Conv2d(in_channels=128, out_channels=64, kernel_size=(3, 3), padding='valid')
        self.bnrm3 = torch.nn.BatchNorm2d(64)
        self.relu3 = torch.nn.LeakyReLU()
        self.conv4 = torch.nn.Conv2d(in_channels=64, out_channels=1, kernel_size=(3, 3), padding='valid')

    def forward(self, x):
        x = self.conv1(x)
        x = self.bnrm1(x)
        x = self.relu1(x)
        x = self.conv2(x)
        x = self.bnrm2(x)
        x = self.relu2(x)
        x = self.conv3(x)
        x = self.bnrm3(x)
        x = self.relu3(x)
        out = self.conv4(x)

        return out

def S1S2GF_train(dataset, n_epochs=100, lr=1e-3, batch_size=128, from_pretrained=None):

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    tst = dataset.__getitem__(0)
    tst_in = torch.Tensor(np.array([tst[:-1]])).to(device)

    if from_pretrained is not None and os.path.exists(from_pretrained):
        model = torch.load(from_pretrained)
    else:
        model = S1S2FCNGapFilling(tst.shape[0]-1)
    model.to(device)

    tst_out = model(tst_in)
    cut = int((tst_in.shape[-1] - tst_out.shape[-1]) / 2)

    opt = torch.optim.Adam(model.parameters(), lr=lr)
    lossFn = torch.nn.L1Loss()

    data_loader = torch.utils.data.DataLoader(dataset, shuffle=True, batch_size=batch_size)
    losses = []

    with tqdm(range(0, n_epochs), unit='epoch') as tepoch:
        for e in tepoch:
            model.train()
            totalTrainLoss = 0
            i_batch = 0

            for s in data_loader:
                #TO DO: cut last band
                x = s[:,:-1,:,:].float().to(device)
                y = s[:,-1,cut:-cut,cut:-cut].float().to(device)

                pred = torch.squeeze(model(x), dim=1)
                loss = lossFn(pred, y)

                opt.zero_grad()
                loss.backward()
                opt.step()

                totalTrainLoss += loss
                i_batch += 1

            tmp = totalTrainLoss.cpu().detach().numpy() / i_batch
            losses.append(tmp)
            tepoch.set_postfix_str('Loss : ' + str(tmp))

    return model, losses





