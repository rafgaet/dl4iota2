from osgeo import gdal, ogr, osr
from sklearn.feature_extraction.image import extract_patches_2d
from math import floor
import numpy as np
import torch

class GDALFullPatchDataset(torch.utils.data.Dataset):

    def __init__(self, filelist, patch_size, band=None, stream_width=10, normalize=True):
        self.patch_size = patch_size
        self.stream_width = stream_width
        self.pad_size = int(floor(patch_size / 2))
        self.normalize = normalize
        self.stats = None
        self.filelist = []
        self.handlers = []
        self.stream = None
        self.current_stream_number = None
        self.band = band

        if self.normalize is not False:
            self.stats = {'min': [], 'max': [], 'mean': [], 'std': []}

        first = True
        for fn in filelist:
            self.filelist.append(fn)
            self.handlers.append(gdal.Open(fn))
            if first:
                self.n_rows, self.n_cols, self.n_channels = self.handlers[-1].RasterYSize, self.handlers[-1].RasterXSize, self.handlers[-1].RasterCount
                first = False
            else:
                assert(self.n_rows == self.handlers[-1].RasterYSize and self.n_cols == self.handlers[-1].RasterXSize and self.n_channels == self.handlers[-1].RasterCount)

            if self.normalize is True:
                sts = [self.handlers[-1].GetRasterBand(i + 1).GetStatistics(0, 1) for i in range(self.n_channels)]
                self.stats['min'].append(np.array([x[0] for x in sts]))
                self.stats['max'].append(np.array([x[1] for x in sts]))

        if self.normalize is True:
            self.stats['min'] = np.min(np.stack(self.stats['min']), axis=0)
            self.stats['max'] = np.max(np.stack(self.stats['max']), axis=0)
            if self.band is not None:
                self.stats['min'] = np.array([self.stats['min'][band]])
                self.stats['max'] = np.array([self.stats['max'][band]])
        elif isinstance(self.normalize,list):
            self.stats['min'] = np.array(self.normalize[0])
            self.stats['max'] = np.array(self.normalize[1])

        if self.band is not None:
            self.n_channels = 1

    def __getitem__(self, index):
        stream_to_load = int(floor(index / (self.n_cols * self.stream_width)))

        if self.current_stream_number != stream_to_load:
            row_pad = [0, 0]
            start_row = int(floor(index / self.n_cols)) - self.pad_size
            if start_row < 0:
                start_row = 0
                row_pad[0] = self.pad_size
            #end_row = int(floor((index + self.batch_size - 1) / self.n_cols)) + self.pad_size
            end_row = start_row + self.stream_width + 2*self.pad_size - 1
            if end_row > self.n_rows - 1:
                end_row = self.n_rows - 1
                row_pad[1] = self.pad_size

            padding = (tuple(row_pad), (self.pad_size, self.pad_size), (0, 0))
            """
            if self.n_channels == 1:
                padding = (tuple(row_pad), (self.pad_size, self.pad_size))
            else:
                padding = (tuple(row_pad), (self.pad_size, self.pad_size), (0, 0))
            """

            self.stream = []

            for ds in self.handlers:
                if self.band is None:
                    stream = ds.ReadAsArray(xoff=0, yoff=start_row, xsize=self.n_cols, ysize=end_row - start_row + 1)
                else:
                    bnd = ds.GetRasterBand(self.band + 1)
                    stream = np.array([bnd.ReadAsArray(xoff=0, yoff=start_row, win_xsize=self.n_cols, win_ysize=end_row - start_row + 1)])
                if self.stats is not None:
                    stream = np.array(
                        [(stream[i, :, :] - self.stats['min'][i]) / (self.stats['max'][i] - self.stats['min'][i]) for i
                         in
                         range(self.n_channels)])

                stream = np.pad(np.moveaxis(stream, 0, -1), padding)
                patches = extract_patches_2d(stream, (self.patch_size, self.patch_size))
                if len(patches.shape) == 4:
                    patches = np.moveaxis(patches, 3, 1)
                self.stream.append(patches)

            self.stream = np.array(self.stream)
            self.stream = np.moveaxis(self.stream, 0, 1)
            self.stream = np.squeeze(self.stream)

            self.current_stream_number = stream_to_load

        return self.stream[index % (self.n_cols * self.stream_width)]


    def __len__(self):
        length = self.n_rows * self.n_cols
        return length

    def __del__(self):
        self.ds = None

class GDALSamplePatchDataset(torch.utils.data.Dataset):

    def __init__(self, filelist, sample_vector, patch_size, band=None, class_field=None, normalize=True, in_memory=True, store_centers=False, as_stack=False):
        self.vectfile = sample_vector
        self.cls_fld = class_field

        self.patch_size = patch_size
        self.pad_size = int(floor(patch_size / 2))

        self.filelist = []
        self.handlers = []
        self.normalize = normalize

        self.store_centers = store_centers
        self.centers = None
        self.as_stack = as_stack

        self.band = band

        self.stats = None
        if self.normalize is not False:
            self.stats = {'min': [], 'max': [], 'mean': [], 'std': []}

        first = True
        for fn in filelist:
            self.filelist.append(fn)
            self.handlers.append(gdal.Open(fn))
            if first:
                srs_raster = osr.SpatialReference()
                srs_raster.ImportFromWkt(self.handlers[-1].GetProjection())
                epsg_raster = srs_raster.GetAuthorityCode('PROJCS')
                self.geot = self.handlers[-1].GetGeoTransform()
                self.n_rows, self.n_cols, self.n_channels = self.handlers[-1].RasterYSize, self.handlers[
                    -1].RasterXSize, self.handlers[-1].RasterCount
                first = False
            else:
                assert (self.n_rows == self.handlers[-1].RasterYSize and self.n_cols == self.handlers[
                    -1].RasterXSize and self.n_channels == self.handlers[-1].RasterCount)

            if self.normalize is True:
                sts = [self.handlers[-1].GetRasterBand(i + 1).GetStatistics(0, 1) for i in range(self.n_channels)]
                self.stats['min'].append(np.array([x[0] for x in sts]))
                self.stats['max'].append(np.array([x[1] for x in sts]))

        if self.normalize is True:
            self.stats['min'] = np.min(np.stack(self.stats['min']), axis=0)
            self.stats['max'] = np.max(np.stack(self.stats['max']), axis=0)
            if band is not None:
                self.stats['min'] = np.array([self.stats['min'][band]])
                self.stats['max'] = np.array([self.stats['max'][band]])
        elif isinstance(self.normalize, list):
            self.stats['min'] = np.array(self.normalize[0])
            self.stats['max'] = np.array(self.normalize[1])

        if band is not None:
            self.n_channels = 1

        self.vds = ogr.Open(self.vectfile)
        self.ly = self.vds.GetLayer(0)
        epsg_vector = self.ly.GetSpatialRef().GetAuthorityCode('PROJCS')
        assert(int(epsg_vector) == int(epsg_raster))

        self.in_memory = in_memory

        self.patches = None
        self.labels = None
        self.preload_samples()

    def preload_samples(self):
        patch_centers = []
        labels = []
        self.ly.ResetReading()
        for f in self.ly:
            g = f.GetGeometryRef()
            p = (g.GetY(), g.GetX())
            patch_centers.append(
                (floor((p[0] - self.geot[3]) / self.geot[5]), floor((p[1] - self.geot[0]) / self.geot[1])))
            if self.cls_fld is not None:
                # labels.append(int(f.GetField(self.cls_fld)))
                labels.append(int(f.GetField(self.cls_fld)) - 1)

        if self.store_centers:
            self.centers = []
        self.patches = []
        self.labels = []

        ysize, xsize = self.patch_size, self.patch_size

        k = 0
        for ds in self.handlers:
            if self.in_memory:
                #print('Reading image #%d' % k)
                if self.band is None:
                    full = ds.ReadAsArray()
                else:
                    bnd = ds.GetRasterBand(self.band + 1)
                    full = bnd.ReadAsArray()
                #print('Done')
            patches_inner = []
            t = 0
            for c in patch_centers:
                yoff, xoff = c[0] - int(floor(self.patch_size / 2)), c[1] - int(floor(self.patch_size / 2))
                if xoff >= 0 and yoff >= 0 and xoff + xsize - 1 < self.n_cols and yoff + ysize - 1 < self.n_rows:
                    if not self.in_memory:
                        if self.band is None:
                            arr = ds.ReadAsArray(xoff=xoff, yoff=yoff, xsize=xsize, ysize=ysize)
                        else:
                            bnd = ds.GetRasterBand(self.band + 1)
                            arr = bnd.ReadAsArray(xoff=xoff, yoff=yoff, win_xsize=xsize, win_ysize=ysize)
                        if self.n_channels == 1:
                            arr = np.array([arr])
                    else:
                        if self.n_channels > 1:
                            arr = full[:, yoff:yoff + ysize, xoff:xoff + xsize]
                        else:
                            arr = np.array([full[yoff:yoff + ysize, xoff:xoff + xsize]])
                    if self.stats is not None:
                        arr = np.array(
                            [(arr[i, :, :] - self.stats['min'][i]) / (self.stats['max'][i] - self.stats['min'][i])
                             for i in range(self.n_channels)])
                    #arr = np.moveaxis(arr, 0, -1)
                    patches_inner.append(arr)

                    if k == 0:
                        if self.store_centers:
                            self.centers.append(c)
                        if self.cls_fld is not None:
                            self.labels.append(labels[t])
                            t += 1

            self.patches.append(np.array(patches_inner))
            k += 1

        full = None
        self.patches = np.array(self.patches)
        self.patches = np.moveaxis(self.patches, 0, 1)
        self.patches = np.squeeze(self.patches)

        if self.as_stack and len(self.filelist) > 0 and self.n_channels > 0:
            self.patches = self.patches.reshape(self.patches.shape[0], self.patches.shape[1] * self.patches.shape[2], self.patches.shape[3], self.patches.shape[4])

    def __getitem__(self, index):
        if self.cls_fld is not None:
            return self.patches[index], self.labels[index]
        else:
            return self.patches[index]

    def __len__(self):
        length = self.patches.shape[0]
        return length

class GDALMultiSamplePatchDataset(torch.utils.data.Dataset):

    def __init__(self, dataset_list, axis=0):
        self.stats = [x.stats for x in dataset_list]
        all_patches = [np.expand_dims(x.patches, axis=1) if x.patches.ndim == 3 else x.patches for x in dataset_list]
        self.patches = np.concatenate(all_patches, axis=axis)
        self.labels = None
        all_labels = [x.labels for x in dataset_list]
        if all(all_labels):
            self.labels = np.concatenate(all_labels)

    def __getitem__(self, index):
        if self.labels == None:
            return self.patches[index]
        else:
            return self.patches[index], self.labels[index]

    def __len__(self):
        return self.patches.shape[0]

